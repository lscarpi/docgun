<?php

namespace App\Exceptions;

use App\Library\ExceptionReporter;
use App\Library\Flash;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthenticationException::class,
        CannotDeleteException::class
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param \Throwable $e
     *
     * @return void
     * @throws \Exception
     */
    public function report(\Throwable $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Throwable $e
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function render($request, \Throwable $e)
    {

        if($e instanceof AuthenticationException){
            return redirect()->route('get_login');
        // Aqui vamos usar a exception pra quando o cidadão não puder deletar o objeto
        }else if($e instanceof CannotDeleteException) {
            // Flash
            Flash::error($e->getMessage());
            // Redirect pra página anterior
            return redirect()->back();
            // Easy
        // Se for not found
        }else if($e instanceof NotFoundHttpException || $e instanceof ModelNotFoundException) {
            return response()->view('errors.404');
            // Se não for nada disso, é uma exception triste, que precisa ser notificada
        }else if($e instanceof TokenMismatchException) {
            Flash::error('Sua sessão expirou. Por favor, faça login novamente!');
            return redirect()->route('get_login');
        // Se a exception for uma ValidationException
        }else if ($e instanceof ValidationException){
            // Retornar para a URL anterior com a messagebag do validator
            return redirect()->back()->withErrors($e->validator->getMessageBag());
        // Se não for nenhum dos acima
        }else{
            // Se for production server
            if(isProductionServer()){
                // Reportar a exception
                ExceptionReporter::handle($e);
                // E mostrar view genérica
                return response()->view('errors.500');
            }
            // Caso contrário, o erro explode na tela
        }

        return parent::render($request, $e);
    }
}
