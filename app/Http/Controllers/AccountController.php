<?php

namespace App\Http\Controllers;

use App\Library\Flash;
use App\Models\Company;
use App\Models\Plan;
use App\Models\Subscription;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class AccountController extends Controller
{
    public function get_account_profile()
    {
        $user = auth()->user();

        return view('account.profile', [
            'user' => $user,
        ]);
    }

    public function post_account_profile(Request $request)
    {
        // Obter o user
        $user = auth()->user();
        // Validar a request malandra
        $this->validate($request, User::validationRules($user), User::validationMessages());
        // Fill no user
        $user->fill($request->except(['password']));

        // Se tiver password na request
        if ($request->filled('password')) {
            // Montar a password do cara
            $user->password = bcrypt($request->get('password'));
        }
        // Salvar o cara
        $user->save();
        // Mensagem do bem
        Flash::success('Perfil alterado com sucesso!');
        // Retornar
        return back();
    }

    public function get_account_company()
    {
        $company = auth()->user()->company;

        return view('account.company', [
            'company' => $company,
        ]);
    }

    public function post_account_company(Request $request)
    {
        // Validar a request
        $this->validate($request, Company::rules(), Company::validationMessages());
        // Se chegou aqui, nice do brasil
        $company = auth()->user()->company;
        // Fill na company
        $company->fill($request->all());
        // Criar o object address
        $company->address = createAddressObject($request);
        // Salvar a company do citzen
        $company->save();
        // Mensagem do bem
        Flash::success('Dados da empresa alterados com sucesso!');
        // Retornar
        return back();
    }

    public function get_account_subscription()
    {
        $user = auth()->user();
        $current_subscription = $user->company->getCurrentSubscription();
        $pending_subscription = $user->company->getPendingSubscription();
        $plans = Plan::get();

        return view('account.subscription', [
            'current_subscription' => $current_subscription,
            'pending_subscription' => $pending_subscription,
            'plans' => $plans,
        ]);
    }

    /**
     * Function para criar um upgrade de plano.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function post_account_subscription(Request $request)
    {
        // Se não tiver um plan_id
        if (!$request->get('plan_id')) {
            // Abandonar a missão
            Flash::error('Por favor, selecione um plano');

            return back();
        }

        // Pegar o user
        $user = auth()->user();
        $company = $user->company;
        // Pegar a subscription
        $current_subscription = $company->getCurrentSubscription();
        // Pegar o plan
        $plan = Plan::find($request->get('plan_id'));
        // Se não tiver plan
        if (!$plan) {
            // Abandona
            Flash::error('O plano selecionado não foi encontrado');

            return back();
        }
        // Se tiver subscription e o id do plano selecionado foi o mesmo da subscription
        if ($current_subscription && $plan->id == $current_subscription->plan->id) {
            // That's a peixe
            Flash::error('Não é possível alterar sua assinatura para o mesmo plano');

            return back();
        }

        // Se o cara já solicitou antes
        if (!$company->canChangeSubscription()) {
            // That's a peixe as well
            Flash::error('Você já possui uma alteração de assinatura pendente!');

            return back();
        }

        if ($company->createNewSubscription($plan)) {
            // That's a peixe as well
            Flash::success('Sucesso! Agradecemos pela sua solicitação.');

            return back();
        }

        // That's a peixe as well
        Flash::error('Erro interno ao processar sua solicitação.');

        return back();
    }

    /**
     * Function que redireciona o client pro web checkout.
     *
     * @param Subscription $subscription
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     *
     * @throws Exception
     */
    public function get_account_subscription_pay(Subscription $subscription)
    {
        // Se não puder ler
        if (!Gate::allows('read', $subscription)) {
            // Dead
            return $this->notAuthorized();
        }

        // Se a subscription Não tiver pending
        if (!$subscription->isPending()) {
            // Não pode pagar
            return $this->notAuthorized();
        }

        // Puxar o provider
        $provider = $subscription->getProviderClass();

        // Gerar a URL de redirect
        $redirectURL = $provider::createSubscription($subscription);

        // Se gerou
        if ($redirectURL) {
            // Redirecionar
            return redirect()->to($redirectURL);
        }

        // Se chegou aqui, bad news
        throw new Exception('Redirect URL de pagamento não foi gerada para a subscription '.$subscription->id);
    }

    /**
     * Function que abre a view de quando o cara acabou de pagar a parada.
     *
     * @param Subscription $subscription
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function get_account_subscription_thankyou(Subscription $subscription)
    {
        // Retornar a view agradecendo o campson
        return view('account.payment_thankyou', [
            'subscription' => $subscription,
        ]);
    }

    /**
     * Function para cancelar a solicitação de upgrade de plano.
     *
     * @param Subscription $subscription
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     *
     * @throws Exception
     */
    public function get_account_subscription_cancel(Subscription $subscription)
    {
        // Se não puder ler
        if (!Gate::allows('read', $subscription)) {
            // Dead
            return $this->notAuthorized();
        }

        // Se a sub não tiver pendente
        if (!$subscription->isPending()) {
            // Dead
            Flash::error('Não é possível cancelar uma assinatura que não está pendente');

            return back();
        }

        // Cancelar a subscription no provider, se houver
        $subscription->setTerminated();

        // Se deletar
        if ($subscription->delete()) {
            // Show boy
            Flash::success('Solicitação cancelada com sucesso');

            return back();
        }

        // Se chegou aqui babou
        Flash::error('Erro interno ao cancelar solicitação');

        return back();
    }
}
