<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Subscription;
use Illuminate\View\View;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * Abrir a dashboard do admin.
     *
     * @return View
     */
    public function getDashboard(): View
    {
        // Puxar os latest companies
        $latestCompanies = Company::orderBy('id', 'DESC')->take(5)->get();
        // Puxar a contagem de todas as companies
        $totalCompanies = Company::count();
        // Puxar a contagem de todas as subscriptions em que o price for 0
        $freeSubscriptions = Subscription::where('price', 0)->count();
        // Puxar a contagem de todas as subscriptions em que o price for maior que 0
        $paidSubscriptions = Subscription::where('price', '>', 0)->count();
        // Puxar todas as subscriptions em que o trial esteja acabando dentro dos próximos 5 dias
        $trialExpiringSubscriptions = Subscription::where('status', 'active')
            ->with('company')
            ->where('plan_id', 1)
            ->where('price', 0)
            ->where('terminate_at', '>=', now()->startOfDay()->toDateTimeString())
            ->where('terminate_at', '<=', now()->addDays(4)->startOfDay()->toDateTimeString())
            // Order pela data de encerramento mais próxima
            ->orderBy('terminate_at', 'ASC')->get();

        $newestPaidSubscriptions = Subscription::where('price', '>', 0)
          ->orderBy('id', 'DESC')
          ->take(5)
          ->get();

        // Puxar os users por mês dos últimos 12 meses
        $companiesDatatable = $this->companiesPerMonthDatatable();
        // Retornar a view
        return view('admin.dashboard', [
            'companiesDatatable'            => $companiesDatatable,
            'latestCompanies'               => $latestCompanies,
            'totalCompanies'                => $totalCompanies,
            'freeSubscriptions'             => $freeSubscriptions,
            'paidSubscriptions'             => $paidSubscriptions,
            'trialExpiringSubscriptions'    => $trialExpiringSubscriptions,
            'newestPaidSubscriptions'       => $newestPaidSubscriptions,
        ]);
    }

    /**
     * @param int $total_months
     *
     * @return array
     */
    public function companiesPerMonthDatatable($total_months = 12)
    {
        // Criar uma array
        $data_table = [];

        // Loop no total de months
        foreach (range(0, $total_months - 1) as $month_index) {
            // Criar uma data de start
            $date_start = now()->subMonths($month_index)->startOfMonth();
            // E uma de end
            $date_end = now()->subMonths($month_index)->endOfMonth();

            // Query na resource
            $resource = Company::query()
                // Onde a data de criação estiver entre as duas
                ->whereBetween('created_at', [$date_start->toDateTimeString(), $date_end->toDateTimeString()])
                // Pegar o total apenas
                ->count();

            // Setar o valor da data table
            $data_table[] = [$date_start->formatLocalized('%m/%Y'), $resource];
        }
        // Retornar a array
        return array_reverse($data_table);
    }
}
