<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Library\Flash;
use App\Models\Subscription;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class SubscriptionsController.
 */
class SubscriptionsController extends Controller
{
    /**
     * @param Request $request
     * @return View
     */
    public function getSubscriptions(Request $request) : View
    {
        $subscriptions = Subscription::orderBy('id', 'DESC')->paginate(15);

        return view('admin.subscriptions.list', [
            'subscriptions' => $subscriptions,
        ]);
    }

    /**
     * Function que gerencia o edit de uma subscription
     * @param Subscription $subscription
     * @return View
     */
    public function getSubscriptionsEdit(Subscription $subscription) : View
    {
        // Return the view with the subscription to edit
        return view('admin.subscriptions.edit', ['subscription' => $subscription]);
    }

    /**
     * Function para dar update em uma subscription
     * @param Request $request
     * @param Subscription $subscription
     * @return RedirectResponse
     */
    public function patchSubscriptionsUpdate(Request $request, Subscription $subscription) : RedirectResponse
    {
        if (! $request->has('max_users') || ! $request->has('status')) {
            // Warn the brother
            Flash::error('Por favor, informe todos os dados obrigatórios da assinatura');
            // Return to the previous page
            return back();
        }

        // Update the subscription
        $subscription->status = $request->get('status');
        // This last one returns a bool
        if ($subscription->setMaxUsers($request->get('max_users'))) {
            // Nice
            Flash::success('Assinatura atualizada com sucesso!');
            // Lesgo
            return redirect()->route('admin.getSubscriptions');
        }
        // Else
        Flash::error('Assinatura não pode ser atualizada, erro interno!');
        // Bad vibes
        return back();
    }
}
