<?php

namespace App\Http\Controllers;

use App\Library\Flash;
use App\Library\MarketingAutomation\VilleTarget;
use App\Mail\AdminNotifications\NewUserRegistered;
use App\Models\User;
use Illuminate\Support\Str;
use function GuzzleHttp\Promise\iter_for;
use http\Params;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use Illuminate\View\View;

/**
 * Class AuthController
 * @package App\Http\Controllers
 */
class AuthController extends Controller
{

    /**
     * Mostrar a tela de login
     * @return Factory|View
     */
    public function get_login()
    {
        return view('auth.login');
    }

    /**
     * Tentar fazer o login
     * @param Request $request
     * @return RedirectResponse
     */
    public function post_login(Request $request)
    {
        // Se o login funcionar
        if (auth()->attempt(['email' => $request->email, 'password' => $request->password], $request->has('keep_connected'))) {

            // Checar se o champion tá ativo já pra poder fazer esse tipo de asneira
            if (! auth()->user()->isActive()) {
                // Show
                Flash::error('Por favor ative sua conta antes de entrar no sistema');
                return back();
            }

            // Checar se a empresa está ativa
            if (! auth()->user()->company->isActive()) {
                // Show
                Flash::error('A Empresa referente a este usuário está inativa.');
                return back();
            }

            // Checar se a empresa possui uma subscription
            if (! auth()->user()->company->isSubscriptionValid()) {
                // Show
                Flash::error('A Empresa referente a este usuário não possui uma assinatura ativa.');
                return back();
            }

            // Marcar que este user está atualmente logado
            auth()->user()->updateCurrentLoggedInSession();

            // Mandar mensagem
            Flash::success('Bem vindo de volta!');
            // Jogar para a dashboard
            return redirect()->route('get_dashboard');
        }
        // Se chegou aqui, não passou no attempt

        // Mandar mensagem
        Flash::error('Usuário ou senha inválidos.');
        // Redirecionar para a página anterior
        return redirect()->back();
    }

    /**
     * Mostrar a tela de register
     * @return Factory|View
     */
    public function get_register()
    {
        return view('auth.register');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function post_register(Request $request)
    {

        // Whats the most MVP thing we can do?
        // Validate the request
        $this->validate($request, User::validationRules(), User::validationMessages());

        // Create the user (auto creates the company, nice)
        $user = new User();
        $user->fill($request->all());
        $user->save();


        // Se tiver na request uma info de call to action
        if ($request->cta) {
            dd($request);
            // Tirar a criptografia
            $json_data = Crypt::decrypt($request->cta);
            // Decode no valor retornado (parseando pra array)
            $decoded_data = json_decode($json_data, true);
            // Se conseguir dar decode
            if ($decoded_data) {
                // Dar um loop nos dados que foram decoded
                foreach ($decoded_data as $key => $value) {
                    // Cache set no usuário com a key e value obtido nos dados decoded
                    $user->cacheSet($key, $value);
                }
            }
        }
        // Enviar email de ativação
        $user->sendActivationEmail();

        // Notificar os admins via email
        notifySystemAdmins(new NewUserRegistered($user));

        // we will register the user on ville Target
        // Create a new ville Target integration instance
        $villeTarget = new VilleTarget();
        // Subscribe the user
        $villeTarget->subscribeUser(config('docgun.general.marketing_automation.ville_target.list_uid'), $user);

        // Flash para abrir próxima pagina pra ele
        session()->flash('just_registered_user', true);
        // Criptografar user data
        session()->put('user_data', ['email' => $user->email, 'first_name' => $user->firstName()]);

        // Retornar pro redirect da maldade, agradecendo o champs pela paradinha angelical que ele acabou de fazer
        return redirect()->route('get_register_thank_you');
    }

    public function get_register_thank_you()
    {
        if (session()->has('just_registered_user')) {
            // Retornando a view
            return view('auth.register_thank_you', [
                'email'         => session()->get('user_data')['email'],
                'first_name'    => session()->get('user_data')['first_name'],
            ]);
        }
        return redirect()->route('get_login');
    }

    public function get_register_confirm_registration($token)
    {
        $user = User::where('cache', 'like', "%$token%")->first();

        if (! $user) {
            Flash::error('Usuário não encontrado!');
            return redirect()->route('get_login');
        }

        if ($user->isActive()) {
            Flash::error('Usuário já se encontra ativado!');
            return redirect()->route('get_login');
        }

        // Se chegou aqui, o cara tem que ativar
        $user->status = User::USER_STATUS_ACTIVE;
        $user->cacheUnset('activation_token');
        // Não precisa fazer $user->save pois dentro do cacheUnset já faz
        // Logar o cidadão
        auth()->login($user);
        // Puxar a company do user
        $company = $user->company;
        // Se ela não tiver subscription
        if (! $company->hasSubscription()) {
            // Criar uma de trial
            $company->createTrialSubscription();
        }

        // Aqui colocaremos o token de login no usuário, para que o middleware não diga que ele está logado em outro terminal
        $user->updateCurrentLoggedInSession();

        // Mensagem de amor pra ele
        Flash::success('Usuário ativado com sucesso! Bem vindo ao docGun.');
        // Redirect pra tela dos customers
        return redirect()->route('get_customers');
    }


    /**
     * Fazer o logout
     * @return RedirectResponse
     */
    public function get_logout()
    {
        auth()->user()->removeCurrentLoggedInSession();
        // Fazer o logout de fato
        auth()->logout();
        // Limpar tudo da session
        session()->flush();
        // Redirecionar para a dashboard (que eventualmente cai na tela de login)
        return redirect()->route('get_dashboard');
    }

    public function get_recover_password()
    {
        return view('auth.password_recovery');
    }

    public function post_recover_password(Request $request)
    {
        $mail = $request->request->get("email");
        $user = User::where('email', $mail)->first();

        if ($mail && $user) {
            $newPassword = Str::random(6);
            $user->password = bcrypt($newPassword);
            $user->save();
            //dd($user->password);
            $user->sendResetPasswordEmail($newPassword);

        }else{
            Flash::warning("Não foi possível encontrar nenhum usuário para o email informado.");
            return view('auth.password_recovery');
        }

        // Retornar pro redirect da maldade, agradecendo o champs pela paradinha angelical que ele acabou de fazer
        Flash::success('Email enviado com sucesso. Dê uma olhada em sua caixa de entrada.');
        return redirect()->route('get_login');
    }
}
