<?php

namespace App\Http\Controllers;

use App\Library\Flash;
use App\Models\Club;
use Illuminate\Http\Request;

class ClubsController extends Controller{

    /**
     * Listar clubes
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_clubs(Request $request)
    {
        // Buscar no BD
        $clubs = Club::resourceIsolated()->search($request)->paginate(20);
        // Mostrar a view
        return view('clubs.list', ['clubs' => $clubs]);
    }

    /**
     * Mostrar a tela de cadastro de clubes
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_clubs_create()
    {
        return view('clubs.create');
    }

    /**
     * Tentar cadastrar um clube
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function post_clubs_create(Request $request)
    {
        // Apenas alegria
        return $this->createOrEditClub($request);
    }

    /**
     * Mostrar a tela de edição de clubes
     * @param Club $club
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_clubs_edit(Club $club)
    {
        if( ! $club->currentUserCanUpdate()){
            return $this->notAuthorized();
        }

        return view('clubs.edit', ['club' => $club]);
    }

    /**
     * Tentar editar um clube
     * @param Request $request
     * @param Club $club
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function post_clubs_edit(Request $request, Club $club)
    {
        // Apenas alegria
        return $this->createOrEditClub($request, $club);
    }

    /**
     * Tentar deletar um clube
     * @param Club $club
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function get_clubs_delete(Club $club)
    {
        // Sucesso
        Flash::success('Clube ' . $club->name . ' excluído com sucesso.');
        // Deletar
        $club->delete();
        // Jogar pra listagem de clubes
        return redirect()->route('get_clubs');
    }


    /**
     * @param Request $request
     * @param Club|null $club
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    protected function createOrEditClub(Request $request, Club $club = null)
    {

        // Criar var pra saber se estamos editando um clube ou criando um novo
        $isEditingExistingClub = ! empty($club);

        // Validação de dados ========================
        // Passar a validation
        $this->validate($request, Club::rules(), Club::validationMessages());
        // Chegou aqui, deu bom

        // Ação ========================

        // Aqui vamos fazer o seguinte
        // Se na function tiver um user passado como parametro, usa ele
        // Senão, cria um novo
        $club = $club ? $club : new Club();
        // Preencher com a request
        $club->fill($request->all());

        // Criar o endereço
        $club->address = createAddressObject($request);

        // Salvar o clube
        $club->save();

        // Montar mensagem
        $message =
            $isEditingExistingClub ?
                'Clube %s editado com sucesso' :
                'Clube %s criado com sucesso';

        // Formatar
        $message = sprintf($message, $club->name);

        // Enviar flash
        Flash::success($message);

        // Redirecionar pra listagem de clubs
        return redirect()->route('get_clubs');
    }
}