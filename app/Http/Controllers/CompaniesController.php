<?php

namespace App\Http\Controllers;

use App\Library\Flash;
use App\Models\Company;
use Illuminate\Http\Request;

class CompaniesController extends Controller{

    /**
     * Listar companies
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_companies(Request $request)
    {
        // Buscar no BD
        $companies = Company::search($request)->paginate(20);
        // Mostrar a view
        return view('companies.list', ['companies' => $companies]);
    }

    /**
     * Tentar deletar um clube
     * @param Company $company
     * @return \Illuminate\Http\RedirectResponse
     */
    public function get_companies_delete(Company $company)
    {
        //TODO: No brother, cant do
        Flash::error('Por gentileza, pare de peixe.');
        // Jogar pra listagem de empresas
        return redirect()->route('get_companies');
//
//
//        // Sucesso
//        Flash::success('Empresa ' . $company->name . ' excluída com sucesso.');
//        // Deletar
//        $company->delete();
//        // Jogar pra listagem de empresas
//        return redirect()->route('get_companies');
    }

    public function get_companies_login_as(Company $company)
    {
        auth()->user()->switchCompany($company);
        Flash::success('Empresa alterada com sucesso!');
        // Jogar para a listagem de users
        return redirect()->route('get_users');
    }

    public function get_companies_login_back()
    {
        auth()->user()->switchCompanyBack();
        Flash::success('Retornado para empresa original com sucesso!');
        // Jogar para a listagem de users
        return redirect()->route('get_users');
    }




}