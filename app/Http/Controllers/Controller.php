<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\View\View;

/**
 * Class Controller
 * @package App\Http\Controllers
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param Request|null $request
     * @return Factory|JsonResponse|View
     */
    public function notAuthorized(Request $request = null)
    {
        // Enviar um email que um cidadão tentou acessar recurso que não podia
        // Retornar view
        if ($request && $request->ajax()) {
            return response()->json('Unauthorized', 403);
        } else {
            return view('errors.403');
        }
    }
}
