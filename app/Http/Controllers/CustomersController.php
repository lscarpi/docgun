<?php

namespace App\Http\Controllers;

use App\Library\Flash;
use App\Models\Customer;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;

class CustomersController extends Controller{

    /**
     * Listar clientes
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_customers(Request $request)
    {
        // Buscar no BD
        $customers = Customer::resourceIsolated()->search($request)->recentsFirst()->paginate(20);
        // Mostrar a view
        return view('customers.list', ['customers' => $customers]);
    }

    /**
     * Mostrar a tela de cadastro de clientes
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_customers_create()
    {
        return view('customers.create');
    }

    /**
     * Tentar cadastrar um cliente
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function post_customers_create(Request $request)
    {
        // Apenas alegria
        return $this->createOrEditCustomer($request);
    }

    /**
     * Mostrar a tela de perfil dos clientes
     * @param Customer $customer
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_customers_view(Customer $customer)
    {
        return view('customers.view', ['customer' => $customer]);
    }

    /**
     * Mostrar a tela de edição de clientes
     * @param Customer $customer
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_customers_edit(Customer $customer)
    {
        return view('customers.edit', ['customer' => $customer]);
    }

    /**
     * Tentar editar um cliente
     * @param Request $request
     * @param Customer $customer
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function post_customers_edit(Request $request, Customer $customer)
    {
        // Apenas alegria
        return $this->createOrEditCustomer($request, $customer);
    }

    /**
     * Tentar deletar um cliente
     * @param Customer $customer
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function get_customers_delete(Customer $customer)
    {
        // Sucesso
        Flash::success('Cliente ' . $customer->name . ' excluído com sucesso.');
        // Deletar
        $customer->delete();
        // Jogar pra listagem de clientes
        return redirect()->route('get_customers');
    }


    /**
     * @param Request $request
     * @param Customer|null $customer
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    protected function createOrEditCustomer(Request $request, Customer $customer = null)
    {
        // Criar var pra saber se estamos editando um cliente ou criando um novo
        $isEditingExistingCustomer = ! empty($customer);

        // Validação de dados ========================
        // Passar a validation
        $this->validate($request, Customer::rules(), Customer::validationMessages());
        // Chegou aqui, deu bom

        // Ação ========================

        // Aqui vamos fazer o seguinte
        // Se na function tiver um user passado como parametro, usa ele
        // Senão, cria um novo
        $customer = $customer ? $customer : new Customer();
        // Preencher com a request
        $customer->fill($request->all());

        // Criar os endereços
        try {
            $customer->residential_address = createAddressObject($request, 'residential_address');
        }catch(\Exception $e){}

        try {
            $customer->comercial_address = createAddressObject($request, 'comercial_address');
        }catch(\Exception $e){}

        // Criar as datas
        $customer->birth_date                               = createDateFromBRFormat($request->get('birth_date'));
        $customer->registration_number_expedition_date      = createDateFromBRFormat($request->get('registration_number_expedition_date'));
        $customer->cr_expedition_date                       = createDateFromBRFormat($request->get('cr_expedition_date'));
        $customer->cr_expiration_date                       = createDateFromBRFormat($request->get('cr_expiration_date'));

        // Salvar o cliente
        $customer->save();

        // Montar mensagem
        $message =
            $isEditingExistingCustomer ?
                'Cliente %s editado com sucesso' :
                'Cliente %s criado com sucesso';

        // Formatar
        $message = sprintf($message, $customer->name);

        // Enviar flash
        Flash::success($message);

        // Redirecionar pra listagem de customers
        return redirect()->route('get_customers');
    }

    /**
     * Mostrar a tela de detalhes de um documento
     * @param Customer $customer
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_customers_customer_data_details(Customer $customer)
    {
//        $pdf = PDF::loadView('customers.customer_details', [
//            'customer' => $customer,
//        ]);
//        return $pdf->download($customer->name . '.pdf');

        return view('customers.customer_details', [
            'customer' => $customer,
        ]);
    }

    public function get_customers_customer_data_details_pdf(Customer $customer)
    {

        $pdf = PDF::loadView('customers.customer_details', [
            'customer' => $customer,
        ]);
        return $pdf->download($customer->name . '.pdf');

//        return view('customers.customer_details', [
//            'customer' => $customer,
//        ]);
    }
}