<?php

namespace App\Http\Controllers;

use App\Library\Flash;
use App\Models\Document;
use Illuminate\Auth\Access\Gate;
use Illuminate\Http\Request;

class DocumentsController extends Controller{

    /**
     * Listar documentos
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_documents(Request $request)
    {
        // Buscar no BD
        $documents = Document::ownedByCurrentUser()->orderBy('title', 'ASC')->search($request)->paginate(20);
        $shared_documents = Document::sharedByOthers()->orderBy('title', 'ASC')->search($request)->paginate(20);




        // Se a request for ajax, voltar em JSON
        if($request->ajax()){
            return response()->json($documents);
        }

        // Senão mostrar a view
        return view('documents.list', ['documents' => $documents, 'shared_documents' => $shared_documents]);
    }

    /**
     * Mostrar a tela de cadastro de documentos
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_documents_create()
    {
        $document = new Document();
        if( ! $document->currentUserCanCreate()){
            return $this->notAuthorized();
        }

        return view('documents.create');
    }

    /**
     * Tentar cadastrar um documento
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function post_documents_create(Request $request)
    {
        $document = new Document();
        if( ! $document->currentUserCanCreate()){
            return $this->notAuthorized();
        }

        // Apenas alegria
        return $this->createOrEditDocument($request);
    }

    /**
     * Visualizar os dados de um documento (ajax apenas)
     * @param Request $request
     * @param Document $document
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_documents_view(Request $request, Document $document)
    {
        if( ! $document->currentUserCanRead()){
            return $this->notAuthorized($request);
        }

        if($request->ajax()){
            return response()->json($document);
        }
        return abort(404);
    }

    /**
     * Mostrar a tela de edição de documentos
     * @param Document $document
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_documents_edit(Document $document)
    {
        if( ! $document->currentUserCanRead()){
            return $this->notAuthorized();
        }

        return view('documents.edit', ['document' => $document]);
    }

    /**
     * Tentar editar um arma
     * @param Request $request
     * @param Document $document
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function post_documents_edit(Request $request, Document $document)
    {
        if( ! $document->currentUserCanUpdate()){
            return $this->notAuthorized();
        }
        // Apenas alegria
        return $this->createOrEditDocument($request, $document);
    }

    /**
     * Tentar deletar um arma
     * @param Document $document
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function get_documents_delete(Document $document)
    {

        if( ! $document->currentUserCanDelete()){
            return $this->notAuthorized();
        }

        // Sucesso
        Flash::success('Documento ' . $document->title . ' excluído com sucesso.');
        // Deletar
        $document->delete();
        // Jogar pra listagem de documentos
        return redirect()->route('get_documents');
    }

    /**
     * Tentar compartilhar um arma
     * @param Document $document
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function get_documents_share(Document $document)
    {

        if( ! $document->currentUserCanShare()){
            return $this->notAuthorized();
        }

        // Alterar o valor do campo shared na table documents
        $document->shared = true;
        // Salvar
        $document->save();
        // Sucesso
        Flash::success('Documento ' . $document->title . ' compartilhado com sucesso.');
        // Jogar pra listagem de documentos
        return redirect()->route('get_documents');
    }

    /**
     * Tentar compartilhar um arma
     * @param Document $document
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function get_documents_unshare(Document $document)
    {

        if( ! $document->currentUserCanShare()){
            return $this->notAuthorized();
        }

        // Alterar o valor do campo shared na table documents
        $document->shared = false;
        // Salvar
        $document->save();
        // Sucesso
        Flash::success('Documento ' . $document->title . ' teve remoção compartilhada.');
        // Jogar pra listagem de documentos
        return redirect()->route('get_documents');
    }

    /**
     * @param Request $request
     * @param Document|null $document
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    protected function createOrEditDocument(Request $request, Document $document = null)
    {

        // Criar var pra saber se estamos editando um arma ou criando um novo
        $isEditingExistingDocument = ! empty($document);

        // Validação de dados ========================
        // Passar a validation
        $this->validate($request, Document::rules(), Document::validationMessages());
        // Chegou aqui, deu bom

        // Ação ========================

        // Aqui vamos fazer o seguinte
        // Se na function tiver um user passado como parametro, usa ele
        // Senão, cria um novo
        $document = $document ? $document : new Document();
        // Preencher com a request
        $document->fill($request->all());
        // Salvar o documento
        $document->save();

        // Montar mensagem
        $message =
            $isEditingExistingDocument ?
                'Documento %s editada com sucesso' :
                'Documento %s criada com sucesso';

        // Formatar
        $message = sprintf($message, $document->title);

        // Enviar flash
        Flash::success($message);

        // Redirecionar pra listagem de documentos
        return redirect()->route('get_documents');
    }
}