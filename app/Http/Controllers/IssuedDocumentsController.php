<?php

namespace App\Http\Controllers;

use App\Library\Flash;
use App\Models\Customer;
use App\Models\Document;
use App\Models\IssuedDocument;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;

class IssuedDocumentsController extends Controller{


    /**
     * Mostrar a tela de geração de um documento
     * @param Customer $customer
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_customers_issued_documents_create(Customer $customer)
    {
        return view('customers.issued_documents.create', [
            'customer' => $customer,
            'documents' => Document::resourceIsolated()->get(),
        ]);
    }

    /**
     * Tentar gerar um documento
     * @param Request $request
     * @param Customer $customer
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function post_customers_issued_documents_create(Request $request, Customer $customer)
    {
        // Apenas alegria
        return $this->createOrEditIssuedDocument($request, $customer);
    }

    /**
     * Mostrar a tela de detalhes de um documento
     * @param Customer $customer
     * @param IssuedDocument $issued_document
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_customers_issued_documents_details(Customer $customer, IssuedDocument $issued_document)
    {
//        $pdf = PDF::loadView('customers.issued_documents.details', [
//            'customer' => $customer,
//            'issued_document' => $issued_document,
//        ]);
//        return $pdf->stream($issued_document->title . '_' . $customer->name . '.pdf');

        return view('customers.issued_documents.details', [
            'customer' => $customer,
            'issued_document' => $issued_document,
        ]);
    }

    /**
     * Mostrar a tela de edição de documentos
     * @param Customer $customer
     * @param IssuedDocument $issued_document
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_customers_issued_documents_edit(Customer $customer, IssuedDocument $issued_document)
    {
        return view('customers.issued_documents.edit', [
            'customer' => $customer,
            'issued_document' => $issued_document,
            'documents' => Document::all(),
        ]);
    }

    /**
     * Tentar editar um documento
     * @param Request $request
     * @param Customer $customer
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function post_customers_issued_documents_edit(Request $request, Customer $customer, IssuedDocument $issued_document)
    {
        // Apenas alegria
        return $this->createOrEditIssuedDocument($request, $customer, $issued_document);
    }

    /**
     * Tentar deletar um documento
     * @param Customer $customer
     * @param IssuedDocument $issued_document
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function get_customers_issued_documents_delete(Customer $customer, IssuedDocument $issued_document)
    {
        // Sucesso
        Flash::success('Documento ' . $issued_document->title . ' excluído com sucesso.');
        // Deletar
        $issued_document->delete();
        // Jogar pra listagem de documentos
        return redirect()->route('get_customers_view', ['customer' => $customer->id, 'tab' => 'issued-documents']);
    }


    /**
     * @param Request $request
     * @param Customer $customer
     * @param IssuedDocument|null $issued_document
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function createOrEditIssuedDocument(Request $request, Customer $customer, IssuedDocument $issued_document = null)
    {

        // Criar var pra saber se estamos editando um documento ou criando um novo
        $isEditingExistingIssuedDocument = ! empty($issued_document);

        // Validação de dados ========================
        // Passar a validation
        $this->validate($request, IssuedDocument::rules(), IssuedDocument::validationMessages());
        // Chegou aqui, deu bom

        // Ação ========================

        // Aqui vamos fazer o seguinte
        // Se na function tiver um user passado como parametro, usa ele
        // Senão, cria um novo
        $issued_document = $issued_document ? $issued_document : new IssuedDocument();
        // Preencher com a request
        $issued_document->fill($request->all());
        // Setar o customer, né
        $issued_document->customer_id = $customer->id;
        // Parsear os valores pra salvar
        $issued_document->content = $issued_document->getParsedContent($customer);
        // Salvar a documento
        $issued_document->save();

        // Montar mensagem
        $message =
            $isEditingExistingIssuedDocument ?
                'Documento %s editado com sucesso' :
                'Documento %s criado com sucesso';

        // Formatar
        $message = sprintf($message, $issued_document->title);

        // Enviar flash
        Flash::success($message);

        // Redirecionar pra listagem de documentos
        return redirect()->route('get_customers_view', ['customer' => $customer->id, 'tab' => 'issued-documents']);
    }
}