<?php

namespace App\Http\Controllers;

use App\Library\Flash;
use App\Mail\AdminNotifications\FeedbackReceived;
use App\Models\Customer;
use App\Models\IssuedDocument;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class SiteController extends Controller
{

    /**
     * Function para abrir a homepage
     * @return Factory|View
     */
    public function get_site_homepage()
    {
        return view('website.website');
    }


    /**
     * Mostrar a dashboard
     * @return Factory|View
     */
    public function get_dashboard()
    {
        $latest_issued_documents = IssuedDocument::resourceIsolated()->orderBy('id', 'DESC')->take(5)->get();
        $latest_customers = Customer::resourceIsolated()->orderBy('id', 'DESC')->take(5)->get();
        $issued_documents_datatable = IssuedDocument::createdPerMonthDataTable();
        $customers_datatable = Customer::createdPerMonthDataTable();

        return view('dashboard', [
            'latest_issued_documents'           => $latest_issued_documents,
            'latest_customers'                  => $latest_customers,
            'issued_documents_datatable'        => $issued_documents_datatable,
            'customers_datatable'               => $customers_datatable,
        ]);
    }

    /**
     * Executar quando o webhook do gitlab chamar
     */
    public function post_gitlab_update()
    {
        // Executar o comando, guardar na var response o stdout
        exec('cd /var/www/docgun && git pull', $response);
        // Dar um var dump pra mostrar no github
        var_dump($response);
    }

    public function post_send_feedback(Request $request)
    {
        if (! $request->filled('feedback_message')) {
            Flash::error('Por favor, preencha a mensagem de feedback.');
            return back();
        }

        if (! auth()->check()) {
            Flash::error('Erro interno no sistema. Por favor, tente mais tarde.');
            return back();
        }

        notifySystemAdmins(new FeedbackReceived(auth()->user(), $request->get('feedback_message')));

        Flash::success('Obrigado! Seu feedback foi enviado com sucesso.');
        return back();
    }

    public function get_teste()
    {
        return view('errors.500');
    }
}
