<?php

namespace App\Http\Controllers;

use App\Models\Subscription;
use Illuminate\Http\Request;

class SubscriptionsController extends Controller{

    /**
     * Function para receber as notifications
     * @param Request $request
     * @param $provider
     * @return mixed
     */
    public function post_subscriptions_handle_notifications(Request $request, $provider)
    {
        // Achar o provider
        $provider = Subscription::getProviderByName($provider);
        // Se não achar o provider
        if( ! $provider){
            // Abandonar a missão
            return abort(403);
        }
        // Chamar o handle notification
        $provider::handleNotifications($request);
        // Retornar uma response OK, só pra fechar
        return 'Handling!';
    }

}