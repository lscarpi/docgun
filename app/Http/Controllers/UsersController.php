<?php

namespace App\Http\Controllers;

use App\Library\Flash;
use App\Models\User;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;
use Illuminate\View\View;

/**
 * Class UsersController
 * @package App\Http\Controllers
 */
class UsersController extends Controller
{

    /**
     * Listar usuários
     * @param Request $request
     * @return Factory|View
     */
    public function get_users(Request $request)
    {
        // Buscar no BD
        $users = User::resourceIsolated()->search($request)->paginate(20);
        // Mostrar a view
        return view('users.list', ['users' => $users]);
    }

    /**
     * Mostrar a tela de cadastro de usuários
     * @return Factory|View
     */
    public function get_users_create()
    {
        if (! Gate::allows('create', new User())) {
            return $this->notAuthorized();
        }

        if (! Gate::allows('addMoreUsers', User::class)) {
            Flash::error('Seu plano não permite a criação de novos usuários. Considere um Upgrade de Plano.');
            return back();
        }

        return view('users.create');
    }

    /**
     * Tentar cadastrar um usuário
     * @param Request $request
     * @return RedirectResponse
     */
    public function post_users_create(Request $request)
    {
        if (! Gate::allows('create', new User())) {
            return $this->notAuthorized();
        }

        if (! Gate::allows('addMoreUsers', User::class)) {
            Flash::error('Seu plano não permite a criação de novos usuários. Considere um Upgrade de Plano.');
            return back();
        }

        // Apenas alegria
        return $this->createOrEditUser($request);
    }

    /**
     * Mostrar a tela de edição de usuários
     * @param User $user
     * @return Factory|View
     */
    public function get_users_edit(User $user)
    {
        if (! Gate::allows('update', $user)) {
            return $this->notAuthorized();
        }

        return view('users.edit', ['user' => $user]);
    }

    /**
     * Tentar editar um usuário
     * @param Request $request
     * @param User $user
     * @return RedirectResponse
     */
    public function post_users_edit(Request $request, User $user)
    {
        // Apenas alegria
        return $this->createOrEditUser($request, $user);
    }

    /**
     * Tentar deletar um usuário
     * @param User $user
     * @return RedirectResponse
     * @throws Exception
     */
    public function get_users_delete(User $user)
    {

        // Se for o próprio usuário logado
        if ($user->id == auth()->user()->id) {
            // Nem pensar, vacilão
            Flash::error('Você não pode se excluir');
        // Se não for
        } else {
            // Sucesso
            Flash::success('Usuário ' . $user->name . ' excluído com sucesso.');
            // Deletar
            $user->delete();
        }
        // Jogar pra listagem de usuários
        return redirect()->route('get_users');
    }


    /**
     * Function responsible for creating or editing a user
     * @param Request $request
     * @param User|null $user
     * @return RedirectResponse
     */
    protected function createOrEditUser(Request $request, User $user = null)
    {

        // Criar var pra saber se estamos editando um usuário ou criando um novo
        $isEditingExistingUser = ! empty($user);

        // Validação de dados ========================

        // Criar uma regra de unique email
        $unique_email_rule = Rule::unique('users');

        // Se estiver editando user
        if ($isEditingExistingUser) {
            // Então ignoramos o id dele
            // Quando editamos um usuário, o email dele não pode ser considerado para a validação de unique
            // Uma vez que não queremos criar um novo usuário, mas sim editar o existente
            $unique_email_rule->ignore($user->id);
        }

        // Regras básicas
        $rules = [
            'name' => 'required',
            'email' => ['required', 'email', $unique_email_rule],
            'password' => ['confirmed'],
        ];

        // Se não estiver editando ninguém, estamos criando
        if (! $isEditingExistingUser) {
            // Precisamos forçar a criação da senha
            $rules['password'][] = 'required';
            $rules['password_confirmation'] = 'required';
        }

        // Passar a validation
        $this->validate($request, $rules, [
            'name.required' => 'O nome do usuário é obrigatório',
            'email.required' => 'O email do usuário é obrigatório',
            'email.unique' => 'O email escolhido já está em uso por outro usuário',
            'password.required' => 'A senha é obrigatória',
            'password.confirmed' => 'As senhas não conferem',
            'password_confirmation.required' => 'A confirmação de senha é obrigatória',

        ]);

        // Chegou aqui, deu bom

        // Ação ========================

        // Aqui vamos fazer o seguinte
        // Se na function tiver um user passado como parametro, usa ele
        // Senão, cria um novo
        $user = $user ? $user : new User();
        // Preencher com a request
        $user->fill($request->all());
        // Colocar o company ID se não tiver já (evitar overwrite desnecessário)
        if (! $user->company_id) {
            $user->company_id = auth()->user()->getCurrentCompanyId();
        }

        // Se não foi preenchido o stats do user
        if (! $user->status) {
            // Setar como active
            $user->status = User::USER_STATUS_ACTIVE;
        }

        // O password não é necessário se estivermos editando o user, uma vez
        // que não precisa ficar mexendo na senha toda hora
        if ($request->filled('password')) {
            // Aí se tiver preenchido, mudaremos
            $user->password = bcrypt($request->password);
        }

        // Salvar o user
        $user->save();

        // Montar mensagem
        $message =
            $isEditingExistingUser ?
                'Usuário %s editado com sucesso' :
                'Usuário %s criado com sucesso';

        // Formatar
        $message = sprintf($message, $user->name);

        // Enviar flash
        Flash::success($message);

        // Redirecionar pra listagem de users
        return redirect()->route('get_users');
    }
}
