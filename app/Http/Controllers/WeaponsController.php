<?php

namespace App\Http\Controllers;

use App\Library\Flash;
use App\Models\Customer;
use App\Models\Document;
use App\Models\Weapon;
use Illuminate\Http\Request;

class WeaponsController extends Controller{


    /**
     * Mostrar a tela de cadastro de armas
     * @param Customer $customer
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_customers_weapons_create(Customer $customer)
    {
        //Buscar no BD - verificar com Lucas se é isso mesmo
        $weapons = Weapon::resourceIsolated()->get();

        //Mostrar a view
        return view('customers.weapons.create', [
            'customer' => $customer,
        ]);
    }

    /**
     * Tentar cadastrar um arma
     * @param Request $request
     * @param Customer $customer
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function post_customers_weapons_create(Request $request, Customer $customer)
    {
        // Apenas alegria
        return $this->createOrEditWeapon($request, $customer);
    }

    /**
     * Mostrar a tela de detalhes de uma arma
     * @param Customer $customer
     * @param Weapon $weapon
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_customers_weapons_details(Customer $customer, Weapon $weapon)
    {
        return view('customers.weapons.details', [
            'customer' => $customer,
            'weapon' => $weapon,
        ]);
    }

    /**
     * Mostrar a tela de edição de armas
     * @param Customer $customer
     * @param Weapon $weapon
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_customers_weapons_edit(Customer $customer, Weapon $weapon)
    {
        return view('customers.weapons.edit', [
            'customer' => $customer,
            'weapon' => $weapon,
        ]);
    }

    /**
     * Tentar editar um arma
     * @param Request $request
     * @param Customer $customer
     * @param Weapon $weapon
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function post_customers_weapons_edit(Request $request, Customer $customer, Weapon $weapon)
    {
        // Apenas alegria
        return $this->createOrEditWeapon($request, $customer, $weapon);
    }

    /**
     * Tentar deletar um arma
     * @param Customer $customer
     * @param Weapon $weapon
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function get_customers_weapons_delete(Customer $customer, Weapon $weapon)
    {
        // Sucesso
        Flash::success('Arma ' . $weapon->name . ' excluída com sucesso.');
        // Deletar
        $weapon->delete();
        // Jogar pra listagem de armas
        return redirect()->route('get_customers_weapons');
    }


    /**
     * @param Request $request
     * @param Weapon|null $weapon
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    protected function createOrEditWeapon(Request $request, Customer $customer, Weapon $weapon = null)
    {

        // Criar var pra saber se estamos editando um arma ou criando um novo
        $isEditingExistingWeapon = ! empty($weapon);

        // Validação de dados ========================
        // Passar a validation
        $this->validate($request, Weapon::rules(), Weapon::validationMessages());
        // Chegou aqui, deu bom

        // Ação ========================

        // Aqui vamos fazer o seguinte
        // Se na function tiver um user passado como parametro, usa ele
        // Senão, cria um novo
        $weapon = $weapon ? $weapon : new Weapon();
        // Preencher com a request
        $weapon->fill($request->all());
        // Setar o customer, né
        $weapon->customer_id = $customer->id;

        // As Datas dos itens solicitados
        $weapon->docs_issued_at                 = createDateFromBRFormat($request->get('docs_issued_at'));
        $weapon->transit_permission_expiry_date = createDateFromBRFormat($request->get('transit_permission_expiry_date'));


        // Salvar a arma
        $weapon->save();

        // Montar mensagem
        $message =
            $isEditingExistingWeapon ?
                'Arma %s editada com sucesso' :
                'Arma %s criada com sucesso';

        // Formatar
        $message = sprintf($message, $weapon->name);

        // Enviar flash
        Flash::success($message);

        // Redirecionar pra listagem de armas
        return redirect()->route('get_customers_view', ['customer' => $customer->id, 'tab' => 'weapons']);
    }
}