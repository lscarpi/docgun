<?php

namespace App\Http\Middleware;

use App\Library\Flash;
use Closure;

class CheckIfItsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->isAdmin()) {
            return $next($request);
        }else{
            Flash::error('Você não está autorizado a executar esta ação.');
            return redirect()->route('get_users');
        }
    }
}
