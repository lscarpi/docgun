<?php

namespace App\Http\Middleware;

use App\Library\Flash;
use Closure;

class VerifyCompanySubscription
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        // Checar se a empresa possui uma subscription
        if( ! auth()->user()->company->isSubscriptionValid()){
            // Show
            Flash::error('A Empresa referente a este usuário não possui uma assinatura ativa.');
            // Tirar o cara do sistema
            auth()->logout();
            // Jogar o cara pro get login
            return redirect()->route('get_login');
        }else{
            return $next($request);
        }

    }
}
