<?php

namespace App\Http\Middleware;

use App\Library\Flash;
use Closure;

class VerifyCurrentLoggedInUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $session_id = session()->getId();
        $current_logged_in_session = auth()->user()->getCurrentLoggedInSession();

        // Se a session do navegador da request for direfente da session do usuário (no BD)
        if( $session_id != $current_logged_in_session ){
            // Show
            Flash::error('Este usuário está logado em outro terminal.');
            // Tirar o cara do sistema
            auth()->logout();
            // Jogar o cara pro get login
            return redirect()->route('get_login');
        }else{
            return $next($request);
        }

    }
}
