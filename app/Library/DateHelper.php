<?php namespace App\Library;

use Carbon\Carbon;
use Exception;

class DateHelper{

    const INTERVAL_TYPE_SECOND  = 'second';
    const INTERVAL_TYPE_MINUTE  = 'minute';
    const INTERVAL_TYPE_HOUR    = 'hour';
    const INTERVAL_TYPE_DAY     = 'day';
    const INTERVAL_TYPE_WEEK    = 'week';
    const INTERVAL_TYPE_MONTH   = 'month';
    const INTERVAL_TYPE_YEAR    = 'year';

    const INTERVAL_TYPES = [
        self::INTERVAL_TYPE_SECOND  => true,
        self::INTERVAL_TYPE_MINUTE  => true,
        self::INTERVAL_TYPE_HOUR    => true,
        self::INTERVAL_TYPE_DAY     => true,
        self::INTERVAL_TYPE_WEEK    => true,
        self::INTERVAL_TYPE_MONTH   => true,
        self::INTERVAL_TYPE_YEAR    => true,
    ];


    /**
     * Base function que cria uma data Carbon a partir de uma string e um formato
     * @param $dateString
     * @param $format
     * @return Carbon|null
     */
    protected static function createFromFormat($dateString, $format) : ? Carbon
    {
        try {
            return Carbon::createFromFormat($format, $dateString);
        }catch(Exception $e){
            return null;
        }
    }

    /**
     * Function que tenta criar um datetime, senão der certo tenta criar uma date, se não der retorna null
     * @param $dateString
     * @param null $format
     * @return Carbon|null
     */
    public static function createFromDynamicFormat($dateString, $format = null) : ? Carbon
    {
        // Tentar criar o datetime
        $date = self::createDateTimeFromFormat($dateString, $format);

        // Se não der
        if(! $date){
            // Tentar criar o date
            $date = self::createDateFromFormat($dateString, $format);
        }
        // Retornar a date ou null
        return $date;
    }

    /**
     * Function que cria uma data Carbon a partir de uma string e um formato
     * Se o formato não for informado, vamos puxar da lang
     * @param $dateString
     * @param null $format
     * @return Carbon|null
     */
    public static function createDateFromFormat($dateString, $format = null) : ? Carbon
    {
        // Se o formato não for passado
        if(! $format){
            // Puxamos do Lang
            $format = config('docgun.formats.raw.date');
        }
        // Retornar a base function
        return self::createFromFormat($dateString, $format);
    }


    /**
     * Function que cria uma data e hora Carbon a partir de uma string e um formato
     * Se o formato não for informado, vamos puxar da lang
     * @param $dateString
     * @param null $format
     * @return Carbon|null
     */
    public static function createDateTimeFromFormat($dateString, $format = null) : ? Carbon
    {
        // Se o formato não for passado
        if(! $format){
            // Puxamos do Lang
            $format = config('docgun.formats.raw.datetime');
        }
        // Retornar a base function
        return self::createFromFormat($dateString, $format);
    }


    /**
     * Function para printar uma carbon date
     * @param Carbon $date
     * @param $format
     * @return string
     */
    protected static function formatLocalized(Carbon $date, $format) : string
    {
        // Tentar
        try{
            // Printar no formato desejado
            return $date->formatLocalized($format);
            // Se falhar
        }catch(Exception $e){
            // Jogar um format padrão do sistema
            return $date->toDateTimeString();
        }
    }

    /**
     * Function para printar uma date dinamicamente com a lang
     * @param Carbon $date
     * @param null $format
     * @return string
     */
    public static function toDateString(Carbon $date = null, $format = null) : string
    {
        //Se a instânica de Carbon for nula significa que o user ou outra data do banco está vazia.
        if (!$date) return "";

        // Se o formato não for passado
        if(! $format){
            // Puxamos do Lang
            $format = config('docgun.formats.carbon.date');
        }
        // Retornar a base function
        return self::formatLocalized($date, $format);
    }

    /**
     * Function para printar um date time dinamicamente com a lang
     * @param Carbon $date
     * @param null $format
     * @return string
     */
    public static function toDateTimeString(Carbon $date, $format = null) : string
    {
        // Se o formato não for passado
        if(! $format){
            // Puxamos do Lang
            $format = config('docgun.formats.carbon.datetime');
        }
        // Retornar a base function
        return self::formatLocalized($date, $format);
    }

    /**
     * Function que gera datas dentro do start e end, com base no intervalo e amount
     * @param Carbon $dateStart
     * @param Carbon $dateEnd
     * @param $intervalType
     * @param $intervalAmount
     * @return array
     */
    public static function generateDatesInRange(Carbon $dateStart, Carbon $dateEnd, $intervalType, $intervalAmount) : array
    {
        // Se não for um interval válido
        if(! self::isValidIntervalType($intervalType)){
            // Abandona
            return [];
        }

        // Criar um cursor
        $dateCursor = $dateStart->clone();

        // Criar a array de retorno
        $dates = [];

        // Enquanto o cursor for menor que a data final
        while($dateCursor <= $dateEnd){
            // Adicionamos um clone do cursor na array
            $dates[] = $dateCursor->clone();
            // Incrementamos o cursor
            self::incrementDate($dateCursor, $intervalType, $intervalAmount);
        }
        // Retornamos a array já geradinha
        return $dates;
    }


    /**
     * Function que incrementa uma data em X de acordo com o tipo do intervalo e a quantidade
     * @param Carbon $date
     * @param $intervalType
     * @param $intervalAmount
     * @return Carbon
     */
    public static function incrementDate(Carbon $date, $intervalType, $intervalAmount) : Carbon
    {
        // Switch no tipo do interval
        // E para cada tipo, add um amount
        switch($intervalType){
            case self::INTERVAL_TYPE_SECOND:
                return $date->addSeconds($intervalAmount);
            case self::INTERVAL_TYPE_MINUTE:
                return $date->addMinutes($intervalAmount);
            case self::INTERVAL_TYPE_HOUR:
                return $date->addHours($intervalAmount);
            case self::INTERVAL_TYPE_DAY:
                return $date->addDays($intervalAmount);
            case self::INTERVAL_TYPE_WEEK:
                return $date->addWeeks($intervalAmount);
            case self::INTERVAL_TYPE_MONTH:
                return $date->addMonths($intervalAmount);
            case self::INTERVAL_TYPE_YEAR:
                return $date->addYears($intervalAmount);
            // Caso não seja dos acima
            default:
                // Retornar a date normal
                return $date;
        }
    }

    /**
     * Function que determina se o type do interval é válido
     * @param $intervalType
     * @return bool
     */
    protected static function isValidIntervalType($intervalType) : bool
    {
        return isset(self::INTERVAL_TYPES[$intervalType]);
    }


    /**
     * Function que gera uma array com os intervals e labels respectivos
     * @param array $ignoredIntervals
     * @return array
     */
    public static function intervalsToSelect($ignoredIntervals = []) : array{
        // Criar a array de response
        $response = [];

        // Loop nos interval types
        foreach(array_keys(self::INTERVAL_TYPES) as $interval){
            // Se o interval tiver nos ignorados
            if(in_array($interval, $ignoredIntervals)){
                // Abandona
                continue;
            }
            // Se não tiver, adicionar na response
            $response[] = ['value' => $interval, 'name' => config('docgun.formats.date_intervals.' . $interval)];
        }
        // Retornar a array pronta
        return $response;
    }

}
