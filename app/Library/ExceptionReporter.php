<?php namespace App\Library;

use App\Library\GitLab\IssueHandler;
use App\Mail\AdminNotifications\SystemErrorDetected;
use Exception;

/**
 * Class ExceptionReporter
 * @package App\Library
 */
class ExceptionReporter
{
    /**
     * @param \Throwable $e
     */
    public static function handle(\Throwable $e)
    {

        // Notify system admins
        notifySystemAdmins(new SystemErrorDetected($e));
        // URI
        $uri = request()->getUri();
        // Integrate into version control
        IssueHandler::handleException($e, $uri);
    }
}
