<?php

namespace App\Library;

/**
 * Essa classe fará a gestão daqueles alertas que aparecem uma única vez na tela do usuário
 * Class Flash
 * @package App\Library
 */
class Flash{

    /**
     * Essa função salvará na session o flash, colocando um objeto guardado
     * @param $type
     * @param $message
     */
    protected static function create($type, $message)
    {
        $object = new \stdClass();
        $object->type = $type;
        $object->message = $message;
        return session()->flash('flash-message', $object);
    }

    /**
     * Helper para sucesso
     * @param $message
     */
    public static function success($message)
    {
        return self::create('success', $message);
    }

    /**
     * Helper para erro
     * @param $message
     */
    public static function error($message)
    {
        return self::create('danger', $message);
    }

    /**
     * Helper para info
     * @param $message
     */
    public static function info($message)
    {
        return self::create('info', $message);
    }

    /**
     * Helper para warning
     * @param $message
     */
    public static function warning($message)
    {
        return self::create('warning', $message);
    }

}