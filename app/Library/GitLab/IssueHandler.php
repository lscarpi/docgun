<?php namespace App\Library\GitLab;

use Carbon\Carbon;
use Gitlab\Client;
use Gitlab\Model\Issue;
use Gitlab\Model\Project;
use Illuminate\Support\Str;

/**
 * Class IssueHandler
 * @package App\Library\GitLab
 */
class IssueHandler
{

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Project
     */
    protected $project;

    /**
     * @var Issue[]
     */
    protected $issues;

    const ISSUE_TITLE = "Auto Issuer - Exception: %s";
    const ISSUE_DESCRIPTION =
<<<DESCRIPTION

Issue gerada automaticamente pelo docGun Auto Issuer.

# Details:
```
Data/Hora: %s
URL: %s
File: %s
Line: %s
Class: %s
Message: %s
Trace:
%s
```

### Obs.: Não editar a seção "Details" pois a mesma é utilizada para verificações do sistema.

DESCRIPTION;

    const ISSUE_LABELS = 'bug,to-do,high-priority';


    /**
     * Método responsável por receber uma exception do sistema e jogar pro gitlab se necessário
     * @param \Throwable $exception
     * @param $uri
     */
    public static function handleException(\Throwable $exception, $uri)
    {
        $issue_handler = new self();
        if (! $issue_handler->issueExists($exception)) {
            $issue_handler->createIssue($exception, $uri);
        }
    }


    public function __construct()
    {
        // Load nas issues
        $this->getIssues();
    }

    /**
     * @return Client
     */
    private function getClient()
    {
        if (! $this->client) {
            $this->client = Client::create(env('GITLAB_CLIENT_URL'))->authenticate(env('GITLAB_ACCESS_TOKEN'), Client::AUTH_URL_TOKEN);
        }

        return $this->client;
    }

    /**
     * @return Project
     */
    private function getProject()
    {
        if (! $this->project) {
            $projects = $this->getClient()->projects()->all(['search' => 'docgun']);
            foreach ($projects as $project) {
                if ($project['name'] == 'docgun') {
                    $this->project = $project;
                }
            }
        }
        return $this->project;
    }

    /**
     * @return Issue[]
     */
    private function getIssues()
    {
        if (! $this->issues) {
            $project = $this->getProject();
            $this->issues = $this->getClient()->issues()->all($project['id']);
        }
        return $this->issues;
    }

    /**
     * @param \Throwable $exception
     * @return bool
     */
    private function issueExists(\Throwable $exception)
    {
        foreach ($this->getIssues() as $issue) {
            $description = $issue['description'];

            if ($this->descriptionMatches($description, $exception)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param \Throwable $exception
     * @param $uri
     * @return mixed
     */
    private function createIssue(\Throwable $exception, $uri)
    {
        $project = $this->getProject();
        $response = $this->getClient()->issues()->create($project['id'], [
            'title' => Str::limit(sprintf(self::ISSUE_TITLE, $exception->getMessage()), 252),
            'description' => sprintf(self::ISSUE_DESCRIPTION, Carbon::now()->formatLocalized('%d/%m/%Y as %H:%M:%S'), $uri, $exception->getFile(), $exception->getLine(), get_class($exception), $exception->getMessage(), $exception->getTraceAsString()),
            'labels' => self::ISSUE_LABELS,
        ]);

        return $response;
    }

    /**
     * @param $needle
     * @param $haystack
     * @return null | string
     */
    private function findDescriptionItem($needle, $haystack)
    {
        $needle_regex = "/$needle\:\s(.*)/";
        if (preg_match($needle_regex, $haystack, $needle_match)) {
            return $needle_match[1];
        }
        return null;
    }

    /**
     * @param $description
     * @param \Throwable $exception
     * @return bool
     */
    private function descriptionMatches($description, \Throwable $exception)
    {
        $items_to_check = [
            'File' => $exception->getFile(),
            'Line' => $exception->getLine(),
            'Message' => $exception->getMessage(),
            'Class' => get_class($exception)
        ];

        foreach ($items_to_check as $item_to_check => $exception_value) {
            $item_value = $this->findDescriptionItem($item_to_check, $description);
            if ($item_value != $exception_value) {
                return false;
            }
        }
        // Se chegar até aqui, tudo bateu ok
        return true;
    }
}
