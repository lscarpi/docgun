<?php namespace App\Library;

class JSONResponse{

    /**
     * Function para criar uma JSON response padronizada
     * @param bool $success
     * @param null $message
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public static function create($success = true, $message = null, $data = [])
    {
        $final_data = [
            'success' => $success,
            'message' => $message,
            'data' => $data
        ];

        return response()->json($final_data);
    }

    /**
     * Helper pra adiantar a vida
     * @param null $message
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public static function success($message = null, $data = [])
    {
        return self::create(true, $message, $data);
    }

    /**
     * Helper pra adiantar a vida
     * @param null $message
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public static function error($message = null, $data = [])
    {
        return self::create(false, $message, $data);
    }
}