<?php namespace App\Library\MarketingAutomation;

use App\Mail\AdminNotifications\SystemErrorDetected;
use App\Models\User;
use Exception;
use GuzzleHttp\Client;
use stdClass;

/**
 * Class villeTarget
 * @package App\Models\Integradores
 */
class VilleTarget
{
    const REQUEST_METHOD_GET = 1;
    const REQUEST_METHOD_POST = 2;
    const REQUEST_METHOD_PUT = 3;
    const REQUEST_METHOD_PATCH = 4;
    const REQUEST_METHOD_DELETE = 5;

    /**
     * Function responsible
     * @param $method
     * @param $uri
     * @param array $form_params
     * @return mixed
     */
    private function request(string $method, string $uri, array $form_params = []) : ? stdClass
    {
        $client = new Client(['headers' => [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . config('docgun.general.marketing_automation.ville_target.api_key')
        ]]);

        switch ($method) {
            default:
            case self::REQUEST_METHOD_GET:
                $uri .= '?' . http_build_query($form_params);
                $response = $client->get($uri);
                break;
            case self::REQUEST_METHOD_POST:
                $response = $client->post($uri, ['form_params' => $form_params]);
                break;
            case self::REQUEST_METHOD_PUT:
                $uri .= '?' . http_build_query($form_params);
                $response = $client->put($uri);
                break;
            case self::REQUEST_METHOD_PATCH:
                $uri .= '?' . http_build_query($form_params);
                $response = $client->patch($uri);
                break;
            case self::REQUEST_METHOD_DELETE:
                $uri .= '?' . http_build_query($form_params);
                $response = $client->delete($uri);
                break;
        }
        return json_decode($response->getBody()->getContents());
    }

    /**
     * Function responsible for sending a user to ville Target
     * @param $listUid
     * @param User $user
     * @return bool
     */
    public function subscribeUser(string $listUid, User $user) : bool
    {

        // The route to create the subscriber
        $route = "https://app.villetarget.com/api/v1.1/subscribers/create";

        // Try
        try {
            // Send the request
            $response = $this->request(self::REQUEST_METHOD_POST, $route, [
                // The data
                'list' => $listUid,
                'name' => $user->name,
                'email' => $user->email,
            ]);

            // Define if it was successfull
            $success =
                // If the success property exists
                property_exists($response, 'success') &&
                // And if it was success
                $response->success &&
                // And if there is some data
                property_exists($response, 'data');

            // If not success
            if (! $success) {
                // Generate the new exception (but dont throw it)
                $exception = new Exception(json_encode($response));
                // Notify the sys admins
                notifySystemAdmins(new SystemErrorDetected($exception));
            }

            // Return wether was a success or not
            return $success;
            // If any error
        } catch (Exception $e) {
            // False
            return false;
        }
    }
}
