<?php namespace App\Library;

use Illuminate\Support\Facades\File;

/**
 * Class RouteLoader
 * Responsável por carregar rotas recursivamente para o sistema
 * @package App\Library
 */
class RouteLoader
{
    /**
     * Function recursiva que importa o routes directory
     * @param string $directory
     */
    public static function loadRoutesFromDirectory($directory) : void
    {
        // Loop no scandir do path
        foreach (File::allFiles($directory) as $scannedItem) {
            // Importar
            require_once $scannedItem->getPathname();
        }
    }
}
