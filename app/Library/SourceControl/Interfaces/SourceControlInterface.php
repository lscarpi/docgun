<?php

namespace App\Library\SourceControl\Interfaces;

use Illuminate\Http\Request;

interface SourceControlInterface{

    public function handleWebhook(Request $request);

}