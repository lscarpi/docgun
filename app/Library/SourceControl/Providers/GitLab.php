<?php
namespace App\Library\SourceControl\Providers;

use Illuminate\Http\Request;
use App\Library\SourceControl\Interfaces\SourceControlInterface;

class GitLab implements SourceControlInterface{

    const OBJECT_KIND_PUSH              = 'push';
    const OBJECT_KIND_TAG               = 'tag_push';
    const OBJECT_KIND_ISSUE             = 'issue';
    const OBJECT_KIND_COMMENT           = 'note';
    const OBJECT_KIND_MERGE_REQUEST     = 'merge_request';
    const OBJECT_KIND_WIKI_PAGE         = 'wiki_page';
    const OBJECT_KIND_PIPELINE          = 'pipeline';
    const OBJECT_KIND_BUILD             = 'build';

    const BRANCH_MASTER     = 'master';
    const BRANCH_DEVELOP    = 'develop';

    const MERGE_REQUEST_STATUS_OPENED = 'opened';
    const MERGE_REQUEST_STATUS_CLOSED = 'closed';
    const MERGE_REQUEST_STATUS_LOCKED = 'locked';
    const MERGE_REQUEST_STATUS_MERGED = 'merged';


    /**
     * Function para atualizar o sistema dependendo da .env dele
     * @param Request $request
     * @return array|void
     */
    public function handleWebhook(Request $request){
        // Se não tiver o kind é inválido
        if(! $request->has('object_kind')){
            abort(404);
        }
        // Obter o kind
        $object_kind = $request->get('object_kind');
        // Switch no kind
        switch($object_kind){
            // Se for uma push request
            case self::OBJECT_KIND_PUSH:
                return $this->handlePushRequest($request);
            case self::OBJECT_KIND_MERGE_REQUEST:
                return $this->handleMergeRequest($request);
            // Otherwise
            default:
                // Abandona
                return abort(404);
        }
    }

    /**
     * Controlar as push requests do Webhook
     * @param Request $request
     * @return array
     */
    private function handlePushRequest(Request $request){
        // Obter o after e o ref
        $after = $request->get('after'); // After é o código que marca o ponto em que o branch está, é o id do último commit
        $ref =  $request->get('ref'); // Ref é o branch de reference
        // Se não tiver after ou ref
        if(! $after || ! $ref){
            // Azedou
            return abort(404);
        }
        // Pegar o target branch
        $target_branch = $this->getTargetBranchFromRef($ref);
        // Retornar o pull changes
        return $this->pullChanges($target_branch, $after);
    }

    /**
     * Function para pegar o branch que estamos trabalhando a partir da ref informada
     * @param $ref
     * @return string|null
     */
    private function getTargetBranchFromRef($ref)
    {
        // O prefix de toda ref
        $prefix = 'refs/heads/';
        // Se tiver o prefix
        if(starts_with($ref, $prefix)){
            // Apenas retira e retorna o que sobrar
            return str_replace($prefix, '', $ref);
        }
        // Se não tiver, não é uma ref
        return null;
    }

    /** Function para rodar a merge request
     * @param Request $request
     * @return array
     */
    private function handleMergeRequest(Request $request)
    {
        // Obter object attributes
        $object_attribtues = $request->get('object_attributes');
        // Se não tiver os itens abaixo
        if(     ! $object_attribtues
            &&  ! isset($object_attribtues['merge_status'])
            ||  ! isset($object_attribtues['target_branch'])
            || ! isset($object_attribtues['last_commit'])
        ){
            // Abandona
            return abort(404);
        }
        // Passar as vars
        $merge_status       = $object_attribtues['merge_status'];
        $target_branch      = $object_attribtues['target_branch'];
        $last_commit        = $object_attribtues['last_commit'];
        // Se a Merge Request está merged
        if($merge_status == self::MERGE_REQUEST_STATUS_MERGED){
            // Puxar as changes
            return $this->pullChanges($target_branch, $last_commit['id']);
        }
        // Caso contrário, abandona
        return ['Nothing to do'];

    }


    private function pullChanges($target_branch, $last_commit)
    {
        // Se for production server e o push foi no master
        if(isProductionServer() && $target_branch == self::BRANCH_MASTER){
            // Atualizar qual é after atual, para que os outros servers do cluster possam saber
            exec('cd /var/www/html/docgun && git pull origin master', $exec_response);
            // Se foi no testing server e o push foi no develop
        }else if(isTestServer() && $target_branch == self::BRANCH_DEVELOP){
            exec('cd /var/www/docgun-testing-stage && git pull origin develop', $exec_response);
            // Se não for nada disso
        }else{
            // Abandona
            return abort(404);
        }
    }
}
