<?php namespace App\Library\SourceControl;

use Illuminate\Http\Request;
use App\Library\SourceControl\Exceptions\InvalidHandlerException;
use App\Library\SourceControl\Interfaces\SourceControlInterface;

class UpdateHandler{


    /**
     * @param Request $request
     * @return mixed
     * @throws InvalidHandlerException
     */
    public static function handle(Request $request)
    {
        $current_source_control_handler = config('docgun.general.error_reporting.current_version_control_handler');
        $handler = new $current_source_control_handler();

        if($handler instanceof SourceControlInterface){
            return $handler->handleWebhook($request);
        }

        throw new InvalidHandlerException('Handler inválido!');
    }

}