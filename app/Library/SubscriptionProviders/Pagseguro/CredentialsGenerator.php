<?php namespace App\Library\SubscriptionProviders\Pagseguro;


use PHPSC\PagSeguro\Credentials;
use PHPSC\PagSeguro\Environments\Production;
use PHPSC\PagSeguro\Environments\Sandbox;

class CredentialsGenerator{

    /**
     * Function para gerar as credentials da sandbox
     * @return Credentials
     */
    protected static function getSandboxCredentials(): Credentials
    {
        return new Credentials(
            env('PAGSEGURO_EMAIL'),
            env('PAGSEGURO_SANDBOX_TOKEN'),
            new Sandbox()
        );
    }

    /**
     * Function para gerar as credentials da production
     * @return Credentials
     */
    protected static function getProductionCredentials() : Credentials
    {
        return new Credentials(
            env('PAGSEGURO_EMAIL'),
            env('PAGSEGURO_PRODUCTION_TOKEN'),
            new Production()
        );
    }

    /**
     * Function que gera as credentials de forma adequada
     * @return Credentials
     */
    public static function getCredentials() : Credentials
    {
        // Retornar de acordo com o env
        return isProductionServer() ?
            self::getProductionCredentials() :
            self::getSandboxCredentials();
    }



}