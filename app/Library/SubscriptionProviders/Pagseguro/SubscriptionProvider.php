<?php namespace App\Library\SubscriptionProviders\Pagseguro;

use App\Models\Subscription;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use PHPSC\PagSeguro\Client\PagSeguroException;
use PHPSC\PagSeguro\Purchases\Subscriptions\Locator as SubscriptionLocator;
use PHPSC\PagSeguro\Purchases\Subscriptions\Subscription as PagSeguroSubscription;
use PHPSC\PagSeguro\Purchases\Subscriptions\SubscriptionService;
use PHPSC\PagSeguro\Purchases\Transactions\Locator as TransactionLocator;
use PHPSC\PagSeguro\Requests\PreApprovals\Period;
use PHPSC\PagSeguro\Requests\PreApprovals\PreApprovalService;

/**
 * Class SubscriptionProvider
 * @package App\Library\SubscriptionProviders\Pagseguro
 */
class SubscriptionProvider
{


    /**
     * Function que cria a subscrption
     * @param Subscription $subscription
     * @param string $period
     * @return string|null
     */
    public static function createSubscription(
        Subscription $subscription,
        $period = Subscription::SUBSCRIPTION_BILLING_CYCLE_MONTHLY
    ) : ?string {
        // Criar o service
        $service = new PreApprovalService(CredentialsGenerator::getCredentials());

        // Criar a request
        $request = $service
            ->createRequestBuilder(false)
            ->setName($subscription->plan->name)
            ->setPeriod(self::convertPeriod($period))
            ->setAmountPerPayment($subscription->calculateSubscriptionPrice())
            ->setReference($subscription->id)
            ->setFinalDate(Carbon::now()->addDays(3))
            ->setRedirectTo(route('get_account_subscription_thankyou', $subscription->id))
            ->getRequest();

        // Aprovar
        $response = $service->approve($request);

        // Salvar o id da request na subscription no BD
        $subscription->provider_subscription_id = $response->getCode();
        $subscription->save();
        // Retornar pra URL
        return $response->getRedirectionUrl();
    }

    /**
     * Function para cancelar uma subscription
     * @param Subscription $subscription
     * @return bool
     */
    public static function cancelSubscription(Subscription $subscription) : bool
    {
        // Tentar
        try {
            // Criar  o service
            $service = new SubscriptionService(CredentialsGenerator::getCredentials());
            // Cancelar a sub
            $response = $service->cancel($subscription->provider_subscription_id);
            // Retornar se o satus está ok
            return $response->getStatus() == "ok";
            // Se zebrar
        } catch (PagSeguroException $e) {
            return true;
        }
    }

    /**
     * Function para achar a subscription no provider
     * @param Subscription $subscription
     * @return PagSeguroSubscription|null
     */
    public static function getSubscription(Subscription $subscription) : ? PagSeguroSubscription
    {
        // Tentar
        try {
            // Criar o locator
            $locator = new SubscriptionLocator(CredentialsGenerator::getCredentials());
            // Achar a sub e retornar
            return $locator->getByCode($subscription->provider_subscription_id);
            // Se zebrar
        } catch (PagSeguroException $e) {
            return null;
        }
    }

    /**
     * Function que converte o period do docgun em period do pagseguro
     * @param $docgunPeriod
     * @return string
     */
    protected static function convertPeriod($docgunPeriod) : string
    {
        // Switch no period
        switch ($docgunPeriod) {
            default:
            case Subscription::SUBSCRIPTION_BILLING_CYCLE_MONTHLY:
                return Period::MONTHLY;
            case Subscription::SUBSCRIPTION_BILLING_CYCLE_TRIMONTHLY:
                return Period::TRIMONTHLY;
            case Subscription::SUBSCRIPTION_BILLING_CYCLE_HALF_YEARLY:
                return Period::SEMESTRALLY;
            case Subscription::SUBSCRIPTION_BILLING_CYCLE_YEARLY:
                return Period::YEARLY;
        }
    }

    /**
     * Function que gerencia as notifications do pagseguro
     * @param Request $request
     */
    public static function handleNotifications(Request $request)
    {
        $credentials = CredentialsGenerator::getCredentials();
        try {
            $service = $request->get('notificationType') == 'preApproval'
                ? new SubscriptionLocator($credentials)
                : new TransactionLocator($credentials); // Cria instância do serviço de acordo com o tipo da notificação

            // Puxar a purchase
            $purchase = $service->getByNotification($request->get('notificationCode'));

            // Se for instancia de uma pagseguro subscription
            if ($purchase instanceof PagSeguroSubscription) {
                self::handlePagseguroSubscriptionNotification($purchase);
            }
        } catch (Exception $error) { // Caso ocorreu algum erro
            echo $error->getMessage(); // Exibe na tela a mensagem de erro
        }
    }


    /**
     * @param PagSeguroSubscription $pagSeguroSubscription
     */
    protected static function handlePagseguroSubscriptionNotification(PagSeguroSubscription $pagSeguroSubscription)
    {

        // Achar a subscription referente no docgun
        $subscription = Subscription::find($pagSeguroSubscription->getDetails()->getReference());

        // Se não achar a subscription
        if (! $subscription) {
            // Abandonar
            return;
        }

        // Se a subscription tiver active
        if ($pagSeguroSubscription->isActive()) {
            // Se a local estiver pendente
            if ($subscription->isPending()) {
                // Puxar a company
                $company = $subscription->company;
                // Puxar a subscription atual do titio
                $currentSubscription = $company->getCurrentSubscription();
                // Cancelar a subscription atual do cliente
                $currentSubscription->setTerminated();
                // Enviar email se quiser
            }
            // Aqui é executado mesmo se já existe a sub, ou seja, tava suspensa e voltou a ativa
            // Ativar a subscription
            $subscription->setActive();
        // Se a subscription tiver expired ou suspensa
        } elseif ($pagSeguroSubscription->isExpired() ||
            $pagSeguroSubscription->getDetails()->getStatus() == 'SUSPENDED') {
            // Suspender a subscription local
            $subscription->setSuspended();
        // Se a subscription tiver cancelled
        } elseif ($pagSeguroSubscription->isCancelledByAcquirer()
            || $pagSeguroSubscription->isCancelledByCustomer() || $pagSeguroSubscription->isCancelledByReceiver()) {
            // Terminar a subscription local
            $subscription->setTerminated();
        }
    }
}
