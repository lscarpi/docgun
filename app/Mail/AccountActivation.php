<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AccountActivation extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;

    /**
     * Create a new message instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('docGun - Ativação da Conta')
            ->view('emails.account_activation', [
            'user' => $this->user,
            'confirm_registration_url' => route('get_register_confirm_registration', $this->user->cacheGet('activation_token'))
        ]);
    }
}
