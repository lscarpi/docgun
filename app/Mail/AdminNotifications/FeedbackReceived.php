<?php

namespace App\Mail\AdminNotifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FeedbackReceived extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */


    protected $user, $feedback_message;

    public function __construct(User $user, $feedback_message)
    {
        $this->user                 = $user;
        $this->feedback_message     = $feedback_message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return
            $this
                ->subject('docGun - Feedback received from ' . $this->user->name)
                ->view('emails.admin_notifications.feedback_received', [
                    'user'                  => $this->user,
                    'feedback_message'      => $this->feedback_message,
                ]);
    }
}
