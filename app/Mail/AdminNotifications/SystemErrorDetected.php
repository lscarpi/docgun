<?php

namespace App\Mail\AdminNotifications;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class SystemErrorDetected
 * @package App\Mail\AdminNotifications
 */
class SystemErrorDetected extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $exception;

    /**
     * SystemErrorDetected constructor.
     * @param \Throwable $exception
     */
    public function __construct(\Throwable $exception)
    {
        $this->exception    = $exception;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return
            $this
                ->subject('docGun - New fish detected!')
                ->view('emails.admin_notifications.system_error_detected', [
                    'request_uri'               => request()->getUri(),
                    'exception_message'         => $this->exception->getMessage(),
                    'exception_file'            => $this->exception->getFile(),
                    'exception_line'            => $this->exception->getLine(),
                    'exception_trace_string'    => $this->exception->getTraceAsString(),
                    'exception_class'           => get_class($this->exception),
                ]);
    }
}
