<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $password;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param $newPassword
     */
    public function __construct(User $user, $newPassword)
    {
        $this->user = $user;
        $this->password = $newPassword;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('docGun - Reiniciar Senha')
            ->view('emails.reset_password', [
            'user' => $this->user,
            'password' => $this->password
            //'confirm_registration_url' => route('get_register_confirm_registration', $this->user->cacheGet('activation_token'))

        ]);
    }
}
