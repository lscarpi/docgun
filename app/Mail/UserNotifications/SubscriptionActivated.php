<?php

namespace App\Mail\UserNotifications;

use App\Models\Subscription;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RSubscriptionActivated extends Mailable
{
    use Queueable, SerializesModels;

    protected $user, $subscription;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param Subscription $subscription
     */
    public function __construct(User $user, Subscription $subscription)
    {
        $this->user = $user;
        $this->subscription = $subscription;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('docGun - Assinatura ativada')
            ->view('emails.user_notifications.subscription_activated', [
                'user' => $this->user,
                'subscription' => $this->subscription,
            ]);
    }
}
