<?php

namespace App\Models;

use App\Exceptions\CannotDeleteException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Club extends ResourceIsolatedModel{

    protected $casts = [
        'address' => 'object',
    ];

    protected $fillable = [
        'name', 'phone', 'person_in_charge', 'email',
    ];

    public function customers()
    {
        return $this->hasMany(Customer::class);
    }

    public static function rules()
    {
        return [
            'name'                  => 'required',
            'address_1'             => 'required',
            'address_number'        => 'required',
            'address_neighborhood'  => 'required',
            'address_city'          => 'required',
            'address_state'         => 'required',
            'address_zip'           => 'required',
            'email'                 => 'nullable|email',
        ];
    }

    public static function validationMessages()
    {
        return [
            'name.required'                     => 'O nome do clube é obrigatório',
            'address_1.required'                => 'O endereço do clube é obrigatório',
            'address_number.required'           => 'O número do endereço é obrigatório',
            'address_neighborhood.required'     => 'O bairro é obrigatório',
            'address_city.required'             => 'A cidade é obrigatória',
            'address_state.required'            => 'O estado é obrigatório',
            'address_zip.required'              => 'O CEP é obrigatório',
            'email.email'                       => 'O email informado é inválido',
        ];
    }

    // Override no método delete padrão
    public function delete()
    {
        // Checar se pode deletar
        if($this->customers()->count() > 0){
            // Se não puder, throw no erro
            throw new CannotDeleteException('Não é possível excluir um clube com clientes vinculados.');
        }
        // Retornar ok se der bom
        return parent::delete();
    }


    // Function para montar address do cliente
    public function maskAddressFull()
    {
        return defaultAddressFormat($this->address);
    }

    //Function para search na Club/List view
    public function scopeSearch($query, Request $request)
    {
        if($request->filled('busca_texto')){
            $query->where(function($query) use($request){
                $query->orWhere('name', 'like', '%' . $request->get('busca_texto') . '%');
                $query->orWhere('person_in_charge', 'like', '%' . $request->get('busca_texto') . '%');
            });
        }

        return $query;

    }
}



