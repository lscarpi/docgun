<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Company extends Model{

    const COMPANY_STATUS_ACTIVE = 'active';
    const COMPANY_STATUS_INACTIVE = 'inactive';

    protected $casts = [
        'address' => 'object',
    ];

    protected $fillable = [
        'name', 'legal_name', 'status', 'email', 'cpf_or_cnpj',
    ];

    //Define status de uma Company ao ser criada como ACTIVE
    public static function boot()
    {
        parent::boot();

        static::creating(function(self $company){
            $company->status = self::COMPANY_STATUS_ACTIVE;
        });
    }

    public function weapons()
    {
        return $this->hasMany(Weapon::class);
    }

    public function clubs()
    {
        return $this->hasMany(Club::class);
    }

    public function documents()
    {
        return $this->hasMany(Document::class);
    }

    public function issuedDocuments()
    {
        return $this->hasMany(IssuedDocument::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function customers()
    {
        return $this->hasMany(Customer::class);
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }

    /**
     * Function para achar o owner da company
     * @return User
     */
    public function findOwner()
    {
        return $this->users()->where('company_owner', true)->first();
    }

    /**
     * Function para verificar se a company possui um owner
     * @return bool
     */
    public function hasOwner()
    {
        return ! empty($this->findOwner());
    }

    /**
     * Helper para saber se a company está no trial
     * @return bool
     */
    public function isOnTrial()
    {
        return $this->getCurrentSubscription()->isTrial();
    }

    /**
     * Function responsável por obter a subscription ativa mais recente
     * @return Subscription|null
     */
    public function getCurrentSubscription()
    {
        return $this->subscriptions()->withSystemAccess()->orderBy('id', 'DESC')->first();
    }

    /**
     * Function helper para puxar todas as subscriptions de uma empresa
     * @return Subscription[]
     */
    public function getAllSubscriptions()
    {
        return $this->subscriptions;
    }

    public function isActive()
    {
        return $this->status == self::COMPANY_STATUS_ACTIVE;
    }

    // Function para montar address da company
    public function maskAddressFull()
    {
        return defaultAddressFormat($this->address);
    }

    /**
     * Function que checa se a subscription da empresa é válida.
     * @return bool
     */
    public function isSubscriptionValid()
    {
        // Buscar a subscription no BD
        $subscription = $this->getCurrentSubscription();
        // Se não achar
        if( ! $subscription){
            // Já é inválida
            return false;
        }
        // Se achar, retornar se ela permite o acesso ao sistema
        return $subscription->allowsAccess();
    }

    /**
     * Function para criar uma subscription de teste para a empresa se não tiver nenhuma já
     * @param Carbon|null $termination_date
     * @return bool
     */
    public function createTrialSubscription(Carbon $termination_date = null)
    {
        // Se essa company já possui uma subscription
        if($this->hasSubscription()){
            // Não pode criar outra
            return false;
        }

        // Se não possui, aí vai legal
        // Checar a data de término
        // Se não foi passado uma termination date
        if( ! $termination_date){
            $termination_date = now()->addDays(config('docgun.general.trial.period_in_days'));
        }

        // Criar uma nova sub
        $subscription = new Subscription();
        // Definir o plan id
        $subscription->plan_id = config('docgun.general.trial.plan_id');
        // É trial
        $subscription->trial = true;
        // E a data de término
        $subscription->terminate_at = $termination_date;
        // Informar que a subscription pertence a esta company
        $subscription->company_id = $this->id;

        // Retornar o save
        return $subscription->save();
    }

    /**
     * Function que checa se a empresa já possui uma subscription
     * @return bool
     */
    public function hasSubscription()
    {
        $subscription = $this->getCurrentSubscription();
        return $subscription ? true : false;
    }

    /**
     * Function que checa se a company pode solicitar upgrade/downgrade de plano
     * @return bool
     */
    public function canChangeSubscription()
    {
        return is_null($this->getPendingSubscription());
    }

    /**
     * Function que puxa uma subscription upgrade/downgrade pendente
     * @return mixed
     */
    public function getPendingSubscription()
    {
        return $this->subscriptions()->pending()->orderBy('id', 'DESC')->first();
    }

    /**
     * @param Plan $plan
     * @return Subscription|null
     */
    public function createNewSubscription(Plan $plan)
    {

        // Se a company não puder trocar a subscrption
        if( ! $this->canChangeSubscription()){
            // Abandonate
            return null;
        }

        // Criar a sub
        $subscription = new Subscription();
        $subscription->plan_id = $plan->id;
        $subscription->company_id = $this->id;
        $subscription->trial = $plan->isFree();

        // Se salvar
        if($subscription->save()){
            // Retornar a sub
            return $subscription;
        }
        // Otherwise peixe
        return null;
    }

    /**
     * Function que mostra o total de users atualmente cadastrados nesta company
     * @return int
     */
    public function usersCount()
    {
        return $this->users()->count();
    }

    /***
     * Function que informa o máximo de users que essa company pode criar
     * @return int
     */
    public function maxUsers()
    {
        // Se não tiver subscription
        if(! $this->hasSubscription()){
            // Zerão na lata
            return 0;
        }
        // Puxar a subscription
        $subscription = $this->getCurrentSubscription();
        // Puxar a resource, passando 0 como default se não achar
        return $subscription->getResourceLimit(Plan::RESOURCE_MAX_USERS, 0);
    }

    /**
     * Function que nos mostra quantos users ainda podemos cadastrar para esta company
     * @return int
     */
    public function usersRemaining()
    {
        return $this->maxUsers() - $this->usersCount();
    }


    public static function rules()
    {
        return [
            'name' => 'required',
        ];
    }

    public static function validationMessages()
    {
        return [
            'name.required' => 'O nome da empresa é obrigatório',
        ];
    }

    public function scopeSearch($query, Request $request)
    {
        if($request->filled('busca_texto')){
            $query->where(function($query) use($request){
                $query->orWhere('name', 'like', '%' . $request->get('busca_texto') . '%');
            });
        }

        return $query;

    }
}