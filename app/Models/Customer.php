<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class Customer extends ResourceIsolatedModel {

    const CR_CATEGORY_SHOOTER   = 'shooter';
    const CR_CATEGORY_HUNTER    = 'hunter';
    const CR_CATEGORY_COLLECTOR = 'collector';
    const CR_CATEGORY_MIXED     = 'mixed';

    const CR_CATEGORIES = [
        self::CR_CATEGORY_SHOOTER       => 'Atirador',
        self::CR_CATEGORY_HUNTER        => 'Caçador',
        self::CR_CATEGORY_COLLECTOR     => 'Colecionador',
        self::CR_CATEGORY_MIXED         => 'Misto',
    ];

    const CIVIL_STATUS_SINGLE               = 'single';
    const CIVIL_STATUS_MARRIED              = 'married';
    const CIVIL_STATUS_DIVORCED             = 'divorced';
    const CIVIL_STATUS_WIDOWED              = 'widowed';
    const CIVIL_STATUS_LEGALLY_SEPARATED    = 'legally_separated';

    const CIVIL_STATUSES = [
        self::CIVIL_STATUS_SINGLE               => 'Solteiro(a)',
        self::CIVIL_STATUS_MARRIED              => 'Casado(a)',
        self::CIVIL_STATUS_DIVORCED             => 'Divorciado(a)',
        self::CIVIL_STATUS_WIDOWED              => 'Viuvo(a)',
        self::CIVIL_STATUS_LEGALLY_SEPARATED    => 'Separado(a) Judicialmente',
    ];

    const GENDER_MALE       = 'm';
    const GENDER_FEMALE     = 'f';
    const GENDER_UNDEFINED  = 'u';

    const GENDERS = [
        self::GENDER_MALE           => 'Masculino',
        self::GENDER_FEMALE         => 'Feminino',
        self::GENDER_UNDEFINED      => 'Indefinido',
    ];

    protected $casts = [
        'residential_address'   => 'object',
        'comercial_address'     => 'object',
    ];

    protected $dates = [
        'birth_date',
        'registration_number_expedition_date',
        'cr_expedition_date',
        'cr_expiration_date',
    ];

    protected $fillable = [
        'name', 'email', 'gender', 'cpf_cnpj', 'registration_number', 'voter_id', 'civil_status',
        'cr_number', 'cr_category', 'rm_number', 'naturality', 'nacionality', 'profession',
        'fathers_name', 'mothers_name', 'residential_phone', 'comercial_phone', 'club_id', 'registration_number_expeditor',
        'email2', 'commercial_partner', 'obs'
    ];

    public function club()
    {
        return $this->belongsTo(Club::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function weapons()
    {
        return $this->hasMany(Weapon::class);
    }

    public function issuedDocuments()
    {
        return $this->hasMany(IssuedDocument::class);
    }

    public static function rules()
    {
        return [
            'name'                  => 'required',
            'email'                 => 'nullable|email',
            'gender'                => ['nullable', Rule::in(array_keys(self::GENDERS))],
            'civil_status'          => ['nullable', Rule::in(array_keys(self::CIVIL_STATUSES))],
            'cr_category'           => ['nullable', Rule::in(array_keys(self::CR_CATEGORIES))],
            'club_id'               => ['nullable', Rule::exists('clubs', 'id')],
        ];
    }

    public static function validationMessages()
    {
        return [
            'name.required'                     => 'O nome do cliente é obrigatório',
            'email.email'                       => 'O email informado é inválido',
            'gender.in'                         => 'O gênero informado é inválido',
            'civil_status.in'                   => 'O estado civil informado é inválido',
            'cr_category.in'                    => 'A categoria do CR informada é inválida',
            'club_id.exists'                    => 'O clube informado não foi encontrado',
        ];
    }

    // Override no método delete padrão
    public function delete()
    {
        // Checar se pode deletar
        //TODO Executar as checagens para deletar um cliente (Armas, Documentos etc)
        //if(checagem){
            // Se não puder, throw no erro
            //throw new CannotDeleteException('Não é possível excluir um clube com clientes vinculados.');
        //}
        // Retornar ok se der bom
        return parent::delete();
    }

    public function scopeSearch($query, Request $request)
    {
        if($request->filled('busca_texto')){
            $query->where(function($query) use($request){
                $query->orWhere('name', 'like', '%' . $request->get('busca_texto') . '%');
                $query->orWhere('email', 'like', '%' . $request->get('busca_texto') . '%');
            });
        }

        return $query;

    }


    // Data de nascimento por extenso
    public function maskBirthDateVerbose()
    {
        return defaultVerboseDateFormat($this->birth_date);
    }

    // Data de expedição do RG por extenso
    public function maskRegistrationNumberExpeditionDateVerbose()
    {
        return defaultVerboseDateFormat($this->registration_number_expedition_date);
    }

    // Data de expedição do CR por extenso
    public function maskCrExpeditionDateVerbose()
    {
        return defaultVerboseDateFormat($this->cr_expedition_date);
    }

    // Data de expiração do CR por extenso
    public function maskCrExpirationDateVerbose()
    {
        return defaultVerboseDateFormat($this->cr_expiration_date);
    }

    // Function para montar address do cliente
    public function maskResidentialAddressFull()
    {
        return defaultAddressFormat($this->residential_address);
    }

    // Function para montar address do cliente
    public function maskComercialAddressFull()
    {
        return defaultAddressFormat($this->comercial_address);
    }

    // Pegar o label do gender
    public function maskGender()
    {
        return $this->getGender();
    }

    // Pegar o label do civil status
    public function maskCivilStatus()
    {
        return $this->getCivilStatus();
    }

    // Pegar o label da categoria do CR
    public function maskCRCategory()
    {
        return $this->getCRCategory();
    }

    // Pegar a data atual
    public function maskCurrentDate()
    {
        return defaultDateFormat(Carbon::now());
    }

    // Pegar a data atual por extenso
    public function maskCurrentDateVerbose()
    {
        return defaultVerboseDateFormat(Carbon::now());
    }

    // Pegar o mês atual
    public function maskCurrentMonth()
    {
        return (string) Carbon::now()->month;
    }

    // Pegar o mês atual
    public function maskCurrentMonthVerbose()
    {
        return utf8_encode(Carbon::now()->formatLocalized('%B'));
    }

    // Pegar o ano atual
    public function maskCurrentYear()
    {
        return (string) Carbon::now()->year;
    }

    // Pegar o dia atual
    public function maskCurrentDay()
    {
        return (string) Carbon::now()->day;
    }




    public function getGender()
    {
        if($this->gender) {
            return self::GENDERS[$this->gender];
        }
        return null;
    }

    public function getCivilStatus()
    {
        if ($this->civil_status) {
            return self::CIVIL_STATUSES[$this->civil_status];
        }
        return null;
    }

    public function getCRCategory()
    {
        if ($this->cr_category) {
            return self::CR_CATEGORIES[$this->cr_category];
        }
        return null;
    }
}