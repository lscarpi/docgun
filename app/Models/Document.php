<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class Document extends ResourceIsolatedModel {


    protected $fillable = [
        'title','content'
    ];

    protected $casts = [
        'shared' => 'boolean',
    ];

    public static function resourceIsolated()
    {
        return parent::resourceIsolated()->orWhere('shared', true);
    }

    /**
     * Buscar apenas os documents que são do próprio user (não shared)
     * @return mixed
     */
    public static function ownedByCurrentUser()
    {
        return parent::resourceIsolated();
    }

    /**
     * Buscar todos os documents que não são meus, mas estão compartilhados
     * @return mixed
     */
    public static function sharedByOthers()
    {
        return self::where('shared', true)->where('company_id', '!=', auth()->user()->company_id);
    }

    public static function rules()
    {
        return [
            'title'     => 'required',
            'content'   => 'required',
        ];
    }

    public static function validationMessages()
    {
        return [
            'title.required'    => 'O título do documento é obrigatório',
            'content.required'  => 'O conteúdo do documento é obrigatório',
        ];
    }

    // Override no método delete padrão
    public function delete()
    {
        // Checar se pode deletar
        //if($this->customers()->count() > 0){
            // Se não puder, throw no erro
            //throw new CannotDeleteException('Não é possível excluir um clube com clientes vinculados.');
        //}
        // Retornar ok se der bom
        return parent::delete();
    }

    /**
     * Essa é a main function do objetivo do sistema
     * Ela vai pegar as masks, construir tudo e entregar o contrato parseado
     */
    public function getParsedContent(Customer $customer)
    {
        return MaskReplacer::parseText($this->content, $customer);
    }

    /**
     * Function para verificar se o documento é compartilhado
     * @return true
     */
    public function isShared()
    {
        // O shared vem do BD e tá como cast para boolean
        return $this->shared;
    }

    /**
     * Verifica se o cara pode ler o document
     * @param User $user
     * @return bool
     */
    public function canRead(User $user)
    {
        return $this->isOwner($user) || $this->isShared();
    }

    /**
     * Function que verifica se o usuário pode compartilhar um doc
     * @param User $user
     * @return bool
     */
    public function canShare(User $user)
    {
        return $this->isOwner($user) && $user->isCompanyOwner();
    }

    /**
     * @return bool
     */
    public function currentUserCanShare()
    {
        return Gate::allows('share', $this);
    }

    //Function para search na Documents/List view
    public function scopeSearch($query, Request $request)
    {
        if($request->filled('busca_texto')){
            $query->where(function($query) use($request){
                $query->orWhere('title', 'like', '%' . $request->get('busca_texto') . '%');
            });
        }

        return $query;

    }
}

