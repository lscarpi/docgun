<?php

namespace App\Models;

class IssuedDocument extends ResourceIsolatedModel {


    protected $fillable = [
        'customer_id', 'document_id', 'title','content',
    ];


    public static function rules()
    {
        return [
            'document_id'   => 'required',
            'title'         => 'required',
            'content'       => 'required',
        ];
    }

    public static function validationMessages()
    {
        return [
            'document_id.required'      => 'É necessário informar o modelo do documento',
            'title.required'            => 'O título do documento é obrigatório',
            'content.required'          => 'O conteúdo do documento é obrigatório',
        ];
    }

    public function originalDocument()
    {
        return $this->belongsTo(Document::class, 'document_id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    // Override no método delete padrão
    public function delete()
    {
        // Checar se pode deletar
        //if($this->customers()->count() > 0){
            // Se não puder, throw no erro
            //throw new CannotDeleteException('Não é possível excluir um clube com clientes vinculados.');
        //}
        // Retornar ok se der bom
        return parent::delete();
    }

    /**
     * Essa é a main function do objetivo do sistema
     * Ela vai pegar as masks, construir tudo e entregar o contrato parseado
     * @param Customer $customer
     * @return mixed
     */
    public function getParsedContent(Customer $customer)
    {
        return MaskReplacer::parseText($this->content, $customer);
    }
}