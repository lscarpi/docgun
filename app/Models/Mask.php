<?php namespace App\Models;


use Carbon\Carbon;
use Illuminate\Support\Str;

class Mask{


    /**
     * @var string
     */
    private $name;

    /**
     * @var array
     */
    private $mapping;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var array
     */
    private $parameters;


    /**
     * Mapeamentos
     */
    const MASK_MAPPINGS_CUSTOMER = [
        // CLIENTE
        'NOME_CLENTE' => ['name'],
        'EMAIL_CLIENTE' => ['email'],
        'GENERO_CLIENTE' => ['gender'],
        'DATA_NASCIMENTO_CLIENTE' => ['birth_date'],
        'DATA_NASCIMENTO_CLIENTE_EXTENSO' => ['birth_date_verbose'],
        'CPF_CNPJ_CLIENTE' => ['cpf_cnpj'],
        'NUMERO_RG_CLIENTE' => ['registration_number'],
        'ORGAO_EXPEDIDOR_RG_CLIENTE' => ['registration_number_expeditor'],
        'DATA_EXPEDICAO_RG_CLIENTE' => ['registration_number_expedition_date'],
        'DATA_EXPEDICAO_RG_CLIENTE_EXTENSO' => ['registration_number_expedition_date_verbose'],
        'TITULO_ELEITOR_CLIENTE' => ['voter_id'],
        'ESTADO_CIVIL_CLIENTE' => ['civil_status'],
        'NATURALIDADE_CLIENTE' => ['naturality'],
        'NACIONALIDADE_CLIENTE' => ['nacionality'],
        'PROFISSAO_CLIENTE' => ['profession'],
        'NOME_PAI_CLIENTE' => ['fathers_name'],
        'NOME_MAE_CLIENTE' => ['mothers_name']


        // CLUBE QUE O CLIENTE ESTÁ ASSOCIADO

//        'ENDERECO_LOGRADOURO_CLUBE' => ['address', 'address_1'],
//        'ENDERECO_COMPLEMENTO_CLUBE' => ['address', 'address_2'],
//        'ENDERECO_NUMERO_CLUBE' => ['address', 'number'],
//        'ENDERECO_BAIRRO_CLUBE' => ['address', 'neighborhood'],
//        'ENDERECO_CIDADE_CLUBE' => ['address', 'city'],
//        'ENDERECO_UF_CLUBE' => ['address', 'state'],
//        'ENDERECO_CEP_CLUBE' => ['address', 'zip'],

        // ARMA QUE O CLIENTE POSSUI
//        'NOTA_FISCAL_ARMA' => ['weapons', 'receipt_number'],
//        'TIPO_ARMA' => ['weapons', 'type'],
//        'MARCA_ARMA' => ['weapons', 'brand'],
//        'CALIBRE_ARMA' => ['weapons', 'caliber'],
//        'NUMERO_SERIAL_ARMA' => ['weapons', 'serial_number'],
//        'MODELO_ARMA' => ['weapons', 'model'],
//        'ACERVO_ARMA' => ['weapons', 'stash'],
//        'DATA_EXPEDICAO_DOCUMENTOS_ARMA' => ['weapons', 'docs_issued_at'],
//        'NUMERO_SIGMA_ARMA' => ['weapons', 'sigma_number'],
//        'STATUS_ARMA' => ['weapons', 'status'],
//        'NUMERO_GUIA_TRANSITO_ARMA' => ['weapons', 'transit_permission_number'],
//        'DATA_VALIDADE_GUIA_TRANSITO_ARMA' => ['weapons', 'transit_permission_expiry_date'],
    ];

    const MASK_MAPPINGS_RES_ADDRESS = [
        'COMPLETO_RESIDENCIAL_CLIENTE' => ['residential_address_full'],
        'LOGRADOURO_RESIDENCIAL_CLIENTE' => ['residential_address', 'address_1'],
        'COMPLEMENTO_RESIDENCIAL_CLIENTE' => ['residential_address', 'address_2'],
        'NUMERO_RESIDENCIAL_CLIENTE' => ['residential_address', 'number'],
        'BAIRRO_RESIDENCIAL_CLIENTE' => ['residential_address', 'neighborhood'],
        'CIDADE_RESIDENCIAL_CLIENTE' => ['residential_address', 'city'],
        'UF_RESIDENCIAL_CLIENTE' => ['residential_address', 'state'],
        'CEP_RESIDENCIAL_CLIENTE' => ['residential_address', 'zip'],
        'TELEFONE_RESIDENCIAL_CLIENTE' => ['residential_phone']
    ];

    const MASK_MAPPINGS_COM_ADDRESS = [
        'COMPLETO_COMERCIAL_CLIENTE' => ['comercial_address_full'],
        'LOGRADOURO_COMERCIAL_CLIENTE' => ['comercial_address', 'address_1'],
        'COMPLEMENTO_COMERCIAL_CLIENTE' => ['comercial_address', 'address_2'],
        'NUMERO_COMERCIAL_CLIENTE' => ['comercial_address', 'number'],
        'BAIRRO_COMERCIAL_CLIENTE' => ['comercial_address', 'neighborhood'],
        'CIDADE_COMERCIAL_CLIENTE' => ['comercial_address', 'city'],
        'UF_COMERCIAL_CLIENTE' => ['comercial_address', 'state'],
        'CEP_COMERCIAL_CLIENTE' => ['comercial_address', 'zip'],
        'TELEFONE_COMERCIAL_CLIENTE' => ['comercial_phone']
    ];

    const MASK_MAPPINGS_DOCUMENTS = [
        'NUMERO_CR_CLIENTE' => ['cr_number'],
        'CATEGORIA_CR_CLIENTE' => ['cr_category'],
        'DATA_EXPEDICAO_CR_CLIENTE' => ['cr_expedition_date'],
        'DATA_EXPEDICAO_CR_CLIENTE_EXTENSO' => ['cr_expedition_date_verbose'],
        'DATA_VALIDADE_CR_CLIENTE' => ['cr_expiration_date'],
        'DATA_VALIDADE_CR_CLIENTE_EXTENSO' => ['cr_expiration_date_verbose'],
        'NUMERO_RM_CLIENTE' => ['rm_number']
    ];

    const MASK_MAPPINGS_CLUBS = [
        'NOME_CLUBE' => ['club', 'name'],
        'ENDERECO_COMPLETO_CLUBE' => ['club', 'address_full'],
        'TELEFONE_CLUBE' => ['club', 'phone'],
        'NOME_RESPONSAVEL_CLUBE' => ['club', 'person_in_charge'],
        'EMAIL_CLUBE' => ['club', 'email']
    ];

    // ADICIONAIS - HELPERS
    const MASK_MAPPINGS_HELPERS = [
        'DATA_ATUAL' => ['current_date'],
        'DATA_ATUAL_EXTENSO' => ['current_date_verbose'],
        'MES_ATUAL' => ['current_month'],
        'MES_ATUAL_EXTENSO' => ['current_month_verbose'],
        'ANO_ATUAL' => ['current_year'],
        'DIA_ATUAL' => ['current_day']
    ];

    // COMPANIES MASKS
    const MASK_MAPPINGS_COMPANIES = [
        'NOME_FANTASIA_EMPRESA' => ['company', 'name'],
        'RAZAO_SOCIAL_EMPRESA' => ['company', 'legal_name'],
        'CPF_CNPJ_EMPRESA' => ['company', 'cpf_or_cnpj'],
        'EMAIL_EMPRESA' => ['company', 'email'],
        'ENDEREÇO_EMPRESA' => ['company', 'address_1'],
        'COMPLEMENTO_ENDEREÇO_EMPRESA' => ['company', 'address_2'],
        'NUMERO_ENDEREÇO_EMPRESA' => ['company', 'address_number'],
        'BAIRRO_ENDEREÇO_EMPRESA' => ['company', 'address_neighborhood'],
        'CIDADE_ENDEREÇO_EMPRESA' => ['company', 'address_city'],
        'ESTADO_ENDEREÇO_EMPRESA' => ['company', 'address_state'],
        'ENDERECO_COMPLETO_EMPRESA' => ['company', 'address_full']
    ];



    // Get all mask mappings into just one array
    const AVAILABLE_MASK_MAPPINGS = [
        self::MASK_MAPPINGS_CUSTOMER,
        self::MASK_MAPPINGS_RES_ADDRESS,
        self::MASK_MAPPINGS_COM_ADDRESS,
        self::MASK_MAPPINGS_DOCUMENTS,
        self::MASK_MAPPINGS_CLUBS,
        self::MASK_MAPPINGS_HELPERS,
        self::MASK_MAPPINGS_COMPANIES
    ];



    /**
     * Mask constructor.
     * @param string $name
     * @param array $mapping
     */
    public function __construct(string $name, array $mapping)
    {
        $this->name = $name;
        $this->mapping = $mapping;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * @param array $parameters
     */
    public function setParameters(array $parameters)
    {
        $this->parameters = $parameters;
    }


    /**
     * Helper function
     * @param string $name
     * @param array $parameters
     * @param Customer $customer
     * @return Mask|null
     */
    public static function find(string $search, array $parameters = [], Customer $customer)
    {
        // Loop na array de mask mappings
        foreach(self::AVAILABLE_MASK_MAPPINGS as $mask_mapping) {
            // Loop no mask mapping em questão
            foreach ($mask_mapping as $mask_name => $mapping) {
                // Se o nome for o mesmo da busca
                if (strtolower($mask_name) == strtolower($search)) {
                    // Montar a mask
                    $mask = new self($mask_name, $mapping);
                    $mask->setParameters($parameters);
                    $mask->setCustomer($customer);
                    // Retornar
                    return $mask;
                }
            }
        }
        // se chegar aqui, não achou nada
        return null;
    }

    /**
     * Obter o valor da máscara finalmente
     * Caso queiramos modificar o valor da mask via function
     * É necessário criar uma function maskCamelCaseNomeDoAtributo();
     * Automaticamente será carregado
     * @return string
     */
    public function getValue()
    {
        //TODO: Implementar parametros not yet recognized

        // Obter o mapeamento
        $mapping = $this->mapping;

        // Tentar executar
        try {
            // A primeira step é o customer
            $current_step = $this->customer;
            // Loop no mapeamento
            foreach($mapping as $step){

                // Exemplo de nome maskVoterId();
                $method_name = 'mask' . ucfirst(Str::camel($step));

                // Se esse método existir
                if(method_exists($current_step, $method_name)){
                    // A gente já dá return e acabou no método
                    return $current_step->{$method_name}();
                // Se não existir
                }else{
                    // Andamos mais uma step
                    $current_step = $current_step->{$step};
                }
            }

            // Se chegar até aqui, estamos na última step
            // Vamos retornar o valor dela e fechou brasil

            // Se a current step for uma data, já corta logo
            if($current_step instanceof Carbon){
                return defaultDateFormat($current_step);
            }

            // Se não for, retorna tudo
            return $current_step;

        // Se der qualquer erro durante o processo
        }catch(\Exception $e){
            // Babou, retorna null
            return null;
        }

    }
}