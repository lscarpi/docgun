<?php namespace App\Models;

class MaskReplacer{

    /**
     * Actual Stuff happening
     * @param $mask_name
     * @param array $parameters
     * @param Customer $customer
     * @return string
     */
    private static function getMaskValue($mask_name, $parameters = [], Customer $customer)
    {
        // Achar a mask
        $mask = Mask::find($mask_name, $parameters, $customer);
        // Se não achou a mask
        if(! $mask){
            // E tiver parameters
            if($parameters){
                // Adicionar novamente na mask
                $mask_name = $mask_name . '|' .  http_build_query($parameters);
            }
            // Retorna ela zuada mesmo
            return "{$mask_name}";
        }

        // Se chegou aqui, a mask é válida e show
        return $mask->getValue();
    }


    public static function parseText($text, Customer $customer)
    {
        // Regex pra achar as masks
        $regex = '/\{(.*?)\}/';
        // Executar o preg match
        preg_match_all($regex, $text, $matches);
        // Se matches for uma array e tiver o index 01
        if(is_array($matches) && isset($matches[1])){
            // Setando var pra ficar mais legível
            $results = $matches[1];
            // Loop nos results
            foreach($results as $result){
                // Explode nos results usando a | (mesmo que não tenha, vai virar uma array com 01 pos)
                $explode = explode('|', $result);
                // Setar uma var null
                $parameters = [];
                // Se tiver mais do que 01 result
                if(count($explode) > 1){
                    // vamos parsear o resto como uma query string
                    parse_str($explode[1], $parameters);
                }
                // Obter o valor da mask
                $mask_value = self::getMaskValue($explode[0], $parameters, $customer);
                // Se achar algum valor no replace da mask
                if($mask_value) {
                    if(! is_string($mask_value)){
                        dd([
                            'NAME' => $explode[0],
                            'VALUE' => $mask_value
                        ]);
                    }

                    // Trocar lá no content
                    $text = str_replace("{" . $explode[0] . "}", $mask_value, $text);
                }
            }
        }
        // Retornar o content depois de tudo
        return $text;
    }
}