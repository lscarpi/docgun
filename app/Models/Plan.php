<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plan extends Model{

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    const PLAN_STATUS_ACTIVE = 'active';
    const PLAN_STATUS_INACTIVE = 'inactive';

    // Resources que vamos usar sempre
    const RESOURCE_MAX_USERS = 'max_users';

    protected $casts = [
        'resource_limits' => 'object',
    ];

    protected $fillable = [
        'name', 'price', 'status', 'resource_limits',
    ];

    //Define que um Plano, ao ser criado, adquire status ACTIVE
    public static function boot()
    {
        parent::boot();

        static::creating(function(self $plan){
            $plan->status = self::PLAN_STATUS_ACTIVE;
        });
    }

    //Define que um Plano pode conter várias inscrições
    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }


    /**
     * Function que checa a limitação de algum recurso do sistema com base no plan
     * @param $resource_name
     * @param null $default_value
     * @return mixed
     */
    public function getResourceLimit($resource_name, $default_value = null)
    {
        // Puxar o custom resource limits (auto cast para object)
        $resource_limits = $this->resource_limits;
        // Se essa var for um object (Não for null) e existir essa property lá
        if($resource_limits && property_exists($resource_limits, $resource_name)){
            // Retornar ela
            return $resource_limits->{$resource_name};
        }
        // Se chegou aqui, babou
        return $default_value;
    }

    /**
     * Buscar quantos users esse plan permite
     * @return mixed
     */
    public function getMaxUsers()
    {
        return $this->getResourceLimit(self::RESOURCE_MAX_USERS, 0);
    }











    //Define campos requeridos para criaçao de Planos
    public static function rules()
    {
        return [
            'name' => 'required',
            'price' => 'required',
        ];
    }


    public static function validationMessages()
    {
        return [
            'name.required' => 'O nome do plano é obrigatório',
            'price.required' => 'O preço do plano é obrigatório',
        ];
    }


    /**
     * Function que checa se o plano é gratuito
     * @return bool
     */
    public function isFree()
    {
        return $this->price == 0;
    }

    /**
     * Function que checa se o plano é pago
     * @return bool
     */
    public function isPaid()
    {
        return ! $this->isFree();
    }

    public static function createFreePlan()
    {

        // Tentar dar fetch no plan
        $plan = Plan::find(config('docgun.general.trial.plan_id'));

        // Se achar o plan
        if($plan){
            // Retornar ele diretamente
            return $plan;
        }

        // Se chegou aqui, Não tem plan

        // Criar um novo plan
        $plan = new Plan();

        // Fill nos valores
        $plan->name = "Teste Grátis";
        $plan->price = 0.00;
        $plan->status = self::PLAN_STATUS_ACTIVE;

        // Criar resources
        $resources = new \stdClass();
        $resources->max_users = 1;
        // Setar no plan
        $plan->resource_limits = $resources;

        // Save no plan
        $plan->save();

        // Retornar o plan
        return $plan;

    }
}