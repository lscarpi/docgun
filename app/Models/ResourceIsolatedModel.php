<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Gate;

class ResourceIsolatedModel extends Model{

    public static function resourceIsolated()
    {
        return self::where('company_id', auth()->user()->getCurrentCompanyId());
    }

    public function scopeRecentsFirst($query)
    {
        return $query->orderBy('id', 'DESC');
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function(self $model){
            $model->company_id = auth()->user()->company_id;
        });
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isOwner(User $user)
    {
        return $user->company_id  == $this->company_id;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function canRead(User $user)
    {
        return $this->isOwner($user);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function canCreate(User $user)
    {
        return true;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function canUpdate(User $user)
    {
        return $this->isOwner($user);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function canDelete(User $user)
    {
        return $this->isOwner($user);
    }

    /**
     * @return bool
     */
    public function currentUserCanRead()
    {
        return Gate::allows('read', $this);
    }

    /**
     * @return bool
     */
    public function currentUserCanCreate()
    {
        return Gate::allows('create', $this);
    }

    /**
     * @return bool
     */
    public function currentUserCanUpdate()
    {
        return Gate::allows('update', $this);
    }

    /**
     * @return bool
     */
    public function currentUserCanDelete()
    {
        return Gate::allows('delete', $this);
    }

    /**
     * @return bool
     */
    public function isOwnedByCurrentUser()
    {
        return $this->isOwner(auth()->user());
    }



    // Helpers

    public static function createdPerMonthDataTable($total_months = 6)
    {
        // Criar uma array
        $data_table = [];

        // Loop no total de months
        foreach (range(0, $total_months - 1) as $month_index) {
            // Criar uma data de start
            $date_start = now()->subMonths($month_index)->startOfMonth();
            // E uma de end
            $date_end = now()->subMonths($month_index)->endOfMonth();

            // Query na resource
            $resource = self::resourceIsolated()
                // Onde a data de criação estiver entre as duas
                ->whereBetween('created_at', [$date_start->toDateTimeString(), $date_end->toDateTimeString()])
                // Pegar o total apenas
                ->count();

            // Setar o valor da data table
            $data_table[] = [$date_start->formatLocalized('%m/%Y'), $resource];
        }
        // Retornar a array
        return array_reverse($data_table);
    }

}