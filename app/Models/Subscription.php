<?php

namespace App\Models;

use App\Library\SubscriptionProviders\Pagseguro\SubscriptionProvider as PagSeguroSubscriptionProvider;
use Illuminate\Database\Eloquent\Builder;
use stdClass;

/**
 * Class Subscription
 * @package App\Models
 */
class Subscription extends ResourceIsolatedModel
{
    const SUBSCRIPTION_STATUS_PENDING = 'pending';
    const SUBSCRIPTION_STATUS_ACTIVE = 'active';
    const SUBSCRIPTION_STATUS_SUSPENDED = 'suspended';
    const SUBSCRIPTION_STATUS_TERMINATED = 'terminated';
    const SUBSCRIPTION_STATUS_RIGHTFUL_HEIR_TO_THE_THRONE = 'rightful_heir_to_the_throne';

    const SUBSCRIPTION_STATUSES = [
        self::SUBSCRIPTION_STATUS_PENDING,
        self::SUBSCRIPTION_STATUS_ACTIVE,
        self::SUBSCRIPTION_STATUS_SUSPENDED,
        self::SUBSCRIPTION_STATUS_TERMINATED,
        self::SUBSCRIPTION_STATUS_RIGHTFUL_HEIR_TO_THE_THRONE,
    ];

    const SUBSCRIPTION_BILLING_CYCLE_MONTHLY = 'monthly';
    const SUBSCRIPTION_BILLING_CYCLE_TRIMONTHLY = 'trimonthly';
    const SUBSCRIPTION_BILLING_CYCLE_HALF_YEARLY = 'half_yearly';
    const SUBSCRIPTION_BILLING_CYCLE_YEARLY = 'yearly';

    const SUBSCRIPTION_PROVIDERS = [
        'pagseguro' => PagSeguroSubscriptionProvider::class,
    ];

    const SUBSCRIPTION_STATUSES_WITH_SYSTEM_ACCESS = [
        self::SUBSCRIPTION_STATUS_ACTIVE,
        self::SUBSCRIPTION_STATUS_SUSPENDED,
        self::SUBSCRIPTION_STATUS_RIGHTFUL_HEIR_TO_THE_THRONE,
    ];


    const SUBSCRIPTION_PAYMENT_METHOD_BANKING_BILLET    = 'banking_billet';
    const SUBSCRIPTION_PAYMENT_METHOD_CREDIT_CARD       = 'credit_card';

    const SUBSCRIPTION_AVAILABLE_PAYMENT_METHODS = [
        self::SUBSCRIPTION_PAYMENT_METHOD_BANKING_BILLET,
        self::SUBSCRIPTION_PAYMENT_METHOD_CREDIT_CARD,
    ];




    protected $casts = [
        'custom_resource_limits' => 'object',
        'trial' => 'boolean',
    ];

    protected $fillable = [
        'plan_id', 'company_id', 'billing_cycle', 'price', 'status'
    ];

    protected $dates = [
        'terminate_at',
    ];


    public static function boot()
    {
        parent::boot();

        static::creating(function (self $subscription) {
            // Setar como pending se não for trial ou active se for
            $subscription->status =
                $subscription->isTrial() ?
                    self::SUBSCRIPTION_STATUS_ACTIVE :
                    self::SUBSCRIPTION_STATUS_PENDING;

            // Se não existir billing cycle
            if (! $subscription->billing_cycle) {
                // Setar como mensal
                $subscription->billing_cycle = self::SUBSCRIPTION_BILLING_CYCLE_MONTHLY;
            }
            // Se não foi setado price
            if (! $subscription->price) {
                // Colocar o do plano
                $subscription->price = $subscription->plan->price;
            }

            // Setar o current provider da subscription sendo criada
            $subscription->provider = self::getCurrentProviderName();
        });
    }

    public function scopeWithSystemAccess(Builder $query)
    {
        return $query->whereIn('status', self::SUBSCRIPTION_STATUSES_WITH_SYSTEM_ACCESS);
    }

    public function scopePending(Builder $query)
    {
        return $query->where('status', self::SUBSCRIPTION_STATUS_PENDING);
    }

    public function scopeActive(Builder $query)
    {
        return $query->where('status', self::SUBSCRIPTION_STATUS_ACTIVE);
    }

    public function scopeSuspended(Builder $query)
    {
        return $query->where('status', self::SUBSCRIPTION_STATUS_SUSPENDED);
    }

    public function scopeTerminated(Builder $query)
    {
        return $query->where('status', self::SUBSCRIPTION_STATUS_TERMINATED);
    }

    public function isTrial()
    {
        return $this->trial && ! $this->isRightfulHeirToTheThrone();
    }

    public function isPending()
    {
        return $this->status == self::SUBSCRIPTION_STATUS_PENDING;
    }

    public function isActive()
    {
        return $this->status == self::SUBSCRIPTION_STATUS_ACTIVE;
    }

    public function isSuspended()
    {
        return $this->status == self::SUBSCRIPTION_STATUS_SUSPENDED;
    }

    public function isTerminated()
    {
        return $this->status == self::SUBSCRIPTION_STATUS_TERMINATED;
    }

    public function setPending()
    {
        $this->status = self::SUBSCRIPTION_STATUS_PENDING;
        return $this->save();
    }

    public function setActive()
    {
        $this->status = self::SUBSCRIPTION_STATUS_ACTIVE;
        return $this->save();
    }

    public function setSuspended()
    {
        $this->status = self::SUBSCRIPTION_STATUS_SUSPENDED;
        return $this->save();
    }

    public function setTerminated()
    {
        // Setar o status
        $this->status = self::SUBSCRIPTION_STATUS_TERMINATED;
        // Achar o provider
        $provider = $this->getProviderClass();
        // Cancelar a sub
        $provider::cancelSubscription($this);
        // Salvar
        return $this->save();
    }


    /**
     * Verifica se a subscription do cliente é a bichão de honra, entende?
     * @return bool
     */
    public function isRightfulHeirToTheThrone()
    {
        return $this->status == self::SUBSCRIPTION_STATUS_RIGHTFUL_HEIR_TO_THE_THRONE;
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * Function que verifica se a subscription permite o acesso ao sistema
     * @return bool
     */
    public function allowsAccess()
    {
        // Se ele for o bichão de honra
        if ($this->isRightfulHeirToTheThrone()) {
            // Claro que pode, né?
            return true;
        }

        // Se ela for trial
        if ($this->isTrial()) {
            // Verificar se a data atual, é menor ou igual a data de término do trial
            return now()->lessThanOrEqualTo($this->terminate_at->startOfDay());
        }
        // Se não for trial, então é uma sub paga
        // Aí só verificamos se é uma sub ativa
        return $this->status == self::SUBSCRIPTION_STATUS_ACTIVE;
    }

    /**
     * Function que puxa os dias restantes para o término do trial
     * @return null
     */
    public function trialDaysRemaining()
    {
        // Se ela não for trial
        if (! $this->isTrial()) {
            // Tá de peixe
            return null;
        }
        // Se ela for trial, retornar a diferença em dias de agora
        return $this->terminate_at->startOfDay()->diffInDays(now());
    }


    /**
     * Function que checa a limitação de algum recurso do sistema com base na subscription e/ou plan
     * @param $resource_name
     * @param null $default_value
     * @return mixed
     */
    public function getResourceLimit($resource_name, $default_value = null)
    {
        // Puxar o custom resource limits (auto cast para object)
        $custom_resource_limits = $this->custom_resource_limits;
        // Se essa var for um object (Não for null) e existir essa property lá
        if ($custom_resource_limits && property_exists($custom_resource_limits, $resource_name)) {
            // Retornar ela
            return $custom_resource_limits->{$resource_name};
        }
        // Se chegou aqui, temos que ver as resources do plan
        // Puxar o plan
        $plan = $this->plan;
        // Puxar os resource limits do plan
        $plan_resource_limits = $plan->resource_limits;
        // Se existir essa property lá no plan
        if ($plan_resource_limits && property_exists($plan_resource_limits, $resource_name)) {
            // Retornar ela
            return $plan_resource_limits->{$resource_name};
        }
        // Se chegou aqui, babou
        return $default_value;
    }

    /**
     * Buscar quantos users essa subscription permite
     * @return mixed
     */
    public function getMaxUsers()
    {
        return $this->getResourceLimit(Plan::RESOURCE_MAX_USERS, 0);
    }

    /**
     * Function para alterar o máximo de users que essa sub permite
     * @param int $maxUsers
     * @return bool
     */
    public function setMaxUsers($maxUsers = 1) : bool
    {
        $customResourceLimits = $this->custom_resource_limits ?? new stdClass();
        // Set the resource limit
        $customResourceLimits->{Plan::RESOURCE_MAX_USERS} = $maxUsers;
        // Update the whole information
        $this->custom_resource_limits = $customResourceLimits;
        // Save and return the save response
        return $this->save();
    }

    public function hasProviderSubscription()
    {
        return $this->provider_subscription_id ? true : false;
    }

    public function getProviderName()
    {
        // Retornar
        return
            // Se tem provider do OBJECT
            $this->provider
                ? $this->provider // Retornar o do OBJECT
                : self::getCurrentProviderName(); // Senão, retornar o current provider do sistema
    }

    public function getProviderClass()
    {
        // Retornar
        return
            // Se existe um provider na lista com este name
            isset(self::SUBSCRIPTION_PROVIDERS[$this->getProviderName()])
                ? self::SUBSCRIPTION_PROVIDERS[$this->getProviderName()] // Retornar o próprio
                : null; // Senão null
    }




    /**
     * Function que calcula o price adequado para a subscription
     * @return float
     */
    public function calculateSubscriptionPrice() : float
    {
        // Setar o price
        $price = $this->price;
        // Switch no cycle
        switch ($this->billing_cycle) {
            // Não listado ou monthly
            default:
            case self::SUBSCRIPTION_BILLING_CYCLE_MONTHLY:
                // Só passa
                break;
            // Quadrimestral, desconto de 10%
            case self::SUBSCRIPTION_BILLING_CYCLE_TRIMONTHLY:
                $price = ($price * 4) * 0.9;
                break;
            // Semestral, desconto de 20%
            case self::SUBSCRIPTION_BILLING_CYCLE_HALF_YEARLY:
                $price = ($price * 6) * 0.8;
                break;
            // Anual, desconto de 30%
            case self::SUBSCRIPTION_BILLING_CYCLE_YEARLY:
                $price = ($price * 12) * 0.7;
                break;
        }
        // Retornar arredondando o valor calculado
        return round($price, 2);
    }


    /**
     * Function que retorna a status label
     * @return string
     */
    public function statusLabel() : string
    {
        return self::getStatusLabel($this->status);
    }

    /**
     * Function que retornar a hex color referente ao status
     * @return string
     */
    public function statusColor() : string
    {
        return self::getStatusColor($this->status);
    }
    /* * * * * * * * * * * * * * * * * * * * * * * * *
     *                  Statics
     * * * * * * * * * * * * * * * * * * * * * * * * */


    public static function rules()
    {
        return [
            'price' => 'required',
            'plan_id' => 'required',
            'company_id' => 'required',
            'billing_cycle' => 'required',
        ];
    }

    public static function validationMessages()
    {
        return [
            'price.required' => 'O preço da assinatura é obrigatório',
            'plan_id.required' => 'O plano da assinatura é obrigatório',
            'company.required' => 'A empresa da assinatura é obrigatória',
            'billing_cycle.required' => 'O ciclo de pagamento da assinatura é obrigatório',
        ];
    }


    /**
     * Function para retornar a class do provider atual
     * @return string
     */
    public static function getCurrentProviderClass()
    {
        return config('docgun.general.subscriptions.current_provider');
    }

    /**
     * Function para retornar o name do provider atual
     * @return string
     */
    public static function getCurrentProviderName()
    {
        return self::findProviderNameByClass(self::getCurrentProviderClass());
    }

    /**
     * Function que busca sempre o nome do provider baseado na class informada
     * @param $class_name
     * @return string
     */
    public static function findProviderNameByClass($class_name)
    {
        // Loop nos provider possíveis
        foreach (self::SUBSCRIPTION_PROVIDERS as $provider_name => $provider_class) {
            // Se a class do item do loop for a mesma que está sendo buscada
            if ($class_name == $provider_class) {
                // Retornar o name
                return $provider_name;
            }
        }
        // Se chegou aqui, não achou
        return null;
    }

    /**
     * Function que define se um provider é válido
     * @param $provider_name
     * @return bool
     */
    public static function isProviderValid($provider_name)
    {
        return isset(self::SUBSCRIPTION_PROVIDERS[$provider_name]);
    }

    /**
     * Function para puxar uma class de provider
     * @param $provider_name
     * @return mixed|null
     */
    public static function getProviderByName($provider_name)
    {
        return self::isProviderValid($provider_name)
            ? self::SUBSCRIPTION_PROVIDERS[$provider_name]
            : null;
    }

    /**
     * Helper para mostrar se o método de pagamento é válido
     * @param $method
     * @return bool
     */
    public static function isPaymentMethodValid($method)
    {
        // retorna se o valor está a array de métodos disponíveis
        return in_array($method, self::SUBSCRIPTION_AVAILABLE_PAYMENT_METHODS);
    }

    /**
     * Function para achar a subscription pelo provider id
     * @param $provider_subscription_id
     * @return Subscription|null
     */
    public static function findByProviderSubscriptionId($provider_subscription_id) : ? Subscription
    {
        return self::where('provider_subscription_id', $provider_subscription_id)->first();
    }

    /**
     * Function que retorna a status label
     * @param $status
     * @return string|null
     */
    public static function getStatusLabel($status) : ? string
    {
        // Switch no status e retornar string correspondente
        switch ($status) {
            default:
                return null;
            case self::SUBSCRIPTION_STATUS_PENDING:
                return 'Pendente';
            case self::SUBSCRIPTION_STATUS_ACTIVE:
                return 'Ativo';
            case self::SUBSCRIPTION_STATUS_SUSPENDED:
                return 'Suspenso';
            case self::SUBSCRIPTION_STATUS_TERMINATED:
                return 'Terminado';
            case self::SUBSCRIPTION_STATUS_RIGHTFUL_HEIR_TO_THE_THRONE:
                return 'Herdeiro ao Trono';
        }
    }

    /**
     * Function que retornar a hex color referente ao status
     * @param $status
     * @return string|null
     */
    public static function getStatusColor($status) : ? string
    {
        // Switch no status e retornar string correspondente
        switch ($status) {
            default:
                return null;
            case self::SUBSCRIPTION_STATUS_PENDING:
                return '#FFA500';
            case self::SUBSCRIPTION_STATUS_ACTIVE:
                return '#006B3C';
            case self::SUBSCRIPTION_STATUS_SUSPENDED:
                return '#9966CC';
            case self::SUBSCRIPTION_STATUS_TERMINATED:
                return '#800080';
            case self::SUBSCRIPTION_STATUS_RIGHTFUL_HEIR_TO_THE_THRONE:
                return '#007BA7';
        }
    }
}
