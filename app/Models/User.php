<?php

namespace App\Models;

use App\Mail\AccountActivation;
use App\Mail\ResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;


class User extends ResourceIsolatedAuthenticatable{

    use Notifiable;

    const USER_STATUS_ACTIVE = 'active';
    const USER_STATUS_INACTIVE = 'inactive';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'whatsapp',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // Laravel stuff
    protected $casts = [
        'is_admin' => 'boolean',
        'company_owner' => 'boolean',
        'cache' => 'object',
    ];

    protected $dates = [
        'birth_date',
    ];


    public static function validationRules(self $user = null)
    {
        $email_rule = Rule::unique('users', 'email');
        if($user){
            $email_rule->ignore($user->id);
        }

        $rules = [
            'email' => ['email', $email_rule, 'required'],
            'name' => 'required',
        ];

        if(! $user){
            $rules['password'] = 'required|min:4|confirmed';
        }else{
            $rules['password'] = 'nullable|min:4|confirmed';
        }

        return $rules;
    }

    public static function validationMessages()
    {
        return [
            'email.email' => 'O email informado é inválido',
            'email.unique' => 'O email informado já está em uso',
            'email.required' => 'É necessário informar um email',
            'name.required' => 'É necessário informar um nome',
            'password.required' => 'É necessário informar uma senha',
            'password.min' => 'A senha precisa ter pelo menos 04 caracteres',
            'password.confirmed' => 'As senhas informadas não conferem',
        ];
    }


    public static function boot()
    {
        parent::boot();

        static::creating(function(self $user){

            if(! $user->is_admin){
                $user->is_admin = false;
            }

            // Criar company
            if(! $user->company_id){
                $company = new Company();
                $company->name = $user->name;
                $company->save();
                $user->company_id = $company->id;
                $user->company_owner = true;
            }

            // Fix na password
            if(! starts_with($user->password, '$') && strlen($user->password) != 60){
                $user->password = bcrypt($user->password);
            }
        });

        static::created(function(self $user){
            // Aqui vamos preparar o status e enviar o email de confirmação
            if(! $user->status){
                $user->status = self::USER_STATUS_INACTIVE;
                $user->cacheSet('activation_token', Str::random(60));
                // Enviar email to the brother
                //TODO: Gibe email PLX
            }
        });
    }

    public function clubs()
    {
        return $this->hasMany(Club::class);
    }

    public function documents()
    {
        return $this->hasMany(Document::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * Essa function vai quebrar o nome do usuário nos espaços e retornar o primeiro resultado
     * @return string
     */
    public function firstName()
    {
        return explode(' ', $this->name)[0];
    }

    /**
     * Checks if user is admin
     * @return boolean
     */
    public function isAdmin()
    {
        return $this->is_admin;
    }

    public function isCompanyOwner()
    {
        return $this->company_owner;
    }


    public function getCurrentCompanyId()
    {
        if(session()->has('company_id')){
            return session()->get('company_id');
        }
        return $this->company_id;
    }

    public function switchCompany(Company $company)
    {
        session()->put('company_id', $company->id);
    }

    public function switchCompanyBack()
    {
        return session()->forget('company_id');
    }

    public function hasSwitchedCompany()
    {
        return session()->has('company_id');
    }

    public function isActive()
    {
        return $this->status == self::USER_STATUS_ACTIVE;
    }

    /**
     * Function responsável por puxar a subscription da company do user
     * @return null
     */
    public function getCompanySubscription()
    {
        // Puxar a company do user (pode estar logado em outra company se for admin)
        $company = Company::find($this->getCurrentCompanyId());

        // Se não achar
        if(! $company){
            // Abandona
            return null;
        }
        // Puxar a subscription
        $subscription = $company->getCurrentSubscription();

        // Se não achar
        if(! $subscription){
            // Abandona
            return null;
        }
        // Se achar, retorna
        return $subscription;
    }

    /**
     * Function que verifica se a subscription da company é trial
     * @return bool
     */
    public function isOnTrialSubscription()
    {
        $subscription = $this->getCompanySubscription();
        return $subscription && $subscription->isTrial();
    }


    /**
     * Function para atualizar o usuário logado atualmente
     * @return bool
     */
    public function updateCurrentLoggedInSession()
    {
        $session_id = session()->getId();
        return $this->cacheSet('current_session_id', $session_id);
    }

    /**
     * Function que remove um logged in session
     * @return bool
     */
    public function removeCurrentLoggedInSession()
    {
        return $this->cacheUnset('current_session_id');
    }


    /**
     * Function que obtém o current logged in session
     * @return mixed
     */
    public function getCurrentLoggedInSession()
    {
        return $this->cacheGet('current_session_id');
    }


    /* * * * * * * * * * * * * * * * * * * * * *
     *                Cache
     * * * * * * * * * * * * * * * * * * * * * */

    /**
     * Function interna para puxar o cache do cara
     * @return mixed|\stdClass
     */
    private function getCache()
    {
        // Tentar dar cast do banco de dados
        $cache = $this->cache;

        // Se tiver null
        if(! $cache){
            // Return empty object
            return new \stdClass();
        }
        // Se não tiver, volta object do cache do BD mesmo
        return $cache;
    }

    /**
     * @param $key
     * @return bool
     */
    public function cacheHas($key)
    {
        return property_exists($this->getCache(), $key);
    }

    /**
     * @param $key
     * @param $value
     * @return bool
     */
    public function cacheSet($key, $value)
    {
        $cache = $this->getCache();
        $cache->{$key} = $value;
        $this->cache = $cache;
        return $this->save();
    }


    /**
     * @param $key
     * @return bool
     */
    public function cacheUnset($key)
    {
        // Puxar o cache
        $cache = $this->getCache();
        // Se existe a key
        if($this->cacheHas($key)){
            // unset
            unset($cache->{$key});
            // Atualizar a var que vai pro BD
            $this->cache = $cache;
            // Salvar
            return $this->save();
        }
        // Se já não existe, então true malandro
        return true;
    }

    /**
     * @param $key
     * @param null $default_value
     * @return null
     */
    public function cacheGet($key, $default_value = null)
    {
        // Puxar o cache
        $cache = $this->getCache();

        // Se tem a property lá
        if($this->cacheHas($key)){
            // Retorna
            return $cache->{$key};
        }
        // Senão, valor padrão do brasil
        return $default_value;
    }

    public function scopeSearch($query, Request $request)
    {
        if($request->filled('busca_texto')){
            $query->where(function($query) use($request){
                $query->orWhere('name', 'like', '%' . $request->get('busca_texto') . '%');
            });
        }

        return $query;

    }


    /**
     * Helper para enviar email de ativação para o jovem
     * @return bool
     */
    public function sendActivationEmail()
    {
        // Se não tiver o token
        if( ! $this->cacheHas('activation_token')) {
            // Não tem como mandar
            return false;
        }
        // Chegou aqui, é pq tem o token
        Mail::to($this->email)->send(new AccountActivation($this));
        // Avisar que enviou
        return true;
    }

    public function sendResetPasswordEmail($newPassword)
    {
        //dd($this->email);
        // Chegou aqui, é pq tem o token
        Mail::to($this->email)->send(new ResetPassword($this, $newPassword));
        // Avisar que enviou
        return true;
    }

    /*
     * Functions para autorização
     */

    /**
     * @param User $user
     * @return bool
     */
    public function canCreate(User $user)
    {
        // Verificar se o cara é Owner para poder criar
        return $user->isCompanyOwner();
        // Obs, a verificação de limitação é em outra parte
    }


    /**
     * Function que verifica se o usuário pode pagar uma subscrption com os dados já prenchidos
     * @param $method
     * @return bool
     */
    public function canPaySubscriptionWith($method)
    {
        switch($method){
            // Pra pagamentos com boleto bancário
            case Subscription::SUBSCRIPTION_PAYMENT_METHOD_BANKING_BILLET:

                // Montar a array de dados obrigatórios para o método
                $required_fields = [
                    'name' => $this->name,
                    'cpf' => $this->company->cpf_or_cnpj,
                    'email' => $this->email,
                    'phonenumber' => $this->whatsapp,
                ];
                break;

            // Pra pagamentos com cartão de crédito
            case Subscription::SUBSCRIPTION_PAYMENT_METHOD_CREDIT_CARD:

                // Montar a array de dados obrigatórios para o método
                $required_fields = [
                    'name' => $this->name,
                    'cpf' => $this->company->cpf_or_cnpj,
                    'email' => $this->email,
                    'phonenumber' => $this->whatsapp,
                    'birth' => $this->birth_date,
                    'street' => $this->company->address ? $this->company->address->address_1 : null,
                    'number' => $this->company->address ? $this->company->address->number : null,
                    'neighborhood' => $this->company->address ? $this->company->address->neighborhood : null,
                    'zipcode' => $this->company->address ? $this->company->address->zip : null,
                    'city' => $this->company->address ? $this->company->address->city : null,
                    'state' => $this->company->address ? $this->company->address->state : null,
                ];
                break;

            // Se cair aqui, o método é inválido
            default:
                return false;
        }

        // Loop em todos os required fields
        foreach($required_fields as $field_name => $field_value){
            // Se o valor estiver vazio
            if(empty($field_value)){
                // Retornar false, pois tá faltando dados
                return false;
            }
        }
        // Se chegou aqui embaixo, tá válido
        return true;
    }

}
