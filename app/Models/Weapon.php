<?php

namespace App\Models;

class Weapon extends ResourceIsolatedModel {

    const WEAPON_STASH_TYPE_SHOOTER     = 1;
    const WEAPON_STASH_TYPE_COLLECTOR   = 2;
    const WEAPON_STASH_TYPE_HUNTER      = 3;

    const WEAPON_STASHES = [
        self::WEAPON_STASH_TYPE_SHOOTER => 'Atirador',
        self::WEAPON_STASH_TYPE_COLLECTOR => 'Colecionador',
        self::WEAPON_STASH_TYPE_HUNTER => 'Caçador',
    ];

    const WEAPON_STATUS_ACCEPTED                        = 'accepted';
    const WEAPON_STATUS_NOT_ACCEPTED                    = 'not_accepted';
    const WEAPON_STATUS_TEMPORARILY_NOT_ACCEPTED        = 'temporarily_not_accepted';
    const WEAPON_STATUS_PENDENCIES                      = 'pendencies';

    const WEAPON_STATUSES = [
        self::WEAPON_STATUS_ACCEPTED                    => 'Deferido',
        self::WEAPON_STATUS_NOT_ACCEPTED                => 'Indeferido',
        self::WEAPON_STATUS_TEMPORARILY_NOT_ACCEPTED    => 'Temporariamente Indeferido',
        self::WEAPON_STATUS_PENDENCIES                  => 'Exigências',
    ];



    const MASK_MAPPING = [
        'WEAPON_RECEIPT_NUMBER'                 => 'receipt_number',
        'WEAPON_TYPE'                           => 'type',
        'WEAPON_BRAND'                          => 'brand',
        'WEAPON_CALIBER'                        => 'caliber',
        'WEAPON_SERIAL_NUMBER'                  => 'serial_number',
        'WEAPON_MODEL'                          => 'model',
        'WEAPON_STASH'                          => 'stash',
        'WEAPON_DOCS_ISSUED_AT'                 => 'docs_issued_at',
        'WEAPON_SIGMA_NUMBER'                   => 'sigma_number',
        'WEAPON_STATUS'                         => 'status',
        'WEAPON_TRANSIT_PERMISSION_NUMBER'      => 'transit_permission_number',
        'WEAPON_TRANSIT_PERMISSION_EXPIRY_DATE' => 'transit_permission_expiry_date',
//        'WEAPON_CUSTOMER_ID'                    => 'customer_id',
    ];

    protected $fillable = [
        'receipt_number','type', 'brand','caliber','serial_number','model','stash',
        'sigma_number','status','transit_permission_number',
        'customer_id'
    ];

    protected $dates = [
        'docs_issued_at',
        'transit_permission_expiry_date',
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public static function rules()
    {
        return [
            'receipt_number'                    => 'required',
            'type'                              => 'required',
            'brand'                             => 'required',
            'caliber'                           => 'required',
            'serial_number'                     => 'required',
            'model'                             => 'required',
            'stash'                             => 'required',
            'transit_permission_expiry_date'    => 'nullable|date',
        ];
    }

    public static function validationMessages()
    {
        return [
            'receipt_number.required'                => 'O numero da nota fiscal é obrigatório',
            'type.required'                          => 'O tipo da arma é obrigatório',
            'brand.required'                         => 'A marca da arma é obrigatória',
            'caliber.required'                       => 'O calibre da arma é obrigatório',
            'serial_number.required'                 => 'O número de série da arma é obrigatório',
            'model.required'                         => 'O modelo da arma é obrigatório',
            'stash.required'                         => 'O acervo da arma é obrigatório',
            'transit_permission_expiry_date.date'    => 'A data de validade da guia de trânsito é inválida',
        ];
    }

    // Override no método delete padrão
    public function delete()
    {
        // Checar se pode deletar
        //if($this->customers()->count() > 0){
            // Se não puder, throw no erro
            //throw new CannotDeleteException('Não é possível excluir um clube com clientes vinculados.');
        //}
        // Retornar ok se der bom
        return parent::delete();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return sprintf('%s %s - %s', $this->brand, $this->model, $this->caliber);
    }

    /**
     * Mutator
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->getName();
    }


    public function getMaskMapping()
    {
        return self::MASK_MAPPING;
    }


    /**
     * @return string
     * @throws \Exception
     */
    public function getStash()
    {
        try {
            return self::WEAPON_STASHES[$this->stash];
        }catch(\Exception $e){
            return 'Indefinido';
        }
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getStatus()
    {
        try {
            return self::WEAPON_STATUSES[$this->status];
        }catch(\Exception $e){
            return 'Indefinido';
        }
    }


    /**
     * Essa function vai dar uma cor de texto para cada status da arma
     * @return null|string
     */
    public function getStatusClass()
    {
        switch($this->status){
            case self::WEAPON_STATUS_ACCEPTED:
                return 'text-success';
            case self::WEAPON_STATUS_NOT_ACCEPTED:
            case self::WEAPON_STATUS_TEMPORARILY_NOT_ACCEPTED:
                return 'text-danger';
            case self::WEAPON_STATUS_PENDENCIES:
                return 'text-warning';
            default:
                return null;
        }
    }


    public function getDocsIssuedAt()
    {
        return $this->docs_issued_at->formatLocalized('%d/%m/%Y');
    }

    public function getTransitPermissionExpiryDate()
    {
        return $this->transit_permission_expiry_date->formatLocalized('%d/%m/%Y');
    }
}