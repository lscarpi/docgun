<?php

namespace App\Policies;

use App\Models\Document;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DocumentPolicy extends ResourceIsolatedPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Padrão do PHP bitch
        parent::__construct();
    }

    public function share(User $user, Document $document)
    {
        return $document->canShare($user);
    }

    public function unshare(User $user, Document $document)
    {
        return $document->canShare($user);
    }
}