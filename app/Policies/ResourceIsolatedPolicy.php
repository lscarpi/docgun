<?php

namespace App\Policies;

use App\Models\ResourceIsolatedModel;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ResourceIsolatedPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function read(User $user, ResourceIsolatedModel $object)
    {
        return $object->canRead($user);
    }

    public function create(User $user, ResourceIsolatedModel $object)
    {
        return $object->canCreate($user);
    }

    public function update(User $user, ResourceIsolatedModel $object)
    {
        return $object->canUpdate($user);
    }

    public function delete(User $user, ResourceIsolatedModel $object)
    {
        return $object->canDelete($user);
    }

}
