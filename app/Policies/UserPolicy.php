<?php

namespace App\Policies;

use App\Models\Document;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy extends ResourceIsolatedPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Padrão do PHP bitch
        parent::__construct();
    }

    public function addMoreUsers(User $authenticated_user)
    {
        return $authenticated_user->company->usersRemaining() > 0;
    }

}