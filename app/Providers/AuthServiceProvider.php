<?php

namespace App\Providers;

use App\Models\Document;
use App\Models\ResourceIsolatedModel;
use App\Models\User;
use App\Policies\DocumentPolicy;
use App\Policies\ResourceIsolatedPolicy;
use App\Policies\UserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        ResourceIsolatedModel::class    => ResourceIsolatedPolicy::class,
        Document::class                 => DocumentPolicy::class,
        User::class                     => UserPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
