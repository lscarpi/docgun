<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

if (!function_exists('csrf_field')) {
    function csrf_field()
    {
        return '<input type="hidden" name="_token" value="'.csrf_token().'">';
    }
}

/**
 * Funtion que nos diz se existe alguma busca sendo realizada no sistema
 * Baseando-se nos parâmetros da request.
 *
 * @return bool
 */
function isSearchBeingMade()
{
    // Puxar todos os parâmetros da request
    $request_params = request()->all();
    // Loop nos mesmos
    foreach ($request_params as $key => $value) {
        // Se a chave começar com busca_
        if (Str::startsWith($key, 'busca_')) {
            // Tem busca sendo feita
            return true;
        }
    }
    // Se chegou aqui, não achou nada
    return false;
}

/**
 * @param $var
 * @param string $prefix
 *
 * @return stdClass
 *
 * @throws Exception
 */
function createAddressObject($var, $prefix = 'address')
{
    // Se a var informada for um object request
    if ($var instanceof Request) {
        // Passar pra array
        $var = $var->all();
    // Se não for array
    } elseif (!is_array($var)) {
        // Abandonar
        throw new \Exception('A variável informada não é uma array ou um objeto Request');
    }

    // Required fields pra montar o address
    $required_fields = ['_1', '_zip', '_number', '_neighborhood', '_city', '_state'];
    // Loop neles
    foreach ($required_fields as $field) {
        // Se por acaso tiver faltando um dos itens
        if (!isset($var[$prefix.$field])) {
            // Retornar null, não podemos prosseguir
            return null;
        }
    }

    // tentar montar e retornar o obj
    try {
        $address = new \stdClass();
        $address->address_1 = $var[$prefix.'_1'];
        $address->address_2 = isset($var[$prefix.'_2']) ? $var[$prefix.'_2'] : null;
        $address->number = $var[$prefix.'_number'];
        $address->neighborhood = $var[$prefix.'_neighborhood'];
        $address->city = $var[$prefix.'_city'];
        $address->state = $var[$prefix.'_state'];
        $address->zip = $var[$prefix.'_zip'];

        return $address;
        // Se der negativo
    } catch (\Exception $exception) {
        // Lançar outra exception mudando a mensagem
        throw new \Exception('Os dados informados não são suficientes para compor um endereço');
    }
}

// Montar address com base nos dados possíveis
function defaultAddressFormat($address)
{
    $response = $address->address_1.', '.$address->number;
    if ($address->address_2) {
        $response .= ', '.$address->address_2;
    }
    $response .= '. '.$address->neighborhood.', '.$address->city.'-'.$address->state.'. '.$address->zip;

    return $response;
}

function createDateFromBRFormat($date)
{
    try {
        return \Carbon\Carbon::createFromFormat('d/m/Y', $date);
    } catch (\Exception $e) {
        return null;
    }
}

// Helpers apenas
function printDate(\Carbon\Carbon $date = null, $format)
{
    if ($date) {
        return utf8_encode($date->formatLocalized($format));
    }

    return null;
}

// Helpers apenas
function defaultDateFormat(\Carbon\Carbon $date = null)
{
    return printDate($date, '%d/%m/%Y');
}

// Helpers apenas
function defaultVerboseDateFormat(\Carbon\Carbon $date = null)
{
    return printDate($date, '%d de %B de %Y');
}

// Helpers apenas
function defaultDateTimeFormat(\Carbon\Carbon $date = null)
{
    return printDate($date, '%d/%m/%Y %H:%M:%S');
}
// Helpers apenas
function defaultTimeFormat(\Carbon\Carbon $date = null)
{
    return printDate($date, '%H%:%M:%S');
}

// Teste do Gitlab apenas
function testGitlab()
{
    return 'Opa teste do Commit do Gitlab';
}

//Cria lots of fake Customers
function spamCustomers(\App\Models\User $owner, $total = 100)
{
    auth()->login($owner);

    $faker = Faker\Factory::create();

    foreach (range(1, $total) as $index) {
        $date = $faker->dateTimeThisYear;
        $customer = new App\Models\Customer();
        $customer->name = $faker->name;
        $customer->email = $faker->email;
        $customer->created_at = $date;
        $customer->updated_at = $date;
        $customer->save();
        echo 'Criado customer '.$customer->name.PHP_EOL;
    }
}

//Cria lots of fake Documents
function spamDocuments(\App\Models\User $owner, $total = 20)
{
    auth()->login($owner);

    $faker = Faker\Factory::create();

    foreach (range(1, $total) as $index) {
        $date = $faker->dateTimeThisYear;
        $document = new App\models\Document();
        $document->title = $faker->sentence;
        $document->content = $faker->paragraphs(3, true);
        $document->created_at = $date;
        $document->updated_at = $date;
        $document->save();
        echo 'Criado document '.$document->title.PHP_EOL;
    }
}

//Cria lots of fake IssuedDocuments
function spamIssuedDocuments(\App\Models\User $owner, $total = 20)
{
    auth()->login($owner);

    $customers = $owner->company->customers;
    $documents = $owner->company->documents;

    $faker = Faker\Factory::create();

    foreach (range(1, $total) as $index) {
        $date = $faker->dateTimeThisYear;
        $issued_document = new App\models\IssuedDocument();
        $issued_document->title = $faker->sentence;
        $issued_document->content = $faker->paragraphs(3, true);
        $issued_document->created_at = $date;
        $issued_document->updated_at = $date;
        $issued_document->customer_id = $customers->random()->id;
        $issued_document->document_id = $documents->random()->id;
        $issued_document->save();
        echo 'Criado document '.$issued_document->title.PHP_EOL;
    }
}

/**
 * Helper.
 *
 * @return bool
 */
function isMasterServer()
{
    return env('MASTER_SERVER') == true;
}

/**
 * Helper.
 *
 * @return bool
 */
function isProductionServer()
{
    return env('APP_ENV') == 'production';
}

/**
 * Helper.
 *
 * @return bool
 */
function isTestServer()
{
    return env('APP_ENV') == 'test';
}

/**
 * Helper.
 *
 * @return bool
 */
function isDevelopmentServer()
{
    return env('APP_ENV') == 'development';
}

/**
 * Helperzinho para mostrar mensagens no singular ou plural
 * Basicamente comparamos se o valor é 1, se for, mostra-se a mensagem no sigular
 * Se não for, mostramos no plural.
 *
 * @param $value
 * @param $text_singular
 * @param $text_plural
 *
 * @return string
 */
function pluralize($value, $text_singular, $text_plural)
{
    // Descobrir qual text vamos usar
    $text = ($value == 1 ? $text_singular : $text_plural);
    // Jogar no sprintf
    return sprintf('%s %s', $value, $text);
}

/**
 * Function para printar grana em formato BR BR.
 *
 * @param $value
 *
 * @return string
 */
function moneyFormat($value)
{
    return number_format($value, 2, ',', '.');
}

/**
 * Helper para tirar a formatação do CPF.
 *
 * @param $cpf
 *
 * @return mixed
 */
function cleanCPFFormatting($cpf)
{
    $cpf = str_replace('.', '', $cpf);
    $cpf = str_replace('-', '', $cpf);

    return $cpf;
}

function cleanPhoneNumberFormatting($phone_number)
{
    $phone_number = str_replace('(', '', $phone_number);
    $phone_number = str_replace(')', '', $phone_number);
    $phone_number = str_replace('-', '', $phone_number);
    $phone_number = str_replace(' ', '', $phone_number);
    $phone_number = str_replace('+55', '', $phone_number);

    return $phone_number;
}

function cleanZipCodeFormatting($zip_code)
{
    $zip_code = str_replace('-', '', $zip_code);

    return $zip_code;
}

// TODO CHECK
function cleanBirthDateFormatting($birth_date)
{
    $birth_date = str_replace('/', '-', $birth_date);

    return $birth_date;
}

function notifySystemAdmins($mailbale)
{
    // Notificar os emails definidos de que um novo usuário se registrou
    foreach (config('docgun.general.system_notifications.notify_to') as $email_address) {
        // That's it champs
        Mail::to($email_address)->send($mailbale);
    }
}

function fetchEmailsForRobs()
{
    $links = [
        'http://www.sindespms.com.br/despachantes-credenciados',
        'https://www.defesa.org/como-comprar-uma-arma-legalmente/lista-dos-despachantes-de-armamento/',
        'https://www.usemilitar.com.br/blog/lista-dos-despachantes-de-armamento/',
        'http://www.pf.gov.br/servicos-pf/armas/credenciamento-de-instrutores/listagem-de-instrutores-de-armamento-e-tiro',
        'http://site.sindespees.com.br/site/despachantes',
    ];

    $json = '["https://www.defesa.org/estandes-e-clubes-de-tiro/regiao-centro-oeste/distrito-federal/","https://www.defesa.org/estandes-e-clubes-de-tiro/regiao-centro-oeste/goias/","https://www.defesa.org/estandes-e-clubes-de-tiro/regiao-centro-oeste/mato-grosso/","https://www.defesa.org/estandes-e-clubes-de-tiro/regiao-centro-oeste/mato-grosso-do-sul/","https://www.defesa.org/estandes-e-clubes-de-tiro/regiao-nordeste/alagoas/","https://www.defesa.org/estandes-e-clubes-de-tiro/regiao-nordeste/bahia/","https://www.defesa.org/estandes-e-clubes-de-tiro/regiao-nordeste/ceara/","https://www.defesa.org/estandes-e-clubes-de-tiro/regiao-nordeste/maranhao/","https://www.defesa.org/estandes-e-clubes-de-tiro/regiao-nordeste/paraiba/","https://www.defesa.org/estandes-e-clubes-de-tiro/regiao-nordeste/pernambuco/","https://www.defesa.org/estandes-e-clubes-de-tiro/regiao-nordeste/piaui/","https://www.defesa.org/estandes-e-clubes-de-tiro/regiao-nordeste/rio-grande-do-norte/","https://www.defesa.org/estandes-e-clubes-de-tiro/regiao-nordeste/sergipe/","https://www.defesa.org/estandes-e-clubes-de-tiro/regiao-norte/acre/","https://www.defesa.org/estandes-e-clubes-de-tiro/regiao-norte/amapa/","https://www.defesa.org/estandes-e-clubes-de-tiro/regiao-norte/amazonas/","https://www.defesa.org/estandes-e-clubes-de-tiro/regiao-norte/para/","https://www.defesa.org/estandes-e-clubes-de-tiro/regiao-norte/roraima/","https://www.defesa.org/estandes-e-clubes-de-tiro/regiao-norte/tocantins/","https://www.defesa.org/estandes-e-clubes-de-tiro/regiao-sudeste/espirito-santo/","https://www.defesa.org/estandes-e-clubes-de-tiro/regiao-sudeste/minas-gerais/","https://www.defesa.org/estandes-e-clubes-de-tiro/regiao-sudeste/rio-de-janeiro/","https://www.defesa.org/estandes-e-clubes-de-tiro/regiao-sudeste/sao-paulo/","https://www.defesa.org/estandes-e-clubes-de-tiro/regiao-sul/parana/","https://www.defesa.org/estandes-e-clubes-de-tiro/regiao-sul/rio-grande-do-sul/","https://www.defesa.org/estandes-e-clubes-de-tiro/regiao-sul/santa-catarina/"]';
    $links = array_merge($links, json_decode($json));

    $path = '/home/lscarpi/emails.txt';

    $found_emails = [];

    if (File::exists($path)) {
        File::delete($path);
    }

    File::append($path, '"email",'.PHP_EOL);
    $regex = '/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/i';
    foreach ($links as $link) {
        try {
            echo 'Fetching '.$link.PHP_EOL;
            $contents = file_get_contents($link);
            if ($contents) {
                preg_match_all($regex, $contents, $matches);
                foreach ($matches[1] as $email) {
                    $email = strtolower($email);
                    if (!in_array($email, $found_emails)) {
                        File::append($path, '"'.$email.'",'.PHP_EOL);
                        $found_emails[] = $email;
                    }
                }
            }
        } catch (Exception $e) {
            continue;
        }
    }
}

function runLelel()
{
    $path = '/home/lscarpi/Desktop/';
    $jsonFile = 'http://52.67.11.36/configuracoesemails/servEmails';
    $csvFile = $path.'lel_'.uniqid().'.csv';

    $lelFile = file_get_contents($jsonFile);

    $decodedLelFile = json_decode($lelFile);

    $newFile = '"email","password","host", "server"'.PHP_EOL;

    if (file_exists($csvFile)) {
        unlink($csvFile);
    }

    foreach ($decodedLelFile as $randomPrick) {
        if (!property_exists($randomPrick, 'email')
            || $randomPrick->email == ''
            || !property_exists($randomPrick, 'senhaEmail')
            || $randomPrick->senhaEmail == ''
         ) {
            continue;
        }

        echo 'Parsing a random prick by the email of '.$randomPrick->email.PHP_EOL;
        $newLine = '"'.($randomPrick->email ?? null).'",';
        $newLine .= '"'.($randomPrick->senhaEmail ?? null).'",';
        $newLine .= '"'.($randomPrick->host ?? null).'",';
        $newLine .= '"'.($randomPrick->servidor ?? null).'"'.PHP_EOL;

        $newFile .= $newLine;
    }

    File::put($csvFile, $newFile);
}
