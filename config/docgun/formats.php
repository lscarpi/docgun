<?php

use App\Library\DateHelper;

return [

    /*
    |--------------------------------------------------------------------------
    | Formatos - PT-BR
    |--------------------------------------------------------------------------
    | Aqui vamos salvar todos os formatos de data/hora e afins para a linguagem
    */

    'carbon' => [
        'date'      => '%d/%m/%Y',
        'time'      => '%H:%M:%S',
        'datetime'  => '%d/%m/%Y às %H:%M:%S',
    ],
    'raw' => [
        'date'      => 'd/m/Y',
        'time'      => 'H:i:s',
        'datetime'  => 'd/m/Y H:i:s',
        'money'     => '0.00,00'
    ],
    'masks'=> [
        'date'      => '00/00/0000',
        'money'     => '#.##0,00',
    ],

    'date_intervals' => [
        DateHelper::INTERVAL_TYPE_SECOND   => 'Segundo(s)',
        DateHelper::INTERVAL_TYPE_MINUTE   => 'Minuto(s)',
        DateHelper::INTERVAL_TYPE_HOUR     => 'Hora(s)',
        DateHelper::INTERVAL_TYPE_DAY      => 'Dia(s)',
        DateHelper::INTERVAL_TYPE_WEEK     => 'Semana(s)',
        DateHelper::INTERVAL_TYPE_MONTH    => 'Mês(es)',
        DateHelper::INTERVAL_TYPE_YEAR     => 'Ano(s)',
    ],
];
