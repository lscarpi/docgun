<?php

use App\Library\SourceControl\Providers\GitLab;
use App\Library\SubscriptionProviders\Pagseguro\SubscriptionProvider;

return [

    'subscriptions' => [
        'current_provider' => SubscriptionProvider::class
    ],

    'trial' => [
        'plan_id' => 1,
        'period_in_days' => 18
    ],

    'system_notifications' => [
        'notify_to' => [
            'lucasscarpi@gmail.com',
            'mnzt.docgun@gmail.com',
            'dr0ll@icloud.com',
        ],
    ],

    'error_reporting' => [
        'current_version_control_handler' => GitLab::class,
    ],

    'marketing_automation' => [
        'ville_target' => [
            'api_key'       => 'NXrZXHuf3dy6QaNVR2IaRWFBUsSiilQxTHn3DXnG72jBTAJHuWrDz14xwxQe',
            'list_uid'      => 'ARoIP5cb61ae2cc1d1',
        ],
    ],


];