<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');

            // Dados básicos
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('gender', 1)->nullable();
            $table->date('birth_date')->nullable();
            $table->string('cpf_cnpj')->nullable();
            $table->string('registration_number')->nullable();
            $table->date('registration_number_expedition_date')->nullable();
            $table->string('voter_id')->nullable();
            $table->string('civil_status')->nullable();

            // CR
            $table->string('cr_number')->nullable();
            $table->string('cr_category')->nullable();
            $table->date('cr_expedition_date')->nullable();
            $table->date('cr_expiration_date')->nullable();
            $table->string('rm_number')->nullable();

            // Complementar
            $table->string('naturality')->nullable();
            $table->string('nacionality')->nullable();
            $table->string('profession')->nullable();
            $table->string('fathers_name')->nullable();
            $table->string('mothers_name')->nullable();

            // Endereço Residencial
            $table->text('residential_address')->nullable();
            $table->string('residential_phone')->nullable();
            // Endereço Comercial
            $table->text('comercial_address')->nullable();
            $table->string('comercial_phone')->nullable();
            // Clube Filiado
            $table->integer('club_id')->unsigned()->nullable();

            $table->timestamps();

            $table->foreign('club_id')
                ->references('id')
                ->on('clubs')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
