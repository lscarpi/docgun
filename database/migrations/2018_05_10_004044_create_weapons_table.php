<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeaponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weapons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('receipt_number');
            $table->string('type');
            $table->string('brand');
            $table->string('caliber');
            $table->string('serial_number');
            $table->string('model');
            $table->integer('stash');
            $table->date('docs_issued_at')->nullable();
            $table->string('sigma_number')->nullable();
            $table->string('status')->nullable();
            $table->string('transit_permission_number')->nullable();
            $table->date('transit_permission_expiry_date')->nullable();
            $table->integer('customer_id')->unsigned();
            $table->timestamps();

            $table->foreign('customer_id')
                ->references('id')
                ->on('customers')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weapons');
    }
}
