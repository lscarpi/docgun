jQuery(document).ready(function($){
    if(! $.fn.datepicker){
        alert('Erro no plugin datepicker. Por favor, fale com o administrador!');
        return false;
    }

    $('.init-datepicker').each(function(){
        $(this).datepicker({
            autoclose: true,
            language: 'pt-BR'
        })
    });
});