

jQuery(document).ready(function($){

    if( ! $.fn.mask){
        alert('Error no plugin mask! Contate o administrador.');
        return false;
    }

    bindCepFields();

});

function bindCepFields(){
    $('.cep').mask('00000-000', {
        onComplete: function(cep, event, current_field){
            updateAddress(cep, current_field);
        }
    });
}

function updateAddress(cep, current_field){

    // Tratar CEP
    // let cep_correto = [cep.slice(0, 5), '-', cep.slice(5)].join('');
    // console.log(cep_correto);

    let
        address_container = $(current_field).closest('.address'),
        field_state = address_container.find('input.state'),
        field_city = address_container.find('input.city'),
        field_neighborhood = address_container.find('input.neighborhood'),
        field_address_1 = address_container.find('input.address_1'),
        field_address_2 = address_container.find('input.address_2');

    cep = cep.replace('-', '');

    $.get("https://viacep.com.br/ws/" + cep +"/json/",
        function(result){

            if( ! result.logradouro){

                Swal.fire({
                    title: "CEP não encontrado",
                    text: "Por favor, digite o restante do endereço",
                    type: "error",
                    // showCancelButton: true,
                    confirmButtonColor: '#2f5b38',
                    confirmButtonText: 'Entendi',
                    // cancelButtonText: "Não",
                }).then((result) => {
                    console.log(result);
                    return true
                });

                return;
            }

            field_state.val(result.uf);
            field_city.val(result.localidade);
            field_neighborhood.val(result.bairro);
            field_address_1.val(result.logradouro);
            field_address_2.val(result.complemento);
        }
    );
}

//Take off from base view and insert only where needed
//guaravivs
//https://apps.widenet.com.br/busca-cep/api-de-consulta