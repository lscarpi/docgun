@extends('layout', [
    'page_title' => 'Dados da Empresa',
])

@section('content')
    <form method="POST">
        {{csrf_field()}}
        <div class="card">
            <div class="card-header">
                Dados da Empresa
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h5>Dados gerais</h5>
                        <hr>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Nome Fantasia</label>
                            <input type="text" class="form-control" name="name" value="{{$company->name}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="legal_name">Razão Social</label>
                            <input type="text" class="form-control" name="legal_name" value="{{$company->legal_name}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="cpf_or_cnpj">CNPJ/CPF</label>
                            <input type="text" class="form-control" name="cpf_or_cnpj" value="{{$company->cpf_or_cnpj}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email">Email de Contato</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-at"></i>
                                </div>
                                <input type="email" class="form-control" name="email" value="{{$company->email}}">
                            </div>
                        </div>
                    </div>
                </div>

                <h5 style="margin-top: 30px;">Endereço</h5>
                <hr>
                <div class="address">
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label>CEP</label>
                                <input class="form-control cep" type="text" name="address_zip" placeholder="CEP" value="{{$company->address->zip ?? null}}" id="commercial_cep">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Endereço</label>
                                <input class="form-control address_1" type="text" name="address_1" placeholder="Endereço do cliente" value="{{$company->address->address_1 ?? null}}" id="address_1">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Complemento</label>
                                <input class="form-control address_2" type="text" name="address_2" value="{{$company->address->address_2 ?? null}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label>Número</label>
                                <input class="form-control number" type="text" name="address_number" value="{{$company->address->number ?? null}}">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Bairro</label>
                                <input class="form-control neighborhood" type="text" name="address_neighborhood" value="{{$company->address->neighborhood ?? null}}" id="address_neighborhood">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Cidade</label>
                                <input class="form-control city" type="text" name="address_city" value="{{$company->address->city ?? null}}" id="address_city">
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label>UF</label>
                                <input class="form-control state" type="text" name="address_state" value="{{$company->address->state ?? null}}" id="address_state">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="crud-button">
                    <a href="{{route('get_dashboard')}}" class="btn btn-outline-danger"><i class="fa fa-reply"></i> Voltar</a>
                    <button type="submit" class="btn btn_default_color pull-right">
                        <i class="fa fa-check"></i>
                        Salvar alterações
                    </button>
                </div>
            </div>
        </div>
    </form>
@stop

@section('extras_js')
    <script src="{{asset('assets/plugins/jquery-mask/jquery.mask.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/cep/cep.js') }}"></script>
@stop