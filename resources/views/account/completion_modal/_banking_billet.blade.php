
<input type="hidden" name="method" value="{{$method}}">

<div>
    <div class="block">
        <div>
            <div class="block-title">
                <h5>Dados Pessoais</h5>
            </div>
            <div class="block-content">
                <div class="row">
                    <div class="form-group col-md-8">
                        <label for="">Nome</label>
                        <input type="text" name="name" class="form-control" value="{{$user->name}}" required>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="">CPF</label>
                        <input type="text" name="cpf_or_cnpj" class="form-control cpf" value="{{$user->company->cpf_or_cnpj}}" required>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="">Email</label>
                        <input type="email" name="email" class="form-control" value="{{$user->email}}" required>
                    </div>

                    <div class="form-group col-md-3">
                        <label for="">WhatsApp</label>
                        <input type="text" name="whatsapp" class="form-control" value="{{$user->whatsapp}}" required>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>