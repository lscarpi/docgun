
<input type="hidden" name="method" value="{{$method}}">

<div>
    <div class="block">
        <div>
            <div class="block-title">
                <h5>Dados Pessoais</h5>
            </div>
            <div class="block-content">
                <div class="row">
                    <div class="form-group col-md-8">
                        <label for="">Nome</label>
                        <input type="text" name="name" class="form-control" value="{{$user->name}}" required>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="">CPF</label>
                        <input type="text" name="cpf_or_cnpj" class="form-control cpf" value="{{$user->company->cpf_or_cnpj}}" required>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="">Email</label>
                        <input type="email" name="email" class="form-control" value="{{$user->email}}" required>
                    </div>

                    <div class="form-group col-md-3">
                        <label for="">WhatsApp</label>
                        <input type="text" name="whatsapp" class="form-control" value="{{$user->whatsapp}}" required>
                    </div>

                    <div class="form-group col-md-3">
                        <label for="">Data de Nascimento</label>
                        <input type="text" name="birth_date" class="form-control date" value="{{$user->birth_date}}" required>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="block">
            <div class="block-title">
                <h5>Endereço</h5>
            </div>
            <div class="block-content">
                <div class="address">
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label>CEP</label>
                                <input class="form-control cep" type="text" name="address_zip" placeholder="CEP" value="{{$company->address->zip ?? null}}" id="commercial_cep">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Endereço</label>
                                <input class="form-control address_1" type="text" name="address_1" placeholder="Endereço do cliente" value="{{$company->address->address_1 ?? null}}" id="address_1">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Complemento</label>
                                <input class="form-control address_2" type="text" name="address_2" value="{{$company->address->address_2 ?? null}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label>Número</label>
                                <input class="form-control number" type="text" name="address_number" value="{{$company->address->number ?? null}}">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Bairro</label>
                                <input class="form-control neighborhood" type="text" name="address_neighborhood" value="{{$company->address->neighborhood ?? null}}" id="address_neighborhood">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Cidade</label>
                                <input class="form-control city" type="text" name="address_city" value="{{$company->address->city ?? null}}" id="address_city">
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label>UF</label>
                                <input class="form-control state" type="text" name="address_state" value="{{$company->address->state ?? null}}" id="address_state">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>






{{--

    // Montar a array de dados obrigatórios para o método
                $required_fields = [
                    'name' => $this->name,
                    'cpf' => $this->company->cpf_or_cnpj,
                    'email' => $this->email,
                    'phonenumber' => $this->whatsapp,
                    'birth' => $this->birth_date,
                    'street' => $this->company->address ? $this->company->address->address_1 : null,
                    'number' => $this->company->address ? $this->company->address->number : null,
                    'neighborhood' => $this->company->address ? $this->company->address->neighborhood : null,
                    'zipcode' => $this->company->address ? $this->company->address->zip : null,
                    'city' => $this->company->address ? $this->company->address->city : null,
                    'state' => $this->company->address ? $this->company->address->state : null,
                ];

--}}