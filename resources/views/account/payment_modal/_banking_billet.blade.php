@extends('base', [
    'body_class' => 'white'
])

@section('extras_css')
    <style>
        /*.barcode {*/
            /*border: solid 1px grey;*/
        /*}*/

        .banking-billet-details {
            font-family: "Quicksand", sans-serif;
            font-size: 1rem;
        }

        #previewBankingBillet {
            margin-top: 1rem;
        }
    </style>
@stop

@section('page')
    <div class="container" id="previewBankingBillet">


            <div class="card" style="border-radius: 10px">

                @if(isset($payment) && $payment instanceof \App\Library\SubscriptionProviders\Models\BankingBilletPaymentDetails)
                {{--<div class="card-header">--}}
                {{--</div>--}}
                    <div class="card-body banking-billet-details">
                        <h4 class="card-title">Detalhes do boleto</h4>
                        <div style="margin-top: 1.5rem">
                            <p style="font-weight: 300"><strong>Valor: </strong>R$ {{moneyFormat($payment->amount)}}</p>
                            <p style="font-weight: 300"><strong>Vencimento: </strong>{{$payment->due_date->formatLocalized('%d/%m/%Y')}}</p>
                        </div>
                        <div class="form-group barcode">
                            <label for="">Código de Barras</label>
                            <input type="text" class="form-control" id="bankingBilletBarcode" value="{{$payment->barcode}}" style="border-radius: 15px;">
                        </div>
                        <div align="right">
                            <button class="btn btn-outline-info" id="copyBarcode" data-clipboard-target="#bankingBilletBarcode"><i class="fa fa-barcode-o"></i> Copiar Código de Barras</button>
                            {{--<a href="" target="" class="btn btn-outline-info" data-clipboard-target="#bankingBilletBarcode"><i class="fa fa-barcode-o"></i> Copiar Código de Barras</a>--}}
                            <a href="{{$payment->view_url}}" target="_blank" class="btn btn-primary"><i class="fa fa-file-pdf-o"></i> Visualizar Boleto</a>
                        </div>
                    </div>
                @else
                    {{--<div class="card">--}}
                        {{--<div class="card-header"></div>--}}
                        <div class="card-body">
                            <div class="text-center">
                                <img src="{{asset('assets/img/errors/500.png')}}" style="width: 250px; margin-top: -30px;" alt="">
                            </div>
                            <div class="text-center" style="margin-top: 50px">
                                <h3>Ocorreu um erro interno ao gerar seu Boleto. Por favor, feche a janela e tente novamente.</h3>
                            </div>
                        </div>
                        {{--<div class="card-footer"></div>--}}
                    {{--</div>--}}
                @endif
            </div>


    </div>
@stop


@section('extras_js')
    <script src="{{asset('assets/plugins/clipboardjs/clipboardjs.min.js')}}"></script>
    <script>
        jQuery(document).ready(function($){
            let
                copyBarcodeButton = $('#copyBarcode'),
                clipboard = new ClipboardJS('#copyBarcode');

            clipboard.on('success', function(e) {
                copyBarcodeButton.attr('title', 'Copiado!');
                copyBarcodeButton.tooltip('show');
                setTimeout(function(){
                    copyBarcodeButton.tooltip('dispose');
                    copyBarcodeButton.attr('title', '');
                }, 3000);
            });
        });
    </script>
@stop



