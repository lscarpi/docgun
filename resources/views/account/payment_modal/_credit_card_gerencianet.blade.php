@extends('layout')

@section('extras_css')
    <link rel="stylesheet" href="{{asset('assets/plugins/card-js/card.css')}}">
    <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-toggle/bootstrap-toggle.min.css')}}">
@stop

@section('content')
    <div class="container loader text-center">
        <i class="fa fa-spinner fa-spin" style="font-size: 80px; margin-top: 50px;"></i> <br />
        <br />
        <h3>Carregando</h3>
    </div>
    <div class="container loaded" style="display: none;">
        <div class="container">
            <div class="credit-card-payment-modal">
                    <div class="row">
                        <div class="col-md-6">
                            <form method="post" id="credit_card_form">
                            <div class="credit-card-gerencianet text-center">
                                <img src="{{asset('assets/img/gerencianet.png')}}" alt="" style="background-repeat: no-repeat; max-width: 75%">
                            </div>
                            <div class='card-wrapper'></div>

                            <div class="credit-card-payment-data mt-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                    <input  class="form-control" type="text" name="number"    id="cc_number"  placeholder="Número" required>
                                </div>
                            </div>
                            <div class="credit-card-payment-data">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input class="form-control" type="text" name="name"      id="cc_name"  placeholder="Nome"  required>
                                </div>
                            </div>
                            <div class="credit-card-payment-data">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input class="form-control" type="text" name="expiry"    id="cc_expiry"  placeholder="Validade" required>
                                </div>
                            </div>
                            <div class="credit-card-payment-data">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-shield"></i></span>
                                    <input class="form-control" type="text" name="cvc"       id="cc_cvv"    placeholder="CVC" required>
                                </div>
                            </div>

                            <div class="form-group text-center mt-4">
                                <button type="submit" class="btn btn-lg btn-success mb-3" style="background-color: #44824E; border-color: #44824E"><i class="fa fa-credit-card-alt"></i> Concluir pagamento</button>
                                <footer class="blockquote-footer"><cite>Este pagamento será processado de maneira segura através da plataforma
                                        <a href="">Gerencianet</a>.</cite></footer>
                            </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <div style="margin-top: 25px;">
                                <input type="checkbox" checked class="bootstrap-toggle-btn" id="toggle-one"  onchange="creditCardOwner()" data-toggle="toggle" data-on="Cartão de crédito pertence ao usuário" data-off="Cartão de crédito NÃO pertence ao usuário">
                            </div>

                            {{--DADOS PESSOAIS--}}
                            <form method="post" id="payment_form">
                                <input type="hidden" name="name" id="hidden-name" value="{{$user->name}}">
                                <div class="credit-card-payment-data">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-id-card-o"></i></span>
                                        <input type="text" name="cpf_or_cnpj" id="cpf_or_cnpj" class="form-control cpf additional-data" placeholder="CPF" value="{{$user->company->cpf_or_cnpj}}" required readonly>
                                    </div>
                                </div>
                                <div class="credit-card-payment-data">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-at"></i></span>
                                        <input type="email" name="email" id="email" class="form-control additional-data" placeholder="Email" value="{{$user->email}}" required readonly>
                                    </div>
                                </div>
                                <div class="credit-card-payment-data">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-whatsapp"></i></span>
                                        <input type="text" name="whatsapp" id="whatsapp" class="form-control additional-data" placeholder="Whatsapp" value="{{$user->whatsapp}}" required readonly>
                                    </div>
                                </div>
                                <div class="credit-card-payment-data">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                                        <input type="text" name="birth_date" id="birth_date" class="form-control date additional-data" placeholder="Data de Nascimento" value="{{\App\Library\DateHelper::toDateString($user->birth_date)}}" required readonly>
                                    </div>
                                </div>

                                {{--ENDERECO--}}
                                <div class="credit-card-payment-data">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                        <input class="form-control cep additional-data" id="address_zip" type="text" name="address_zip" placeholder="CEP" value="{{$company->address->zip ?? null}}" id="commercial_cep">
                                    </div>
                                </div>
                                <div class="credit-card-payment-data">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                        <input class="form-control address_1 additional-data" id="address_1" type="text" name="address_1" placeholder="Endereço do cliente" value="{{$company->address->address_1 ?? null}}" id="address_1">
                                    </div>
                                </div>
                                <div class="credit-card-payment-data">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                        <input class="form-control address_2 additional-data" id="address_2" type="text" name="address_2" placeholder="Complemento" value="{{$company->address->address_2 ?? null}}">
                                    </div>
                                </div>
                                <div class="credit-card-payment-data">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                        <input class="form-control number additional-data" id="address_number" type="text" name="address_number" placeholder="Número" value="{{$company->address->number ?? null}}">
                                    </div>
                                </div>
                                <div class="credit-card-payment-data">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                        <input class="form-control neighborhood additional-data" id="address_neighborhood" type="text" name="address_neighborhood" placeholder="Bairro" value="{{$company->address->neighborhood ?? null}}" id="address_neighborhood">
                                    </div>
                                </div>
                                <div class="credit-card-payment-data">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                        <input class="form-control city additional-data" type="text" id="address_city" name="address_city" placeholder="Cidade" value="{{$company->address->city ?? null}}" id="address_city">
                                    </div>
                                </div>
                                <div class="credit-card-payment-data">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                        <input class="form-control state additional-data" type="text" id="address_state" name="address_state" placeholder="UF" value="{{$company->address->state ?? null}}" id="address_state">
                                    </div>
                                </div>
                                {{csrf_field()}}
                                <input type="hidden" name="payment_token">
                            </form>
                        </div>
                    </div>

            </div>
        </div>






    </div>
@stop


@section('extras_js')
    <script src="{{asset('assets/plugins/card-js/jquery.card.js')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap-toggle/bootstrap-toggle.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datepicker/datepicker.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datepicker/pt-br.js')}}"></script>
    <script src="{{asset('assets/plugins/jquery-mask/jquery.mask.min.js')}}"></script>
    {{-- Se for production server --}}
    @if(isProductionServer())
        {{-- Colocar o code do production --}}
        <script type='text/javascript'>var s=document.createElement('script');s.type='text/javascript';var v=parseInt(Math.random()*1000000);s.src='https://api.gerencianet.com.br/v1/cdn/1b59161d77fed193d10aa00add10ad2c/'+v;s.async=false;s.id='1b59161d77fed193d10aa00add10ad2c';if(!document.getElementById('1b59161d77fed193d10aa00add10ad2c')){document.getElementsByTagName('head')[0].appendChild(s);};$gn={validForm:true,processed:false,done:{},ready:function(fn){$gn.done=fn;}};</script>
        {{-- Se for outro tipo --}}
    @else
        {{-- Colocar o code do develop --}}
        <script type='text/javascript'>var s=document.createElement('script');s.type='text/javascript';var v=parseInt(Math.random()*1000000);s.src='https://sandbox.gerencianet.com.br/v1/cdn/1b59161d77fed193d10aa00add10ad2c/'+v;s.async=false;s.id='1b59161d77fed193d10aa00add10ad2c';if(!document.getElementById('1b59161d77fed193d10aa00add10ad2c')){document.getElementsByTagName('head')[0].appendChild(s);};$gn={validForm:true,processed:false,done:{},ready:function(fn){$gn.done=fn;}};</script>
    @endif


    <script>
        $gn.ready(function(checkout) {

            let payment_token_callback = function(error, response) {
                if(error) {
                    if(error.error_description) {
                        swal('', error.code + ' - ' + error.error_description, 'error');
                    }
                } else {
                    if(response.code && response.code === 200){
                        $('[name=payment_token]').val(response.data.payment_token);
                        $('#payment_form').submit();
                    }
                }
            };

            jQuery(document).ready(function ($) {

                $('.container.loader').slideUp();
                $('.container.loaded').slideDown();
                $('.date').mask('00/00/0000');

                // Ao digitar o cc_name
                $(document).on('keyup', '#cc_name', function(){
                    // Alterar o val do hidden name
                    $('#hidden-name').val($(this).va    l());
                });

                //Invalidar o submit original
                $(document).on('submit', '#credit_card_form', function (e) {
                    e.preventDefault();
                    e.stopImmediatePropagation();

                    // Ao submit, gerar o payment token do gerencianet
                    let
                        cc_number = $('#cc_number').val(),
                        cc_brand = getCardType(cc_number),
                        cc_cvv = $('#cc_cvv').val(),
                        cc_expiry = $('#cc_expiry').val(),
                        cc_expiry_month = cc_expiry.split('/')[0].trim(),
                        cc_expiry_year = cc_expiry.split('/')[1].trim();


                        checkout.getPaymentToken({
                            brand: cc_brand, // bandeira do cartão
                            number: cc_number, // número do cartão
                            cvv: cc_cvv, // código de segurança
                            expiration_month: cc_expiry_month, // mês de vencimento
                            expiration_year: cc_expiry_year // ano de vencimento
                        }, payment_token_callback);


                    return false;
                });

                $('#credit_card_form').card({
                    container: '.card-wrapper',
                });

            });

            function getCardType(card_number) {
                const cards = Payment.getCardArray();
                for (let i = 0; i < cards.length; i += 1) {
                    if (cards[i].pattern.test(card_number)) {
                        return cards[i].type;
                    }
                }
                return undefined;
            }
        });
    </script>

    <script>
        $(document).ready(function () {
            creditCardOwner();

            $('.cpf').mask('000.000.000-00', {reverse: true});
        });

        // $('#toggle-one').on("click", creditCardOwner);

        $(function() {
            $('#toggle-one').change(function() {
                let isOwner = $("#toggle-one").is(":checked");
                additionalData = $("input.additional-data");

                if (isOwner){
                    additionalData.prop("readonly", true);
                }
                additionalData.prop("readonly", false);
            });
        });

        function creditCardOwner() {
            let isOwner = $("#toggle-one").is(":checked");
                additionalData = $("input.additional-data");

            if (isOwner){
                additionalData.prop("readonly", true);
                // additionalData.val("");

            }
            else{
                additionalData.prop("readonly", false);

            }

        }
    </script>

    <script>

        //salva os dados do user em uma array dentro de um json_encode
        let customerData = '{!!json_encode([
        'cpf_or_cnpj' => $company->cpf_or_cnpj,
        'email' => $user->email,
        'whatsapp' => $user->whatsapp,
        'birth_date' => \App\Library\DateHelper::toDateString($user->birth_date),
        'address_zip' => $company->address->zip,
        'address_1' => $company->address->address_1,
        'address_2' => $company->address->address_2,
        'address_number' => $company->address->number,
        'address_neighborhood' => $company->address->neighborhood,
        'address_city' => $company->address->city,
        'address_state' => $company->address->state,
        ])!!}';
        //parseia a array para um objeto
        customerData = JSON.parse(customerData);

        let additionalData = $('.additional-data');


        $(document).on('click', '.toggle-group', function () {

            let isToggledOn = $('#toggle-one').is(':checked');

            if (isToggledOn){
                additionalData.val('');
            }
            else{

            Object.keys(customerData).forEach(function (key) {
                $('#' + key).val(customerData[key]);
            });

            }
        });

    </script>
@stop
