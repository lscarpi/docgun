    @extends('layout')

    @section('content')
        <div style="display: flex; height: 100%; justify-content: center">
            <div class="flex-row align-self-center text-center" style="width: 100%;">
                <img width="200px;" src="{{asset('assets/img/docgun-upgrade.png')}}" alt="">
                <h3 class="">Obrigado pela parceria!</h3>
                <p>Sua assinatura será confirmada pelo nosso provedor em instantes, e seus recursos liberados.</p>
                <a href="{{route('get_dashboard')}}" class="btn btn_default_color">
                    <i class="fa fa-dashboard"></i>
                    Voltar para Dashboard
                </a>
            </div>
        </div>
    @endsection