    @extends('layout', [
    'page_title' => 'Meu Perfil',
])

@section('extras_css')
    <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-toggle/bootstrap-toggle.min.css')}}">

    <style>
        .change-account-password{
            display: none;
        }
    </style>
@endsection

@section('content')
    <form method="POST">
        {{csrf_field()}}
        <div class="card">
            <div class="card-header">
                Meu Perfil
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h5>Dados gerais</h5>
                        <hr>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name">Nome</label>
                            <input type="text" class="form-control" name="name" value="{{$user->name}}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-at"></i>
                                </div>
                                <input type="email" class="form-control" name="email" value="{{$user->email}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="email">Data de Nascimento</label>
                            <span class="input-group calendar">
                                <div class="input-group-addon">
                                    <span class="input-group-text">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control date" name="birth_date" value="{{$user->birth_date}}">
                            </span>
                        </div>
                    </div>
                </div>
                    {{--ALTERAR SENHA--}}
                <div class="row">
                    <div class="col-md-4 mt-3 mb-3">
                        <input type="checkbox" class="change-password-toggle" checked data-toggle="toggle" data-on="Manter senha" data-off="Alterar senha">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 change-password change-account-password">
                        <div class="form-group">
                            <label for="password">Nova senha</label>
                            <input type="password" class="form-control change-password-input" name="password">
                        </div>
                    </div>
                    <div class="col-md-4 change-password change-account-password">
                        <div class="form-group">
                            <label for="password_confirmation">Confirme a nova senha</label>
                            <input type="password" class="form-control change-password-input" name="password_confirmation">
                        </div>
                    </div>
                </div>

            </div>
            <div class="card-footer">
                <div class="crud-button">
                    <a href="{{route('get_dashboard')}}" class="btn btn-outline-danger"><i class="fa fa-reply"></i> Voltar</a>
                    <button type="submit" class="btn btn_default_color pull-right">
                        <i class="fa fa-check"></i>
                        Salvar alterações
                    </button>
                </div>
            </div>
        </div>
    </form>
@stop

@section('extras_js')
    <script src="{{asset('assets/plugins/jquery-mask/jquery.mask.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/cep/cep.js') }}"></script>
    <script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js')}}"></script>
    <script src="{{ asset('assets/js/datepickers.js') }}"></script>
    <script src="{{asset('assets/plugins/bootstrap-toggle/bootstrap-toggle.min.js')}}"></script>

    <script>
        $(".calendar").datepicker([]);

        $(document).ready(function () {
            $('.date').mask('00/00/0000');
        });

        let passwordDiv = $(".change-password"),
            passwordInput = $(".change-password-input");


        $(document).on('change', '.toggle', function () {
            let changePassword = $(".change-password-toggle").is(":checked");

            if (changePassword){
                passwordDiv.addClass('change-account-password');
                passwordInput.attr('disabled', true);
            }
            else{
                passwordDiv.removeClass('change-account-password');
                passwordInput.attr('disabled', false);
            }
        });
    </script>

@endsection