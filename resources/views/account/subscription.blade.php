@extends('layout', [
    'page_title' => 'Dados da Empresa',
])

@section('extras_css')
    <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-toggle/bootstrap-toggle.min.css')}}">
@endsection

@section('content')
    <style>

        li{
            list-style-type: none;
            text-align: left;
        }

        .plan-block{
            padding: 10px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            border: 1px solid #78866B;
            background-color: #78866B;
            margin-bottom: 15px;
            line-height: 32px;
        }

        .plan-block.current{
            background-color: #eee !important;
        }

        .plan-block.current .plan-header{
            font-size: 20px;
            color: #2f5a38;
        }

        .plan-block p{
            margin-bottom: 0;
        }

        .plan-block.current .plan-name{
            /*color: #FFFFFF;*/
        }

        .plan-name {
            font-family: "Bai Jamjuree", sans-serif;
            font-weight: 700;
            font-size: 2.5rem;
        }

        .plan-name-selected {
            font-family: "Bai Jamjuree", sans-serif;
            font-weight: 700;
            font-size: 5.5rem;
            padding-top: 2rem;
            padding-bottom: 2rem;
        }

        .plan-users {
            font-family: Quicksand;
            line-height: 2rem;
            font-size: 1.2rem;
            font-weight: 500;
            margin-top: 1.5rem;
        }

        .plan-users-selected {
            font-family: Quicksand, "sans-serif";
            line-height: 2rem;
            font-size: 1.8rem;
            font-weight: 500;
            margin-top: 1.5rem;
        }

        .plan-price {
            font-size: 1.2rem;
            font-weight: 300;
            color: #151b1e;
            margin-top: 1.8rem;
        }

        .plan-price-selected {
            font-size: 3.7rem;
            font-weight: 300;
            color: #151b1e;
            margin-top: 2.8rem;
        }

        .subscription-btn a {
            text-align: center;
            margin-top: .5rem;
            margin-bottom: .5rem;
        }

        .col-md-3 {
            padding-left: 5px;
            padding-right: 5px;
        }

        .plan-features {
            text-align: left;
            margin-top: 1.8rem;
        }

        .plan-features-selected {
            text-align: left;
            margin-top: 3.5rem;
            margin-left: 7.5rem;
        }

        .plan-features-list{
            position: relative;
            left: -5px;
            color: #fff;
            font-size: 1em;
            font-family: Quicksand;
            font-weight: 700;
        }

        .plan-features-list-selected{
            position: relative;
            left: -5px;
            color: #fff;
            font-size: 1.5em;
            font-family: Quicksand;
            font-weight: 700;
        }

        .plan-features-list-selected::before{
            font-family: FontAwesome;
            content: "\f00c";
            color: #2a5030;
            margin-right: 8px;
            width: 20px;
            margin-left: -20px;
        }

    </style>



    <form method="POST">
        {{csrf_field()}}
        <div class="card">
            <div class="card-header">
                Assinaturas
            </div>
            <div class="card-body">
                <ul class="nav nav-tabs" id="subscription-nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link {{request()->get('tab', 'subscription-data') == 'subscription-data' ? 'active' : null }}" id="nav-subscription-data-tab" data-toggle="tab" role="tab" href="#nav-subscription-data">Planos</a>
                    </li>
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link {{request()->get('tab') == 'purchase-data' ? 'active' : null}}" id="nav-purchase-data" data-toggle="tab" role="tab" href="#nav-purchase-data">Histórico</a>--}}
{{--                    </li>--}}
                </ul>

                <div class="tab-content" id="subscription-nav-content">
                    <div class="tab-pane fade {{request()->get('tab', 'subscription-data') == 'subscription-data' ? 'show active' : null }}" id="nav-subscription-data" role="tabpanel">
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">

                                <h5 class="default_color_dark text-center">Plano atual</h5>

                                <div class="plan-block current">
                                    <div class="plan-details text-center">
                                        <p class="plan-name">
                                            {{$current_subscription->plan->name}}
                                        </p>
                                        <div>
                                            <img src="{{asset('assets/img/golden_star_subscription.png')}}" style="max-width: 25%" alt="">
                                        </div>
                                        <p class="plan-users">
                                            Até {{$current_subscription->getMaxUsers()}} usuário
                                        </p>
                                        <p class="plan-price" style="color: #000;">
                                            R$ {{moneyFormat($current_subscription->price)}}/mês
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <hr>
                        @if($pending_subscription)
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    
                                    <h5 class="default_color_dark text-center">Sua solicitação de mudança de plano</h5>


                                        <div class="plan-block pending">
                                            <div class="plan-details text-center">
                                                <p class="plan-name-selected">
                                                    {{$pending_subscription->plan->name}}
                                                </p>

                                                <hr style="padding-top: 1rem; border-top: 10px solid rgba(0, 0, 0, 0.1);">

                                                <p class="plan-users-selected">
                                                    Até <strong>{{$pending_subscription->getMaxUsers()}}</strong> usuários
                                                </p>

                                                @switch($pending_subscription->plan->id)
                                                    @case(4)
                                                    {!!str_repeat("<img src=" . asset('assets/img/golden_star_subscription.png') . " style='max-width: 12%' alt=''>", $pending_subscription->plan->getMaxUsers())!!}
                                                    @break
                                                    @case(5)
                                                    {!!str_repeat("<img src=" . asset('assets/img/golden_star_subscription.png') . " style='max-width: 12%' alt=''>", $pending_subscription->plan->getMaxUsers())!!}
                                                    @break
                                                    @case(6)
                                                    {!!str_repeat("<img src=" . asset('assets/img/golden_star_subscription.png') . " style='max-width: 12%' alt=''>", $pending_subscription->plan->getMaxUsers())!!}
                                                    @break
                                                    @case(7)
                                                    {!!str_repeat("<img src=" . asset('assets/img/golden_star_subscription.png') . " style='max-width: 12%' alt=''>", $pending_subscription->plan->getMaxUsers())!!}
                                                    @break
                                                @endswitch

                                                <p class="plan-price-selected">
                                                    <span class="subsciption-price-selected">R$ {{moneyFormat($pending_subscription->price)}}/mês</span>
                                                </p>

                                                <div class="plan-features-selected">
                                                    <ul>
                                                        <li><span class="plan-features-list-selected">Todas as funcionalidades</span></li>
                                                        <li><span class="plan-features-list-selected">Suporte 24h</span></li>
                                                    </ul>
                                                </div>

                                                <div class="subscription-btn text-center">
                                                    <span>
                                                        <a href="{{route('get_account_subscription_pay', ['subscription' => $pending_subscription->id])}}" class="btn btn_default_color btn-pay btn-subscription-price-selected" style="margin-right: 10px;"><i class="fa fa-money"></i> Pagar</a>
                                                    </span>
                                                    <div>
                                                        <a href="{{route('get_account_subscription_cancel', $pending_subscription->id)}}" class="text-white"><i class="fa fa-times"></i> Cancelar</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                </div>
                                <div class="col-md-2"></div>
                            </div>
                        @else
                            <div class="row">
                                <div class="col-md-12">
                                    <h5 class="default_color_dark">Planos disponíveis</h5>
                                </div>
                                @foreach($plans as $plan)
                                    @if($plan->isPaid() && $plan->id != $current_subscription->plan->id)
                                        <div class="col-md-3">
                                            <div class="plan-block">
                                                <div class="plan-details text-center">
                                                    <p class="plan-name mt-3 mb-3">
                                                        {{$plan->name}}
                                                    </p>

                                                    <hr style="border-top: 10px solid rgba(0, 0, 0, 0.1);">

                                                    <p class="plan-users">
                                                        Até <strong>{{$plan->getMaxUsers()}}</strong> usuários
                                                    </p>
                                                    <div>
                                                        @switch($plan->id)
                                                            @case(4)
                                                            {!!str_repeat("<img src=" . asset('assets/img/golden_star_subscription.png') . " style='max-width: 12%' alt=''>", $plan->getMaxUsers())!!}
                                                            @break
                                                            @case(5)
                                                            {!!str_repeat("<img src=" . asset('assets/img/golden_star_subscription.png') . " style='max-width: 12%' alt=''>", $plan->getMaxUsers())!!}
                                                            @break
                                                            @case(6)
                                                            {!!str_repeat("<img src=" . asset('assets/img/golden_star_subscription.png') . " style='max-width: 12%' alt=''>", $plan->getMaxUsers())!!}
                                                            @break
                                                            @case(7)
                                                            {!!str_repeat("<img src=" . asset('assets/img/golden_star_subscription.png') . " style='max-width: 12%' alt=''>", $plan->getMaxUsers())!!}
                                                            @break
                                                        @endswitch
                                                    </div>

                                                    <p class="plan-price">
                                                        <span class="subsciption-price">R$ {{moneyFormat($plan->price)}}</span>/mês
                                                    </p>

                                                   <div class="plan-features">
                                                        <ul>
                                                            <li><span class="plan-features-list">Todas as funcionalidades</span></li>
                                                            <li><span class="plan-features-list">Suporte 24h</span></li>
                                                        </ul>
                                                    </div>

                                                    <button class="btn btn-lg btn_default_color mt-3 btn-subscription-price" name="plan_id" value="{{$plan->id}}">Contratar</button>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="tab-pane fade {{request()->get('tab') == 'purchase-data' ? 'show active' : null}}" id="nav-purchase-data" role="tabpanel">
                    {{--<h1>histórico</h1>--}}
                    </div>
                </div>
            </div>
        </div>
    </form>
@stop

{{--@section('modals')--}}
{{--    --}}{{--Account completion modal--}}
{{--    <div class="modal fade fadeIn" tabindex="-1" role="dialog" id="account_completion_modal" >--}}
{{--        <div class="modal-dialog modal-lg" role="document">--}}
{{--            <form method="POST" action="{{route('post_account_subscription_account_completion_modal')}}">--}}
{{--                {{csrf_field()}}--}}
{{--                <div class="modal-content" style="border-radius: 5px">--}}
{{--                    <div class="modal-header">--}}
{{--                        <h5 class="modal-title" style="font-size: .75rem">Por favor, complete seu cadastro para prosseguir com o pagamento</h5>--}}
{{--                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                            <span aria-hidden="true">&times;</span>--}}
{{--                        </button>--}}
{{--                    </div>--}}
{{--                    <div class="modal-body">--}}

{{--                    </div>--}}
{{--                    <div class="modal-footer">--}}
{{--                        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Finalizar e prosseguir</button>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </form>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--@stop--}}

@section('extras_js')
    <script src="{{asset('assets/plugins/jquery-mask/jquery.mask.min.js')}}"></script>
    <script src="{{asset('assets/plugins/cep/cep.js') }}"></script>

{{--    @if(isset($pending_subscription))--}}
{{--        <script>--}}
{{--            jQuery(document).ready(function($){--}}


{{--                $(document).on('click', '.btn-pay', function(e){--}}
{{--                    // Stop events e propagation--}}
{{--                    e.preventDefault();--}}
{{--                    e.stopImmediatePropagation();--}}
{{--                    let--}}
{{--                        button = $(this),--}}
{{--                        method = button.data('method'),--}}
{{--                        original_html = button.html();--}}
{{--                    // Trocar HTML do botão--}}
{{--                    button.html('<i class="fa fa-spinner fa-spin"></i> Carregando...');--}}
{{--                    isAccountReadyForPayment(method, function(isReady){--}}
{{--                        if(isReady){--}}
{{--                            goToPayment(method);--}}
{{--                        }else{--}}
{{--                            openAccountCompletionModal(method);--}}
{{--                        }--}}
{{--                        // Restore no html do botão--}}
{{--                        button.html(original_html);--}}
{{--                    });--}}
{{--                });--}}
{{--            });--}}


{{--            //Bind ao executar submit no complete modal--}}
{{--            $(document).on('submit', '#account_completion_modal form', function(){--}}
{{--                // Puxar o form--}}
{{--                let form = $(this);--}}
{{--                // Enviar request serializando o form--}}
{{--                $.post('{{route('post_account_subscription_account_completion_modal')}}', form.serialize(), function(response){--}}
{{--                    // Se a repsosta for sucesso--}}
{{--                    if(response.success){--}}
{{--                        // Fechar o modal--}}
{{--                        $('#account_completion_modal').modal('hide');--}}
{{--                        // Enviar para o pagamento--}}
{{--                        goToPayment(response.data.method);--}}
{{--                    }--}}
{{--                });--}}
{{--                // Apenas abandonis--}}
{{--                return false;--}}
{{--            });--}}


{{--            /**--}}
{{--             *  Function que determina qual é a ação para se tomar dado o método de pagamento--}}
{{--             */--}}
{{--            function goToPayment(method){--}}
{{--                switch(method) {--}}
{{--                    // Se for pagamento com boleto--}}
{{--                    case "{{\App\Models\Subscription::SUBSCRIPTION_PAYMENT_METHOD_BANKING_BILLET}}":--}}
{{--                        // Abrir a modal--}}
{{--                        openBankingBilletModal();--}}
{{--                        break;--}}
{{--                    case "{{\App\Models\Subscription::SUBSCRIPTION_PAYMENT_METHOD_CREDIT_CARD}}":--}}
{{--                        redirectToCreditCardPayment();--}}
{{--                        break;--}}
{{--                }--}}
{{--                return false;--}}
{{--            }--}}

{{--            /**--}}
{{--            * Funtion que gera a URL de pagamento--}}
{{--            */--}}
{{--            function getPaymentUrl(method){--}}
{{--                let paymentUrl = "{{route('get_account_subscription_pay', ['subscription' => $pending_subscription->id, 'method' => '__PAYMENT_METHOD__'])}}";--}}
{{--                return paymentUrl.replace('__PAYMENT_METHOD__', method);--}}
{{--            }--}}

{{--            /**--}}
{{--             *  Function para copiar o barcode--}}
{{--             */--}}
{{--            function copyBarcode() {--}}
{{--                $("#copybarcode").on("click", function () {--}}
{{--                    $("#bankingBilletBarcode").val().execCommand('copy');--}}
{{--                });--}}
{{--            }--}}

{{--            /**--}}
{{--             *  Function para inicializar os bindings do modal de boletos--}}
{{--             */--}}
{{--             function initBankingBilletModalBindings(){--}}
{{--                 // Ao clicar no Print do modal de boleto--}}
{{--                $(document).on('click', '#btnPrint', function(e){--}}
{{--                    // Stop events e propagation--}}
{{--                    e.preventDefault();--}}
{{--                    e.stopImmediatePropagation();--}}
{{--                    // Print--}}
{{--                    $('#banking-billet-modal-iframe').get(0).contentWindow.print();--}}
{{--                    // Sholes--}}
{{--                    return false;--}}
{{--                });--}}

{{--                // Ao fechar o modal de boleto--}}
{{--                $(document).on('hidden.bs.modal', '#banking-billet-modal', function(){--}}
{{--                    // Descarregar o iframe--}}
{{--                    $('#banking-billet-modal-iframe').attr('src', '');--}}
{{--                });--}}
{{--            }--}}

{{--            /**--}}
{{--             * Abrir a modal de pagamento com boleto--}}
{{--             */--}}
{{--            function openBankingBilletModal(){--}}
{{--                $('#banking-billet-modal-iframe').attr('src', getPaymentUrl("{{\App\Models\Subscription::SUBSCRIPTION_PAYMENT_METHOD_BANKING_BILLET}}"));--}}
{{--                $('#banking-billet-modal').modal('show');--}}
{{--            }--}}

{{--            /**--}}
{{--             * Function para redirecionar para o pagamento com CC--}}
{{--             */--}}
{{--            function redirectToCreditCardPayment(){--}}
{{--                window.location.href = getPaymentUrl("{{\App\Models\Subscription::SUBSCRIPTION_PAYMENT_METHOD_CREDIT_CARD}}");--}}
{{--            }--}}

{{--            /**--}}
{{--             * Function que checa se a account tá ready para pagamentos--}}
{{--             * @param method--}}
{{--             * @param callback--}}
{{--             */--}}
{{--            function isAccountReadyForPayment(method, callback){--}}
{{--                // Disparar GET request--}}
{{--                $.get("{{route('get_account_subscription_verify_account_completion')}}", {--}}
{{--                    // Usando o método informado--}}
{{--                    method:method--}}
{{--                // Na reponse--}}
{{--                }, function(response){--}}
{{--                    // Chamar o callback passsando o success da verificação--}}
{{--                    callback(response.success);--}}
{{--                });--}}
{{--            }--}}

{{--            /**--}}
{{--             * Function que abre a modal de completar cadastro--}}
{{--             * @param method--}}
{{--             */--}}
{{--            function openAccountCompletionModal(method){--}}
{{--                // Puxar o modal--}}
{{--                let account_completion_modal = $('#account_completion_modal');--}}
{{--                // Enviar request para obter o HTML da modal--}}
{{--                $.get("{{route('get_account_subscription_account_completion_modal')}}", {--}}
{{--                    // No método especificado--}}
{{--                    method: method--}}
{{--                // Ao receber a response--}}
{{--                }, function(response){--}}
{{--                    // Se tiver o data e tiver o html dentro do data--}}
{{--                    if(response.data && response.data.html){--}}
{{--                        // Preencher o modal--}}
{{--                        account_completion_modal.find('.modal-body').html(response.data.html);--}}
{{--                        // Carregar o CEP só pq sim--}}
{{--                        bindCepFields();--}}
{{--                        // Abrir o modal--}}
{{--                        account_completion_modal.modal({--}}
{{--                            keyboard: false,--}}
{{--                            backdrop: 'static',--}}
{{--                        });--}}
{{--                        $('.date').mask('00/00/0000');--}}
{{--                        $('.cpf').mask('000.000.000-00\', {reverse: true});');--}}
{{--                    // Se não der certo--}}
{{--                    }else{--}}
{{--                        // Swal básico--}}
{{--                        Swal.fire('', 'Erro interno ao concluir solicitação.', 'error');--}}
{{--                    }--}}
{{--                });--}}
{{--            }--}}
{{--        </script>--}}
{{--    @endif--}}
@stop