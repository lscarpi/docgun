@extends('admin.layout', [
    'page_title' => 'Admin Dashboard'
])

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <img class="img-responsive float-left mr-2" style="max-width: 90px" src="{{asset('assets/img/brasao.jpeg')}}" alt="">
                    <h3 class="mt-4">Olá, {{auth()->user()->name}}!</h3>
                    <p class="mb-0">Bem vindo ao Painel Admin docGun!</p>
                </div>
            </div>
            <div class="row" style="margin-top: 50px; min-height: 250px;">
                {{-- Gráfico de empresas nos últimos 12 meses --}}
                <div class="col-md-12">
                    <h5>Total de empresas nos útimos 12 meses</h5>
                    <div id="companies-chart"></div>
                </div>
            </div>
            <div class="row mt-4">

                <div class="col-md-12">
                    <hr />
                    <h6>
                        Total de empresas: {{$totalCompanies}} |
                        Total de planos gratis: {{$freeSubscriptions}} |
                        Total de planos pagos: {{$paidSubscriptions}}
                    </h6>
                    <hr />
                </div>

                {{--  Últimas 05 empresas cadastradas --}}
                <div class="col-md-4">
                    <h5>Últimas empresas cadastradas</h5>
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>Nome</th>
                            <th>Data</th>
                        </tr>
                        @if(count($latestCompanies))
                            @foreach($latestCompanies as $compamy)
                                <tr>
                                    <td>
                                        <a href="nope.html">
                                            {{$compamy->name}}
                                        </a>
                                    </td>
                                    <td>
                                        {{$compamy->created_at->formatLocalized('%d/%m/%Y')}}
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="2">Nenhuma empresa encontrada.</td>
                            </tr>
                        @endif
                    </table>
                </div>
                {{--  Teste grátis encerrando nos próximos 5 dias --}}
                <div class="col-md-4">
                    <h5>Teste grátis encerrando nos próximos 5 dias</h5>
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>Nome</th>
                            <th>Data</th>
                        </tr>
                        @if(count($trialExpiringSubscriptions))
                            @foreach($trialExpiringSubscriptions as $subscription)
                                <tr>
                                    <td>
                                        <a href="nope.html">
                                            {{$subscription->company->name}}
                                        </a>
                                    </td>
                                    <td>
                                        {{$subscription->terminate_at->formatLocalized('%d/%m/%Y')}}
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="2">Nenhuma assinatura encontrada.</td>
                            </tr>
                        @endif
                    </table>
                </div>
                {{--  Subscriptions pagas mais recentes --}}
                <div class="col-md-4">
                    <h5>Últimas assinaturas pagas</h5>
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>Nome</th>
                            <th>Data</th>
                            <th>Status</th>
                        </tr>
                        @if(count($newestPaidSubscriptions))
                            @foreach($newestPaidSubscriptions as $subscription)
                                <tr>
                                    <td>
                                        <a href="nope.html">
                                            {{$subscription->company->name}}
                                        </a>
                                    </td>
                                    <td>
                                        {{$subscription->terminate_at->formatLocalized('%d/%m/%Y')}}
                                    </td>
                                    <td>
                                        {{$subscription->status}}
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="3">Nenhuma assinatura encontrada.</td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop


@section('extras_js')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

        google.charts.load('current', {'packages':['bar']});
        google.charts.setOnLoadCallback(drawChart);


        function drawChart() {

            // Set chart options
            let options = {
                legend: {
                    position: 'none',
                },
                hAxis: {
                    title: '',
                },
                series: {
                    0: {color: '#2F5B38'},
                }
            };

            // ===================== Customers chart
            let array_data = JSON.parse('{!! json_encode($companiesDatatable) !!}');
            array_data.unshift(['Mês', 'Total']);
            let data = new google.visualization.arrayToDataTable(array_data, false);

            // Instantiate and draw our chart, passing in some options.
            let chart = new google.charts.Bar(document.getElementById('companies-chart'));
            chart.draw(data, google.charts.Bar.convertOptions(options));

        }
    </script>
@stop