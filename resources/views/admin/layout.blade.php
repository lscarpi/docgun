@extends('base', [
    'page_title' => isset($page_title) ? $page_title : null,
    'body_class' => 'dotted app header-fixed sidebar-fixed sidebar-compact'
])
@section('page')
    <header class="app-header navbar">
        <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button">☰</button>
        <a class="navbar-brand text-center" href="{{route('get_dashboard')}}">
            <img class="logomarca" src="{{asset('assets/img/docgun-logo.png')}}" alt="">
        </a>

        <ul class="nav navbar-nav ml-auto" style="margin-right: 50px;">

            <li class="nav-item dropdown" >
                <a class="nav-link nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    {{--<img class="img-avatar" src="img/avatars/6.jpg">--}}
                    Olá, {{auth()->user()->firstName()}}
                    <i class="fa fa-caret-down"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="{{route('get_dashboard')}}">
                        <i class="fa fa-user"></i> Painel do Usuário
                    </a>
                    <a class="dropdown-item" href="{{route('get_logout')}}">
                        <i class="fa fa-sign-out"></i> Sair
                    </a>
                </div>
            </li>
        </ul>

    </header>
    <div class="app-body">
    @include('admin.partials._sidebar')

    <!-- Main content -->
        <main class="main">

            <div class="container-fluid">
                <div class="animated fadeIn">

                    {{--Mensagens em flash--}}
                    @include('partials._flash')

                    {{--Mensagens em erro de forms--}}
                    @include('partials._errors')

                    {{--Conteúdo da página--}}
                    @yield('content')
                </div>

            </div>
            <!-- /.conainer-fluid -->
        </main>
    </div>
    <footer class="app-footer">
        <a href="https://monazite.com.br" target="_blank"><span style="color: #d2a147">mona</span><span style="color: #0a0a0a">zite</span></a> © {{\Carbon\Carbon::now()->year}} docGun -
    </footer>
@stop