<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">

            <li class="nav-item">
                <a class="nav-link" href="{{route('admin.getDashboard')}}" >
                    <i class="fa fa-home"></i> Início
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('admin.getSubscriptions')}}" >
                    <i class="fa fa-users"></i> Assinaturas
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="nope.html" >
                    <i class="fa fa-building"></i> Empresas
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="nope.html" >
                    <i class="fa fa-envelope"></i> Cartões de Ajuda
                </a>
            </li>

            <button class="navbar-toggler sidebar-minimizer text-center d-md-down-none" id="sidebarToggler" type="button">☰</button>



            {{--<button class="navbar-toggler sidebar-minimizer d-md-down-none" type="button"><i class="fa fa-fw fa-angle-left"></i></button>--}}

            {{--<li class="nav-item nav-dropdown">--}}
            {{--<a class="nav-link nav-dropdown-toggle" href="#"><i class="icon-star"></i> Cotações</a>--}}
            {{--<ul class="nav-dropdown-items">--}}
            {{--<li class="nav-item">--}}
            {{--<a class="nav-link" href="index.html"><i class="icon-speedometer"></i> Em aberto</a>--}}
            {{--</li>--}}
            {{--<li class="nav-item">--}}
            {{--<a class="nav-link" href="index.html"><i class="icon-speedometer"></i> Listar Todas</a>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{--</li>--}}
        </ul>
    </nav>
</div>