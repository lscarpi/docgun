@extends('admin.layout', [
    'page_title' => 'Editar assinatura'
])

@section('content')
    <div class="row mt-3">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Editar assinatura
                </div>
                <div class="card-body">
                    <form action="{{route('admin.patchSubscriptionsUpdate', $subscription->id)}}" method="POST">
                        @method('PATCH')
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="">Empresa</label>
                                    <input type="text" class="form-control" disabled="disabled" value="{{$subscription->company->name}}">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="">Plano</label>
                                    <input type="text" class="form-control" disabled="disabled" value="{{$subscription->plan->name}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <label for="{{\App\Models\Plan::RESOURCE_MAX_USERS}}">Máx de Usuários</label>
                                <input type="number" min="1" class="form-control" name="{{\App\Models\Plan::RESOURCE_MAX_USERS}}" id="{{\App\Models\Plan::RESOURCE_MAX_USERS}}" value="{{$subscription->getMaxUsers()}}" required>
                            </div>
                            <div class="col-6">
                                <label for="status">Status</label>
                                <select name="status" id="status" class="form-control" required>
                                    @foreach(\App\Models\Subscription::SUBSCRIPTION_STATUSES as $status)
                                        <option value="{{$status}}" {{$subscription->status == $status ? 'selected' : null }}>
                                            {{\App\Models\Subscription::getStatusLabel($status)}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-12">
                                <button type="submit" class="btn btn_default_color pull-right"><i class="fa fa-check"></i> Alterar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop