@extends('admin.layout', [
    'page_title' => 'Assinaturas'
])


@section('content')
    <div class="row mt-3">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Assinaturas
                </div>
                <div class="card-body">
                    @if($subscriptions->count())
                        <form method="GET">
                            <div class="form-group input-group">
                                <div class="input-group-prepend">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Buscar</button>
                                </div>
                                <input type="text" class="form-control search-list" placeholder="nome ou email" name="busca_texto" value="{{request()->get('busca_texto')}}" aria-label="Username" aria-describedby="basic-addon1">
                                <a href="{{route('admin.getSubscriptions')}}" class="btn btn-secondary"><i class="fa fa-times"></i></a>
                            </div>
                        </form>
                        <hr />
                        <div class="table table-responsive">
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <th>Empresa</th>
                                    <th>Plano</th>
                                    <th>Criado em</th>
                                    <th>Status</th>
                                    <th>Ações</th>
                                </tr>
                                @foreach($subscriptions as $subscription)
                                    <tr>
                                        <td>{{$subscription->company->name}}</td>
                                        <td>
                                            {{$subscription->plan->name}} - {{$subscription->getMaxUsers()}} Usuários
                                            @if($subscription->isTrial())
                                                <br />
                                                <small class="text-primary">Expira em {{\App\Library\DateHelper::toDateTimeString($subscription->terminate_at)}}</small>
                                            @endif
                                        </td>
                                        <td>{{\App\Library\DateHelper::toDateTimeString($subscription->created_at)}}</td>
                                        <td style="color: {{$subscription->statusColor()}}">{{$subscription->statusLabel()}}</td>
                                        <td>
                                            <a href="{{route('admin.getSubscriptionsEdit', $subscription->id)}}" class="btn btn-default" data-toggle="tooltip" title="Editar assinatura">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        {{$subscriptions->appends(request()->except('page'))->links()}}
                    @else
                        @include('partials._no_resources_found', [
                            'not_found_message' => 'Nenhum cliente encontrado',
                        ])

                        {{--Checa se houve busca ou não para determinar se o botão voltar aparece--}}
                        @if(isSearchBeingMade())
                            <a href="{{route('get_customers')}}" class="btn btn-outline-danger pull-right btn-no-mr"><i class="fa fa-reply"></i> Voltar</a>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop