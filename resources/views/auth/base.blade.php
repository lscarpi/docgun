<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" conte  nt="">
    <meta name="author" content="">
    <title>docGun - {{$page_title}}</title>
    <!-- Bootstrap core CSS-->
    <link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="{{asset('assets/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <!-- Custom CSS -->
    <link href="{{asset('assets/css/login.css')}}" rel="stylesheet">
    <!-- Fonte do Google -->
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500" rel="stylesheet">

    @include('partials._colors')

</head>

<body class="">

<div id="cloud-container" class="cloud-inicio visible-lg visible-md"></div>


<div class="col-md-6">
    @yield('content')
</div>
<footer class="footer center-block">
    <p class="text-center">
    <ul class="rodape">
        <li><a href="https://monazite.com.br" target="_blank"> Monazite </a></li>
        {{--<li><a href="https://monazite.com.br/blog>"> Blog</a></li>--}}
        <li><a href="https://docgun.com.br/#planos" target="_blank"> Planos e Preços </a></li>
        {{--<li><a href="https://help.doctudo.com"> Ajuda </a></li>--}}
        <li><a href="https://docgun.com.br/termos-de-uso" target="_blank"> Termos de Uso </a></li>
    </ul>
    </p>
</footer>

<!-- Bootstrap core JavaScript-->
<script src="{{ asset('assets/plugins/jquery/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- Core plugin JavaScript-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<script src="{{ asset('assets/plugins/jquery-mask/jquery.mask.min.js') }}"></script>
@yield('masks_js')
</body>
</html>