@extends('auth.base', [
    'page_title' => 'Entrar'
])

@section('content')
    <style>
        /*
            Background para melhorar o btn
         */
        .btn-outline-default{
            background-color: #FFF;
        }
    </style>
    <div class="row form-inicio flex-column" id="form-inicio">
        <div class="flex-form">
            <div class="text-center" style="margin-top: 10%;">
                <img class="img-responsive" style="margin: 20px 0;" src="{{asset('assets/img/docgun-logo-small.png')}}" alt="docgun-logo">
            </div>
            <p class="text-center text-inicio">A pasta inteligente do Despachante de Armas</p>
            <form method="POST">
                @include('partials._flash')
                {{csrf_field()}}
                <container>
                    <div>
                        <div class="form-group">
                            <div class="input-group margin-bottom-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-envelope-o fa-fw"></i></span>
                                </div>
                                <input type="email" name="email" class="form-control" id="formEmail" placeholder="Email" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group margin-bottom-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-key fa-fw"></i></span>
                                </div>
                                <input type="password" name="password" class="form-control" id="formPassword" placeholder="Senha" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="keep_connected" type="checkbox"> Permanecer conectado
                                </label>
                            </div>
                        </div>
                        <button type="submit" class="btn btn_default_color btn-block" style="color: #FFFFFF !important;">
                            Entrar
                        </button>
                        <a class="btn btn-outline-default btn-block" href="{{route('get_register')}}">
                            <img class="img-responsive float-left" style="max-width: 60px" src="{{asset('assets/img/weapon01.png')}}" alt=""> Criar conta grátis <img class="img-responsive float-right" style="max-width: 60px" src="{{asset('assets/img/weapon01-left.png')}}" alt="">
                        </a>

                        <div class="text-center mt-3">
                            <a href="{{route('get_recover_password')}}" style="text-decoration: none !important;">
                                <span id="recoverPassword">Esqueci minha senha</span>
                            </a>
                        </div>

                    </div>
                </container>
            </form>
            {{--<div id="forgotAccess">--}}
                {{--<p class="text-center">--}}
                    {{--<a href="#">Esqueceu os dados de acesso?</a>--}}
                {{--</p>--}}
            {{--</div>--}}
        </div>
    </div>
@stop