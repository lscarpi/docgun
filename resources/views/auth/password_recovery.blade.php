@extends('auth.base', [
    'page_title' => 'Recuperar Senha'
])

@section('content')

    <div class="row form-inicio flex-column" id="form-inicio">
        <div class="flex-form">
            <div class="text-center" style="margin-top: 10%;">
                <a href="{{route('get_login')}}"><img class="img-responsive" style="margin: 20px 0;" src="{{asset('assets/img/docgun-logo-small.png')}}" alt="docgun-logo"></a>
            </div>
            <p class="text-center text-inicio">Esqueceu a sua senha? Não tem problema</p>
            <form method="POST">
                @include('partials._flash')
                @include('partials._errors')
                {{csrf_field()}}
                <conteiner>
                    <div>

                        <div class="form-group">
                            <div class="input-group margin-bottom-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-envelope-o fa-fw"></i></span>
                                </div>
                                <input type="email" name="email" class="form-control" id="formEmail" placeholder="Email" value="{{request()->old('email')}}" required="required">
                            </div>
                        </div>

                        <button type="submit" class="btn btn_default_color btn-block" style="color: #FFFFFF !important;">
                            <i class="fa fa-sign-in"></i> Recuperar Senha
                        </button>

                    </div>
                </conteiner>
            </form>
        </div>
    </div>
@stop

@section('masks_js')
    <script>
        var maskBrTel = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            spOptions = {
                onKeyPress: function(val, e, field, options) {
                    field.mask(maskBrTel.apply({}, arguments), options);
                }
            };

        $('.campo-telefone').mask(maskBrTel, spOptions);
    </script>
@stop