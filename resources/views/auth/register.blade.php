@extends('auth.base', [
    'page_title' => 'Cadastro'
])

@section('content')

    <div class="row form-inicio flex-column" id="form-inicio">
        <div class="flex-form">
            <div class="text-center" style="margin-top: 10%;">
                <a href="{{route('get_login')}}"><img class="img-responsive" style="margin: 20px 0;" src="{{asset('assets/img/docgun-logo-small.png')}}" alt="docgun-logo"></a>
            </div>
            <p class="text-center text-inicio">Comece agora seu teste gratuito por 17 dias</p>
            <form method="POST">
                @include('partials._flash')
                @include('partials._errors')
                {{csrf_field()}}
                <conteiner>
                    <div>
                        <div class="form-group">
                            <div class="input-group margin-bottom-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-user fa-fw"></i></span>
                                </div>
                                <input type="text" name="name" class="form-control" id="formName" placeholder="Nome" value="{{request()->old('name')}}" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group margin-bottom-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-envelope-o fa-fw"></i></span>
                                </div>
                                <input type="email" name="email" class="form-control" id="formEmail" placeholder="Email" value="{{request()->old('email')}}" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group margin-bottom-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-key fa-fw"></i></span>
                                </div>
                                <input type="password" name="password" class="form-control" id="formPassword" placeholder="Senha" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group margin-bottom-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-key fa-fw"></i></span>
                                </div>
                                <input type="password" name="password_confirmation" class="form-control" id="formConfirmPassword" placeholder="Confirme a senha" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group margin-bottom-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-whatsapp fa-fw"></i></span>
                                </div>
                                <input type="tel" name="whatsapp" class="form-control campo-telefone" id="formCellphone" placeholder="WhatsApp" minlength="11" value="{{request()->old('whatsapp')}}" required="required">
                            </div>
                        </div>
                        {{--<div class="form-group">--}}
                            {{--<div class="input-group margin-bottom-sm">--}}
                                {{--<span class="input-group-addon"><i class="fa fa-building fa-fw"></i></span>--}}
                                {{--<input type="text" name="company_name" class="form-control" id="formCompanyName" placeholder="Nome da empresa (opcional)" required="required">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="input-group margin-bottom-sm">--}}
                                {{--<span class="input-group-addon"><i class="fa fa-users fa-fw"></i></span>--}}
                                {{--<input type="number" min="1" name="company_size" class="form-control" id="formCompanySize" placeholder="Número de funcionários (opcional)" required="required">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <button type="submit" class="btn btn_default_color btn-block" style="color: #FFFFFF !important;">
                            <i class="fa fa-sign-in"></i> Criar Conta
                        </button>
                    </div>
                </conteiner>
            </form>
        </div>
    </div>
@stop

@section('masks_js')
    <script>
        var maskBrTel = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            spOptions = {
                onKeyPress: function(val, e, field, options) {
                    field.mask(maskBrTel.apply({}, arguments), options);
                }
            };

        $('.campo-telefone').mask(maskBrTel, spOptions);
    </script>
@stop