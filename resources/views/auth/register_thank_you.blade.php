@extends('auth.base', [
    'page_title' => 'Quase lá!'
])

@section('content')
    <div class="row form-inicio flex-column" id="form-inicio">
        <div class="flex-form" style="width: 100%;">
            <div class="card" style="border-color: #fafafa; box-shadow: -webkit-box-shadow: -webkit-box-shadow: 9px 11px 26px -14px rgba(0,0,0,0.35);
-moz-box-shadow: 9px 11px 26px -14px rgba(0,0,0,0.35);
box-shadow: 9px 11px 26px -14px rgba(0,0,0,0.35); margin-top: 10%;">
                <div class="text-center card-img-top" style="">
                    <img class="img-responsive" style="margin: 20px 0;" src="{{asset('assets/img/docgun-logo-small.png')}}" alt="docgun-logo">
                </div>
                <div class="card-body" style="width: 100%; font-family: Quicksand, sans-serif; background-color: #fafafa;">
                    <h3 class="text-center card-title">{{$first_name}}, falta pouco!</h3>
                    <p class="text-center card-text">
                        <br />
                        Enviamos um email para <strong>{{$email}}</strong> com as instruções
                        para ativar sua conta e iniciar seu período de testes.<br />
                    </p>
                    <br />
                    <p class="card-text">
                        Bom trabalho,<br/>
                        Equipe docGun.
                    </p>
                </div>
            </div>
        </div>
    </div>
@stop