<!doctype html>
<html lang="pt-BR">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-135041517-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-135041517-1');
    </script>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>
        {{isset($page_title) ? $page_title . ' - ': null}} docGun
    </title>

    {{--<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}">--}}
    {{--<link rel="stylesheet" href="{{asset('assets/plugins/tether/css/tether.min.css')}}">--}}
    {{--<link rel="stylesheet" href="{{asset('assets/plugins/font-awesome/css/font-awesome.min.css')}}">--}}
    <link rel="stylesheet" href="{{asset('assets/plugins/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/plugins/simple-line-icons/css/simple-line-icons.css')}}">
    <link rel="stylesheet" href="{{asset('assets/plugins/sweetalert/sweetalert.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/plugins/select2/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/plugins/datepicker/datepicker.min.css')}}">

    <!-- Main styles for this application -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/checkbox.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Bai+Jamjuree:300,400,500,700" rel="stylesheet">

    @include('partials._colors')

    @yield('extras_css')

</head>
<body class="{{$body_class ?? 'dotted'}}">

    @yield('page')
    @yield('modals')

    <script>
        var csrf_token = "{{csrf_token()}}";
    </script>

    <script src="{{asset('assets/plugins/jquery/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('assets/plugins/popperjs/js/popper.min.js')}}"></script>
    <script src="{{asset('assets/plugins/tether/js/tether.min.js')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/plugins/pace/js/pace.min.js')}}"></script>
    <script src="{{asset('assets/plugins/chartjs/js/chart.min.js')}}"></script>
    <script src="{{asset('assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/plugins/select2/select2.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datepicker/datepicker.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datepicker/pt-br.js')}}"></script>
    <script src="{{asset('assets/js/app.js')}}"></script>
    @yield('layout_js')
    @yield('extras_js')
</body>
</html>