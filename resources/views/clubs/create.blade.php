@extends('layout', [
    'page_title' => 'Cadastrar Clube'
])

@section('content')
    <form method="POST">
        {!! csrf_field() !!}
        <div class="card">
            <div class="card-header">
                Cadastrar Clube de Tiro
            </div>
            <div class="card-body">
                <ul class="nav nav-tabs" id="club-nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link {{request()->get('tab', 'club-data') == 'club-data' ? 'active' : null }}" id="nav-club-data-tab" data-toggle="tab" role="tab" href="#nav-club-data">Dados do Clube</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{request()->get('tab') == 'club-address' ? 'active' : null}}" id="nav-club-address-tab" data-toggle="tab" role="tab" href="#nav-club-address">Endereço do Clube</a>
                    </li>
                </ul>
                <div class="tab-content" id="club-nav-content">
                    <div class="tab-pane fade {{request()->get('tab', 'club-data') == 'club-data' ? 'show active' : null }}" id="nav-club-data" role="tabpanel">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Nome fantasia</label>
                                    <input class="form-control" type="text" name="name" value="{{request()->old('name')}}" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Nome do contato</label>
                                    <input class="form-control" type="text" name="person_in_charge" value="{{request()->old('person_in_charge')}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Telefone</label>
                                    <span class="input-group">
                                        <div class="input-group-addon">
                                            <span class="input-group-text">
                                                <i class="fa fa-phone"></i>
                                            </span>
                                        </div>
                                        <input class="form-control phone" type="text" name="phone" value="{{request()->old('phone')}}">
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <span class="input-group">
                                        <div class="input-group-addon">
                                            <span class="input-group-text">
                                                <i class="fa fa-at"></i>
                                            </span>
                                        </div>
                                        <input class="form-control" type="email" name="email" value="{{request()->old('email')}}">
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade {{request()->get('tab') == 'club-address' ? 'show active' : null }}" id="nav-club-address" role="tabpanel">
                        <div class="address">
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>CEP</label>
                                        <input class="form-control cep" type="text" name="address_zip" value="{{request()->old('address_zip')}}" id="commercial_cep">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Endereço</label>
                                        <input class="form-control address_1" type="text" name="address_1" value="{{request()->old('address_1')}}" id="address_1">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Complemento</label>
                                        <input class="form-control address_2" type="text" name="address_2" value="{{request()->old('address_2')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>Número</label>
                                        <input class="form-control number" type="text" name="address_number" value="{{request()->old('address_number')}}">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Bairro</label>
                                        <input class="form-control neighborhood" type="text" name="address_neighborhood" value="{{request()->old('address_neighborhood')}}" id="address_neighborhood">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Cidade</label>
                                        <input class="form-control city" type="text" name="address_city" value="{{request()->old('address_city')}}" id="address_city">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>UF</label>
                                        <input class="form-control state" type="text" name="address_state" value="{{request()->old('address_state')}}" id="address_state">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="crud-button">
                    <a href="{{route('get_clubs')}}" class="btn btn-outline-danger"><i class="fa fa-reply"></i> Voltar</a>
                    <button type="submit" class="btn btn_default_color pull-right">
                        <i class="fa fa-check"></i>
                        Salvar
                    </button>
                </div>
            </div>
        </div>
    </form>
@stop

@section('extras_js')
    <script src="{{asset('assets/plugins/jquery-mask/jquery.mask.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/cep/cep.js') }}"></script>

    <script>
        let PhoneMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
        onKeyPress: function(val, e, field, options) {
        field.mask(PhoneMaskBehavior.apply({}, arguments), options);
        }
        };

        $('.phone').mask(PhoneMaskBehavior, spOptions);
    </script>
@stop