@extends('layout', [
    'page_title' => 'Editar clube: ' . $club->name,
])

@section('content')
    <form method="POST">
        {!! csrf_field() !!}
        <div class="card">
            <div class="card-header">
                Editar Clube de Tiro
            </div>
            <div class="card-body">
                <ul class="nav nav-tabs" id="club-nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link {{request()->get('tab', 'club-data') == 'club-data' ? 'active' : null }}" id="nav-club-data-tab" data-toggle="tab" role="tab" href="#nav-club-data">Dados do Clube</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{request()->get('tab') == 'club-address' ? 'active' : null}}" id="nav-club-address-tab" data-toggle="tab" role="tab" href="#nav-club-address">Endereço do Clube</a>
                    </li>
                </ul>
                <div class="tab-content" id="club-nav-content">
                    <div class="tab-pane fade {{request()->get('tab', 'club-data') == 'club-data' ? 'show active' : null }}" id="nav-club-data" role="tabpanel">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Nome fantasia</label>
                                    <input class="form-control" type="text" name="name" placeholder="Nome fantasia do clube de tiro" value="{{$club->name}}" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Nome do contato</label>
                                    <input class="form-control" type="text" name="person_in_charge" placeholder="Contato no clube de tiro" value="{{$club->person_in_charge}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Telefone</label>
                                    <span class="input-group">
                                        <div class="input-group-addon">
                                            <span class="input-group-text">
                                                <i class="fa fa-phone"></i>
                                            </span>
                                        </div>
                                        <input class="form-control phone" type="text" name="phone" placeholder="Telefone" value="{{$club->phone}}">
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-at"></i>
                                        </div>
                                        <input class="form-control" type="email" name="email" placeholder="E-mail do contato" value="{{$club->email}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade {{request()->get('tab') == 'club-address' ? 'show active' : null }}" id="nav-club-address" role="tabpanel">
                        <div class="address">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>CEP</label>
                                        <input class="form-control cep" type="text" name="address_zip" placeholder="CEP" value="{{$club->address->zip}}" id="commercial_cep">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Endereço</label>
                                        <input class="form-control address_1" type="text" name="address_1" placeholder="Endereço do cliente" value="{{$club->address->address_1}}" id="address_1">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Complemento</label>
                                        <input class="form-control address_2" type="text" name="address_2" placeholder="Complemento do endereço" value="{{$club->address->address_2}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Número</label>
                                        <input class="form-control number" type="text" name="address_number" placeholder="Número" value="{{$club->address->number}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Bairro</label>
                                        <input class="form-control neighborhood" type="text" name="address_neighborhood" placeholder="Bairro" value="{{$club->address->neighborhood}}" id="address_neighborhood">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Cidade</label>
                                        <input class="form-control city" type="text" name="address_city" placeholder="Cidade" value="{{$club->address->city}}" id="address_city">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>UF</label>
                                        <input class="form-control state" type="text" name="address_state" placeholder="UF" value="{{$club->address->state}}" id="address_state">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="crud-button">
                    <a href="{{route('get_clubs')}}" class="btn btn-outline-danger"><i class="fa fa-reply"></i> Voltar</a>
                    <button type="submit" class="btn btn_default_color pull-right">
                        <i class="fa fa-check"></i>
                        Salvar
                    </button>
                </div>
            </div>
        </div>
    </form>
@stop

@section('extras_js')
    <script src="{{asset('assets/plugins/jquery-mask/jquery.mask.min.js')}}"></script>
    <script src="{{asset('assets/plugins/cep/cep.js')}}"></script>

    <script>
        let PhoneMaskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            spOptions = {
                onKeyPress: function(val, e, field, options) {
                    field.mask(PhoneMaskBehavior.apply({}, arguments), options);
                }
            };

        $('.phone').mask(PhoneMaskBehavior, spOptions);
    </script>
@stop