@extends('layout', [
    'page_title' => 'Clubes',
])

@section('content')
    <div class="row mt-3">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Clubes de Tiro
                    <a href="{{route('get_clubs_create')}}" class="btn btn-outline-light btn-add pull-right">
                        <i class="fa fa-plus"></i> Clube
                    </a>
                </div>
                <div class="card-body">
                    @if($clubs->count())
                        <form method="GET">
                            <div class="form-group input-group">
                                <div class="input-group-prepend">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Buscar</button>
                                </div>
                                <input type="text" class="form-control search-list" placeholder="nome ou contato" name="busca_texto" value="{{request()->get('busca_texto')}}" aria-label="Username" aria-describedby="basic-addon1">
                                <a href="{{route('get_clubs')}}" class="btn btn-secondary"><i class="fa fa-times"></i></a>
                            </div>


                            {{--<div class="form-group">--}}
                            {{--<label for="">Busca</label>--}}
                            {{--<input type="text" placeholder="Pesquise pelo nome ou email" name="busca_texto" value="{{request()->get('busca_texto')}}" class="form-control">--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                            {{--<button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Buscar</button>--}}
                            {{--<a href="{{route('get_customers')}}" class="btn btn-secondary"><i class="fa fa-times"></i> Limpar Busca</a>--}}
                            {{--</div>--}}
                        </form>
                        <hr>
                        <div class="table table-responsive">
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <th>Nome</th>
                                    <th>Email</th>
                                    <th>Criado em</th>
                                    <th>Ações</th>
                                </tr>
                                @foreach($clubs as $club)
                                    <tr>
                                        <td>
                                            <a href="{{route('get_clubs_edit', $club->id)}}">{{$club->name}}</a>
                                        </td>
                                        <td>
                                            <a href="mailto:{{$club->email}}">{{$club->email}}</a>
                                        </td>
                                        <td>
                                            {{$club->created_at->formatLocalized('%d/%m/%Y')}}
                                        </td>
                                        <td>
                                            <a href="{{route('get_clubs_edit', $club->id)}}" class="btn btn-light" data-toggle="tooltip" title="Editar">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            <a href="{{route('get_clubs_delete', $club->id)}}" class="btn btn-light" data-delete-confirm="true" data-toggle="tooltip" title="Excluir">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        {{$clubs->appends(request()->except('page'))->links()}}
                    @else
                        @include('partials._no_resources_found', [
                            'not_found_message' => 'Nenhum clube encontrado',
                        ])

                        {{--Checa se houve busca ou não para determinar se o botão voltar aparece--}}
                        @if(isSearchBeingMade())
                            <a href="{{route('get_clubs')}}" class="btn btn-outline-danger btn-no-mr pull-right"><i class="fa fa-reply"></i> Voltar</a>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop