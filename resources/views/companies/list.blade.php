@extends('layout', [
    'page_title' => 'Empresas'
])


@section('content')
    {{--<div class="row mt-3">--}}
        {{--<div class="col-12">--}}
            {{--<a href="{{route('get_companies_create')}}" class="btn btn-success pull-right">--}}
                {{--<i class="fa fa-plus"></i> Casdastrar Empresa--}}
            {{--</a>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="row mt-3">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Empresas
                </div>
                <div class="card-body">
                    @if($companies->count())
                        <form method="GET">
                            <div class="form-group input-group">
                                <div class="input-group-prepend">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Buscar</button>
                                </div>
                                <input type="text" class="form-control search-list" placeholder="nome da empresa" name="busca_texto" value="{{request()->get('busca_texto')}}" aria-label="Username" aria-describedby="basic-addon1">
                                <a href="{{route('get_companies')}}" class="btn btn-secondary"><i class="fa fa-times"></i></a>

                            </div>


                            {{--<div class="form-group">--}}
                            {{--<label for="">Busca</label>--}}
                            {{--<input type="text" placeholder="Pesquise pelo nome ou email" name="busca_texto" value="{{request()->get('busca_texto')}}" class="form-control">--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                            {{--<button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Buscar</button>--}}
                            {{--<a href="{{route('get_customers')}}" class="btn btn-secondary"><i class="fa fa-times"></i> Limpar Busca</a>--}}
                            {{--</div>--}}
                        </form>
                        <hr />
                        <div class="table table-responsive">
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <th>Nome</th>
                                    {{--<th>Email</th>--}}
                                    <th>Criado em</th>
                                    <th>Ações</th>
                                </tr>
                                @foreach($companies as $company)
                                    <tr>
                                        <td>
                                            {{$company->name}}
                                        </td>
                                        {{--<td>--}}
                                        {{--<a href="mailto:{{$user->email}}">{{$user->email}}</a>--}}
                                        {{--</td>--}}
                                        <td>
                                            {{$company->created_at->formatLocalized('%d/%m/%Y')}}
                                        </td>
                                        <td>
                                            {{--<a href="{{route('get_users_edit', $user->id)}}" class="btn btn-light" data-toggle="tooltip" title="Editar">--}}
                                            {{--<i class="fa fa-pencil"></i>--}}
                                            {{--</a>--}}


                                            @if($company->id != auth()->user()->company_id)
                                                <a href="{{route('get_companies_login_as', $company->id)}}"  class="btn btn-light" data-toggle="tooltip" title="Logar como">
                                                    <i class="fa fa-sign-in"></i>
                                                </a>
                                                <a href="{{route('get_companies_delete', $company->id)}}" data-delete-confirm="true" class="btn btn-light" data-toggle="tooltip" title="Excluir">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        {{$companies->appends(request()->except('page'))->links()}}
                    @else
                        <div class="alert alert-warning">
                            Nenhuma empresa encontrada.
                        </div>

                        {{--Checa se houve busca ou não para determinar se o botão voltar aparece--}}
                        @if(isSearchBeingMade())
                            <a href="{{route('get_companies')}}" class="btn btn-outline-danger btn-no-mr pull-right"><i class="fa fa-reply"></i> Voltar</a>
                        @endif
                    @endif
                </div>
            </div>

        </div>
    </div>
@stop