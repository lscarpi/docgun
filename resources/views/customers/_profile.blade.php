<div class="text-right">
    <span >
        <a href="" class="btn btn-light btn_view_customer_data_details" data-toggle="tooltip" title="Ver detalhes">
            <i class="fa fa-search"></i>
        </a>
    </span>
    <span>
        <a href="{{ URL::route('get_customers_customer_data_details_pdf', $customer->id)}}" class="btn btn-light btn_view_customer_data_details-pdf" data-toggle="tooltip" title="Baixar PDF">
            <i class="fa fa-file-pdf-o"></i>
        </a>
    </span>
</div>



<h5>Dados pessoais</h5>
<hr>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label>Nome completo</label>
            <input class="form-control" type="text" name="name" value="{{$customer->name}}" disabled="disabled">
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <label>Email</label>
            <input class="form-control" type="email" name="email" value="{{$customer->email}}" disabled="disabled">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label for="civil_status">Estado Civil</label>
            <input type="text" name="civil_status" class="form-control" value="{{$customer->getCivilStatus()}}" disabled="disabled">
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="gender">Sexo</label>
            <input type="text" name="gender" class="form-control" value="{{$customer->getGender()}}" disabled="disabled">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg">
        <div class="form-group">
            <label>Data de nascimento</label>
            <input class="form-control init-datepicker" type="text" name="birth_date" value="{{$customer->birth_date ? defaultDateFormat($customer->birth_date) : null}}" disabled="disabled">
        </div>
    </div>
    <div class="col-lg">
        <div class="form-group">
            <label for="nacionality">Nacionalidade</label>
            <input type="text" name="nacionality" class="form-control" value="{{$customer->nacionality}}" disabled="disabled">
        </div>
    </div>
    <div class="col-lg">
        <div class="form-group">
            <label>Natural de</label>
            <input class="form-control" type="text" name="naturality"  value="{{$customer->naturality}}" disabled="disabled">
        </div>
    </div>
    <div class="col-lg">
        <div class="form-group">
            <label>Profissão</label>
            <input class="form-control" type="text" name="profession" value="{{$customer->profession}}" disabled="disabled">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg">
        <div class="form-group">
            <label>Nome do pai</label>
            <input class="form-control" type="text" name="fathers_name" value="{{$customer->fathers_name}}" disabled="disabled">
        </div>
    </div>
    <div class="col-lg">
        <div class="form-group">
            <label>Nome da mãe</label>
            <input class="form-control" type="text" name="mothers_name" value="{{$customer->mothers_name}}" disabled="disabled">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg">
        <div class="form-group">
            <label>RG</label>
            <input class="form-control" type="text" name="registration_number" value="{{$customer->registration_number}}" disabled="disabled">
        </div>
    </div>
    <div class="col-lg">
        <div class="form-group">
            <label>RG - Órgão Emissor</label>
            <input class="form-control" type="text" name="registration_number_expeditor" value="{{$customer->registration_number_expeditor}}" disabled="disabled">
        </div>
    </div>
    <div class="col-lg">
        <div class="form-group">
            <label>RG - Data de expedição</label>
            <input class="form-control init-datepicker" type="text" name="registration_number_expedition_date" value="{{$customer->registration_number_expedition_date ? defaultDateFormat($customer->registration_number_expedition_date) : null}}" disabled="disabled">
        </div>
    </div>
    <div class="col-lg">
        <div class="form-group">
            <label>CPF</label>
            <input class="form-control" type="text" name="cpf_cnpj" value="{{$customer->cpf_cnpj}}" disabled="disabled">
        </div>
    </div>
    <div class="col-lg">
        <div class="form-group">
            <label>Título de Eleitor</label>
            <input class="form-control" type="text" name="voter_id" value="{{$customer->voter_id}}" disabled="disabled">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label>Telefone Residencial</label>
            <input class="form-control" type="text" name="residential_phone" value="{{$customer->residential_phone}}" disabled="disabled">
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label>Telefone Comercial</label>
            <input class="form-control" type="text" name="comercial_phone" value="{{$customer->comercial_phone}}" disabled="disabled">
        </div>
    </div>
</div>

<h5 class="mt-5">Endereço Residencial</h5>
<hr />

<div class="address">
    <div class="row">
        <div class="col-lg-2">
            <div class="form-group">
                <label>CEP</label>
                <input class="form-control cep" type="text" name="residential_address_zip" value="{{$customer->residential_address->zip ?? null}}" id="residential_cep" disabled="disabled">
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label>Logradouro</label>
                <input class="form-control address_1" type="text" name="residential_address_1" value="{{$customer->residential_address->address_1 ?? null}}" id="residential_address_1" disabled="disabled">
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label>Complemento</label>
                <input class="form-control address_2" type="text" name="residential_address_2" value="{{$customer->residential_address->address_2 ?? null}}" disabled="disabled">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2">
            <div class="form-group">
                <label>Número</label>
                <input class="form-control number" type="text" name="residential_address_number" value="{{$customer->residential_address->number ?? null}}" disabled="disabled">
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label>Bairro</label>
                <input class="form-control neighborhood" type="text" name="residential_address_neighborhood" value="{{$customer->residential_address->neighborhood ?? null}}" id="residential_address_neighborhood" disabled="disabled">
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label>Cidade</label>
                <input class="form-control city" type="text" name="residential_address_city" value="{{$customer->residential_address->city ?? null}}" id="residential_address_city" disabled="disabled">
            </div>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <label>UF</label>
                <input class="form-control state" type="text" name="residential_address_state" value="{{$customer->residential_address->state ?? null}}" id="residential_address_state" disabled="disabled">
            </div>
        </div>
    </div>
</div>

<h5 class="mt-5">Endereço Comercial</h5>
<hr />

<div class="address">
    <div class="row">
        <div class="col-lg-2">
            <div class="form-group">
                <label>CEP</label>
                <input class="form-control cep" type="text" name="comercial_address_zip" value="{{$customer->comercial_address->zip ?? null}}" id="comercial_cep" disabled="disabled">
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label>Endereço</label>
                <input class="form-control address_1" type="text" name="comercial_address_1" value="{{$customer->comercial_address->address_1 ?? null}}" id="comercial_address_1" disabled="disabled">
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label>Complemento</label>
                <input class="form-control address_2" type="text" name="comercial_address_2" value="{{$customer->comercial_address->address_2 ?? null}}" disabled="disabled">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2">
            <div class="form-group">
                <label>Número</label>
                <input class="form-control number" type="text" name="comercial_address_number" value="{{$customer->comercial_address->number ?? null}}" disabled="disabled">
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label>Bairro</label>
                <input class="form-control neighborhood" type="text" name="comercial_address_neighborhood" value="{{$customer->comercial_address->neighborhood ?? null}}" id="comercial_address_neighborhood" disabled="disabled">
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label>Cidade</label>
                <input class="form-control city" type="text" name="comercial_address_city" value="{{$customer->comercial_address->city ?? null}}" id="comercial_address_city" disabled="disabled">
            </div>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <label>UF</label>
                <input class="form-control state" type="text" name="comercial_address_state" value="{{$customer->comercial_address->state ?? null}}" id="comercial_address_state" disabled="disabled">
            </div>
        </div>
    </div>
</div>

<h5 class="mt-5">Documentos do processo</h5>
<hr />

<div class="row">
    <div class="col-lg">
        <div class="form-group">
            <label>Clube de tiro afiliado</label>
            <input type="text" name="club_id" class="form-control" value="{{$customer->club->name ?? null}}" disabled="disabled">
        </div>
    </div>
    <div class="col-lg">
        <div class="form-group">
            <label>Categoria CR</label>
            <input type="text" name="cr_category" class="form-control" value="{{$customer->getCRCategory()}}" disabled="disabled">
        </div>
    </div>
    <div class="col-lg">
        <div class="form-group">
            <label>Número CR</label>
            <input class="form-control" type="text" name="cr_number" value="{{$customer->cr_number}}" disabled="disabled">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg">
        <div class="form-group">
            <label>Data de expedição CR</label>
            <input class="form-control" type="text" name="cr_expedition_date" value="{{$customer->cr_expedition_date ? defaultDateFormat($customer->cr_expedition_date) : null}}" disabled="disabled">
        </div>
    </div>
    <div class="col-lg">
        <div class="form-group">
            <label>Data de expiração CR</label>
            <input class="form-control" type="text" name="cr_expiration_date" value="{{$customer->cr_expiration_date ? defaultDateFormat($customer->cr_expiration_date) : null}}" disabled="disabled">
        </div>
    </div>
    <div class="col-lg">
        <div class="form-group">
            <label>Número RM</label>
            <input class="form-control" type="text" name="rm_number" value="{{$customer->rm_number}}" disabled="disabled">
        </div>
    </div>
</div>

<h5 class="mt-5">Outros Dados</h5>
<hr />

<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <label>Email Adicional</label>
            <input type="email" name="email2" class="form-control" value="{{$customer->email2 ?? null}}" disabled="disabled">
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <label>Parceiro Comercial</label>
            <input type="text" name="commercial_partner" class="form-control" value="{{$customer->commercial_partner ?? null}}" disabled="disabled">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg">
        <div class="form-group">
            <label>Observações</label>
            <input class="form-control" type="text" name="obs" value="{{$customer->obs ?? null}}" disabled="disabled">
        </div>
    </div>
</div>


{{--@section('extras.js')--}}
    {{--<script>--}}
       {{----}}
    {{--</script>--}}
{{--@endsection--}}