@extends('layout', [
    'page_title' =>  'Cadastrar Cliente',
])

@section('extras_css')
    <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}">
@stop

@section('content')
    <form method="POST">
        {!! csrf_field() !!}

        {{--||||||||----------TABS----------||||||||--}}
        <div class="card">
            <div class="card-header">
                Cadastrar Cliente
            </div>
            <div class="card-body">
                <ul class="nav nav-tabs" id="customer-nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link {{request()->get('tab', 'personal-data') == 'personal-data' ? 'active' : null }}" id="nav-personal-data-tab" data-toggle="tab" role="tab" href="#nav-personal-data">Dados Pessoais</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{request()->get('tab') == 'residential-address' ? 'active' : null}}" id="nav-residential-address-tab" data-toggle="tab" role="tab" href="#nav-residential-address">Endereço Residencial</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link  {{request()->get('tab') == 'commercial-address' ? 'active' : null}}" id="nav-commercial-address-tab" data-toggle="tab" role="tab" href="#nav-commercial-address">Endereço Comercial</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link  {{request()->get('tab') == 'weapon-documents' ? 'active' : null}}" id="nav-weapon-documents-tab" data-toggle="tab" role="tab" href="#nav-weapon-documents">Documentos do Processo</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link  {{request()->get('tab') == 'other-data' ? 'active' : null}}" id="nav-other-data-tab" data-toggle="tab" role="tab" href="#nav-other-data">Outros Dados</a>
                    </li>
                </ul>
                <div class="tab-content" id="customer-nav-content">
                    <div class="tab-pane fade {{request()->get('tab', 'personal-data') == 'personal-data' ? 'show active' : null }}" id="nav-personal-data" role="tabpanel">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Nome completo</label>
                                    <input class="form-control" type="text" name="name" value="{{request()->old('name')}}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-at"></i>
                                        </div>
                                        <input class="form-control" type="email" name="email" value="{{request()->old('email')}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="civil_status">Estado Civil</label>
                                    <select name="civil_status" class="form-control" id="civil_status">
                                        @foreach(\App\Models\Customer::CIVIL_STATUSES as $option_value => $option_label)
                                            <option value="{{$option_value}}">{{$option_label}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="gender">Sexo</label>
                                    <select name="gender" class="form-control" id="gender">
                                        @foreach(\App\Models\Customer::GENDERS as $option_value => $option_label)
                                            <option value="{{$option_value}}">{{$option_label}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg">
                                <label for="email">Data de Nascimento</label>
                                <span class="input-group">
                                    <div class="input-group-addon">
                                        <span class="input-group-text">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </div>
                                    <input class="form-control date" type="text" name="birth_date" value="{{request()->old('birth_date')}}">
                                </span>
                            </div>
                            <div class="col-lg">
                                <div class="form-group">
                                    <label for="nacionality">Nacionalidade</label>
                                    <select name="nacionality" class="form-control" id="nacionality">
                                        <option value="Brasileiro">Brasileiro</option>
                                        <option value="Outro">Outro</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>Natural de</label>
                                    <input class="form-control" type="text" name="naturality" value="{{request()->old('naturality')}}">
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>Profissão</label>
                                    <input class="form-control" type="text" name="profession" value="{{request()->old('profession')}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>Nome do pai</label>
                                    <input class="form-control" type="text" name="fathers_name" value="{{request()->old('fathers_name')}}">
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>Nome da mãe</label>
                                    <input class="form-control" type="text" name="mothers_name" value="{{request()->old('mothers_name')}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>RG</label>
                                    <input class="form-control" type="text" name="registration_number" value="{{request()->old('registration_number')}}">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>RG - Órgão Emissor</label>
                                    <input class="form-control" type="text" name="registration_number_expeditor" value="{{request()->old('registration_number_expeditor')}}">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>RG - Data Expedição</label>
                                    <span class="input-group">
                                        <div class="input-group-addon">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                        <input class="form-control date" type="text" name="registration_number_expedition_date" value="{{request()->old('registration_number_expedition_date')}}">
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>CPF</label>
                                    <input class="form-control" type="text" name="cpf_cnpj" value="{{request()->old('cpf_cnpj')}}">
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>Título de Eleitor</label>
                                    <input class="form-control" type="text" name="voter_id" value="{{request()->old('voter_id')}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade {{request()->get('tab') == 'residential-address' ? 'show active' : null}}" id="nav-residential-address" role="tabpanel">
                        <div class="address">
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>CEP</label>
                                        <input class="form-control cep" type="text" name="residential_address_zip" value="{{request()->old('residential_address_zip')}}" id="residential_cep">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Logradouro</label>
                                        <input class="form-control address_1" type="text" name="residential_address_1" value="{{request()->old('residential_address_1')}}" id="residential_address_1">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Complemento</label>
                                        <input class="form-control address_2" type="text" name="residential_address_2" value="{{request()->old('residential_address_2')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>Número</label>
                                        <input class="form-control number" type="text" name="residential_address_number" value="{{request()->old('residential_address_number')}}">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Bairro</label>
                                        <input class="form-control neighborhood" type="text" name="residential_address_neighborhood" value="{{request()->old('residential_address_neighborhood')}}" id="residential_address_neighborhood">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Cidade</label>
                                        <input class="form-control city" type="text" name="residential_address_city" value="{{request()->old('residential_address_city')}}" id="residential_address_city">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>UF</label>
                                        <input class="form-control state" type="text" name="residential_address_state" value="{{request()->old('residential_address_state')}}" id="residential_address_state">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Telefone Residencial</label>
                                        <span class="input-group">
                                            <div class="input-group-addon">
                                                <span class="input-group-text">
                                                    <i class="fa fa-phone"></i>
                                                </span>
                                            </div>
                                            <input class="form-control phone" type="text" name="residential_phone" value="{{request()->old('residential_phone')}}">
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade {{request()->get('tab') == 'commercial-address' ? 'show active' : null}}" id="nav-commercial-address" role="tabpanel">
                        <div class="address">
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>CEP</label>
                                        <input class="form-control cep" type="text" name="comercial_address_zip" value="{{request()->old('comercial_address_zip')}}" id="comercial_cep">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Endereço</label>
                                        <input class="form-control address_1" type="text" name="comercial_address_1" value="{{request()->old('comercial_address_1')}}" id="comercial_address_1">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Complemento</label>
                                        <input class="form-control address_2" type="text" name="comercial_address_2" value="{{request()->old('comercial_address_2')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>Número</label>
                                        <input class="form-control number" type="text" name="comercial_address_number" value="{{request()->old('comercial_address_number')}}">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Bairro</label>
                                        <input class="form-control neighborhood" type="text" name="comercial_address_neighborhood" value="{{request()->old('comercial_address_neighborhood')}}" id="comercial_address_neighborhood">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Cidade</label>
                                        <input class="form-control city" type="text" name="comercial_address_city" value="{{request()->old('comercial_address_city')}}" id="comercial_address_city">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>UF</label>
                                        <input class="form-control state" type="text" name="comercial_address_state" value="{{request()->old('comercial_address_state')}}" id="comercial_address_state">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Telefone Comercial</label>
                                        <span class="input-group">
                                            <div class="input-group-addon">
                                                <span class="input-group-text">
                                                    <i class="fa fa-phone"></i>
                                                </span>
                                            </div>
                                            <input class="form-control phone" type="text" name="comercial_phone" value="{{request()->old('comercial_phone')}}">
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade {{request()->get('tab') == 'weapon-documents' ? 'show active' : null}}" id="nav-weapon-documents" role="tabpanel">
                        <div class="row">
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>Clube de tiro afiliado</label>
                                    <select class="form-control" type="select" name="club_id" value="{{request()->old('club_id')}}">
                                        <option value="" selected>Nenhum</option>
                                        @foreach(\App\Models\Club::all() as $club)
                                            <option value="{{$club->id}}"
                                                    {{$club->id}}
                                            >{{$club->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>Categoria CR</label>
                                    <select name="cr_category" id="cr_category" class="form-control">
                                        <option value="">Não possui</option>
                                        @foreach(\App\Models\Customer::CR_CATEGORIES as $category_value => $category_name)
                                            <option value="{{$category_value}}"
                                                    {{$category_value}}
                                            >{{$category_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>Número CR</label>
                                    <input class="form-control" type="text" name="cr_number" value="{{request()->old('cr_number')}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>Data de expedição CR</label>
                                    <span class="input-group">
                                        <div class="input-group-addon">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                        <input class="form-control date" type="text" name="cr_expedition_date" value="{{request()->old('cr_expedition_date')}}">
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>Data de expiração CR</label>
                                    <span class="input-group">
                                        <div class="input-group-addon">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                        <input class="form-control date" type="text" name="cr_expiration_date" value="{{request()->old('cr_expiration_date')}}">
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>Número RM</label>
                                    <input class="form-control" type="text" name="rm_number" value="{{request()->old('rm_number')}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade {{request()->get('tab') == 'other-data' ? 'show active' : null}}" id="nav-other-data" role="tabpanel">
                        <div class="address">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Email Adicional</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-at"></i>
                                            </div>
                                            <input class="form-control" type="email" name="email2" value="{{request()->old('email2')}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Parceiro Comercial</label>
                                        <input class="form-control" type="text" name="commercial_partner" value="{{request()->old('commercial_partner')}}" id="commercial_partner">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Observações</label>
                                        <input class="form-control number" type="text" name="obs" value="{{request()->old('obs')}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="card-footer">
                    <div class="crud-button">
                        <a href="{{route('get_customers')}}" class="btn btn-outline-danger"><i class="fa fa-reply"></i> Voltar</a>
                        <button type="submit" class="btn btn_default_color pull-right">
                            <i class="fa fa-check"></i>
                            Salvar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@stop

@section('extras_js')
    <script src="{{asset('assets/plugins/jquery-mask/jquery.mask.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/cep/cep.js') }}"></script>
    <script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js')}}"></script>
    <script src="{{ asset('assets/js/datepickers.js') }}"></script>

    <script>
        $(document).ready(function () {
            $('.date').mask('00/00/0000');
        });

        let PhoneMaskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            spOptions = {
                onKeyPress: function(val, e, field, options) {
                    field.mask(PhoneMaskBehavior.apply({}, arguments), options);
                }
            };

        $('.phone').mask(PhoneMaskBehavior, spOptions);
    </script>
@stop