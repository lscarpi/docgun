@extends('base')

@section('page')
    <style>
        body.dotted {
            background: none;

        thead:before, thead:after,
        tbody:before, tbody:after,
        tfoot:before, tfoot:after
        {
            display: none;
        }
    </style>

    <div class="text-center mt-5">
        <h1 >Ficha Cadastral</h1>
        <h2>{{ $customer->name }}</h2>
    </div>

    <hr>

    <div class="">
{{--        <div class="row">--}}
{{--            <div class="col-6 ">--}}
                <div class="customer-data-view" style="float: left; width: 48%; font-size: 10px">
                    <h5>Dados do Cliente</h5>
                    <p class="customer-details-print">Nome completo: <span class="customer-details-data-print">{{ $customer->name }}</span> </p>
                    <p class="customer-details-print">Email: <span class="customer-details-data-print">{{ $customer->email }}</span></p>
                    <p class="customer-details-print">Estado Civil: <span class="customer-details-data-print">{{ $customer->getCivilStatus() }}</span></p>
                    <p class="customer-details-print">Sexo: <span class="customer-details-data-print">{{ $customer->getGender() }}</span></p>
                    <p class="customer-details-print">Data de nascimento: <span class="customer-details-data-print">{{ $customer->birth_date ? defaultDateFormat($customer->birth_date) : null }}</span></p>
                    <p class="customer-details-print">Nacionalidade: <span class="customer-details-data-print">{{ $customer->nacionality }}</span></p>
                    <p class="customer-details-print">Natural de: <span class="customer-details-data-print">{{ $customer->naturality }}</span></p>
                    <p class="customer-details-print">Profissão: <span class="customer-details-data-print">{{ $customer->profession }}</span></p>
                    <p class="customer-details-print">Nome do pai: <span class="customer-details-data-print">{{ $customer->fathers_name }}</span></p>
                    <p class="customer-details-print">Nome da mãe: <span class="customer-details-data-print">{{ $customer->mothers_name }}</span></p>

                    @if($customer->registration_number && $customer->registration_number_expeditor && $customer-> registration_number_expedition_date)
                        <p class="customer-details-print">RG: <span class="customer-details-data-print">{{ $customer->registration_number }}</span> - <span class="customer-details-data-print">{{ $customer->registration_number }}</span>. Expira em: <span class="customer-details-data-print">{{ $customer->registration_number_expedition_date ? defaultDateFormat($customer->registration_number_expedition_date) : null }}</span></p>
                    @endif

                    <p class="customer-details-print">RG: <span class="customer-details-data-print">{{ $customer->registration_number }}</span></p>
                    <p class="customer-details-print">RG - Órgão Emissor: <span class="customer-details-data-print">{{ $customer->registration_number_expeditor }}</span></p>
                    <p class="customer-details-print">RG - Data de expedição:: <span class="customer-details-data-print">{{ $customer->registration_number_expedition_date ? defaultDateFormat($customer->registration_number_expedition_date) : null }}</span></p>
                    <p class="customer-details-print">CPF: <span class="customer-details-data-print">{{ $customer->cpf_cnpj }}</span></p>
                    <p class="customer-details-print">Título de Eleitor: <span class="customer-details-data-print">{{ $customer->voter_id }}</span></p>
                    <p class="customer-details-print">Telefone Residencial: <span class="customer-details-data-print">{{ $customer->residential_phone }}</span></p>
                    <p class="customer-details-print">Telefone Comercial: <span class="customer-details-data-print">{{ $customer->comercial_phone }}</span></p>
                </div>
{{--            </div>--}}
{{--            <div class="col-6">--}}
                <div class="customer-data-view" style="float: left; width: 48%; font-size: 10px">
                    <h5>Endereço Residencial</h5>
                    <p class="customer-details-print">CEP: <span class="customer-details-data-print">{{ $customer->residential_address->zip ?? null }}</span></p>
                    <p class="customer-details-print">Logradouro: <span class="customer-details-data-print">{{ $customer->residential_address->address_1 ?? null }}</span></p>
                    <p class="customer-details-print">Complemento: <span class="customer-details-data-print">{{ $customer->residential_address->address_2 ?? null }}</span></p>
                    <p class="customer-details-print">Número: <span class="customer-details-data-print">{{ $customer->residential_address->number ?? null }}</span></p>
                    <p class="customer-details-print">Bairro: <span class="customer-details-data-print">{{ $customer->residential_address->neighborhood ?? null }}</span></p>
                    <p class="customer-details-print">Cidade: <span class="customer-details-data-print">{{ $customer->residential_address->city ?? null }}</span></p>
                    <p class="customer-details-print">UF: <span class="customer-details-data-print">{{ $customer->residential_address->state ?? null }}</span></p>
                </div>
                <div class="customer-data-view" style="font-size: 10px">
                    <h5>Endereço Comercial</h5>
                    <p class="customer-details-print">CEP: <span class="customer-details-data-print">{{ $customer->comercial_address->zip ?? null }}</span></p>
                    <p class="customer-details-print">Endereço: <span class="customer-details-data-print">{{ $customer->comercial_address->address_1 ?? null }}</span></p>
                    <p class="customer-details-print">Complemento: <span class="customer-details-data-print">{{ $customer->comercial_address->address_2 ?? null }}</span></p>
                    <p class="customer-details-print">Número: <span class="customer-details-data-print">{{ $customer->comercial_address->number ?? null }}</span></p>
                    <p class="customer-details-print">Bairro: <span class="customer-details-data-print">{{ $customer->comercial_address->neighborhood ?? null }}</span></p>
                    <p class="customer-details-print">Cidade: <span class="customer-details-data-print">{{ $customer->comercial_address->city ?? null }}</span></p>
                    <p class="customer-details-print">UF: <span class="customer-details-data-print">{{ $customer->comercial_address->state ?? null }}</span></p>
                </div>
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-6">--}}
                <div class="customer-data-view" style="float: left; font-size: 10px" >
                    <h5>Documentos do processo</h5>
                    <p class="customer-details-print">Clube de tiro afiliado: <span class="customer-details-data-print">{{ $customer->club->name ?? null }}</span></p>
                    <p class="customer-details-print">CR nº: <span class="customer-details-data-print">{{ $customer->cr_number }}</span></p>
                    <p class="customer-details-print">Categoria CR: <span class="customer-details-data-print">{{ $customer->getCRCategory() }}</span></p>
                    <p class="customer-details-print">Número CR: <span class="customer-details-data-print">{{ $customer->cr_number }}</span></p>
                    <p class="customer-details-print">Data de expedição CR: <span class="customer-details-data-print">{{ $customer->cr_expedition_date ? defaultDateFormat($customer->cr_expedition_date) : null  }}</span></p>
                    <p class="customer-details-print">Data de expiração CR: <span class="customer-details-data-print">{{ $customer->cr_expiration_date ? defaultDateFormat($customer->cr_expiration_date) : null  }}</span></p>
                    <p class="customer-details-print">Número RM: <span class="customer-details-data-print"> {{ $customer->rm_number  }}</span></p>
                </div>
{{--            </div>--}}
{{--        </div>--}}

    </div>

@endsection



<style>
    @media print{
        @page {
            margin: 0;
            /*width: 21cm !important;*/
        }

        body{
            margin: 1.6cm;
            /*width: 21cm !important;*/
        }
    }
</style>

<script>
    window.addEventListener('message', receiveMessage, false);

    function receiveMessage(event){
        if(event.data.method === 'print'){
            window.print();
        }
    }
</script>