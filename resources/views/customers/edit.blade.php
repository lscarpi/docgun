@extends('layout', [
    'page_title' => "Editar cliente: " . $customer->name,
])


@section('extras_css')
    <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}">
@stop

@section('content')
    <form method="POST">
        {!! csrf_field() !!}
        <div class="card">
            <div class="card-header">
                Editar Cliente
            </div>
            <div class="card-body">
                <ul class="nav nav-tabs" id="customer-nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link {{request()->get('tab', 'personal-data') == 'personal-data' ? 'active' : null }}" id="nav-personal-data-tab" data-toggle="tab" role="tab" href="#nav-personal-data">Dados Pessoais</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{request()->get('tab') == 'residential-address' ? 'active' : null}}" id="nav-residential-address-tab" data-toggle="tab" role="tab" href="#nav-residential-address">Endereço Residencial</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link  {{request()->get('tab') == 'commercial-address' ? 'active' : null}}" id="nav-commercial-address-tab" data-toggle="tab" role="tab" href="#nav-commercial-address">Endereço Comercial</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link  {{request()->get('tab') == 'weapon-documents' ? 'active' : null}}" id="nav-weapon-documents-tab" data-toggle="tab" role="tab" href="#nav-weapon-documents">Documentos do Processo</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link  {{request()->get('tab') == 'other-data' ? 'active' : null}}" id="nav-other-data-tab" data-toggle="tab" role="tab" href="#nav-other-data">Outros Dados</a>
                    </li>
                </ul>
                <div class="tab-content" id="customer-nav-content">
                    <div class="tab-pane fade {{request()->get('tab', 'personal-data') == 'personal-data' ? 'show active' : null }}" id="nav-personal-data" role="tabpanel">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Nome completo</label>
                                    <input class="form-control" type="text" name="name" value="{{$customer->name}}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-at"></i>
                                        </div>
                                        <input class="form-control" type="email" name="email" value="{{$customer->email}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="civil_status">Estado Civil</label>
                                    <select name="civil_status" class="form-control" id="civil_status">
                                        @foreach(\App\Models\Customer::CIVIL_STATUSES as $option_value => $option_label)
                                            <option value="{{$option_value}}"
                                                    {{$option_value == $customer->civil_status ? 'selected' : null}}
                                            >{{$option_label}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="gender">Sexo</label>
                                    <select name="gender" class="form-control" id="gender">
                                        @foreach(\App\Models\Customer::GENDERS as $option_value => $option_label)
                                            <option value="{{$option_value}}"
                                                    {{$option_value == $customer->gender ? 'selected' : null}}
                                            >{{$option_label}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>Data de nascimento</label>
                                    <span class="input-group">
                                        <div class="input-group-addon">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                        <input class="form-control" type="text" name="birth_date" value="{{$customer->birth_date ? defaultDateFormat($customer->birth_date) : null}}">
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="form-group">
                                    <label for="nacionality">Nacionalidade</label>
                                    <select name="nacionality" class="form-control" id="nacionality">
                                        <option value="Brasileiro">Brasileiro</option>
                                        <option value="Outro">Outro</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>Natural de</label>
                                    <input class="form-control" type="text" name="naturality" value="{{$customer->naturality}}">
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>Profissão</label>
                                    <input class="form-control" type="text" name="profession" value="{{$customer->profession}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>Nome do pai</label>
                                    <input class="form-control" type="text" name="fathers_name" value="{{$customer->fathers_name}}">
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>Nome da mãe</label>
                                    <input class="form-control" type="text" name="mothers_name" value="{{$customer->mothers_name}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>RG</label>
                                    <input class="form-control" type="text" name="registration_number" value="{{$customer->registration_number}}">
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>RG - Órgão Emissor</label>
                                    <input class="form-control" type="text" name="registration_number_expeditor" value="{{$customer->registration_number_expeditor}}">
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>RG - Data de expedição</label>
                                    <span class="input-group">
                                        <div class="input-group-addon">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                        <input class="form-control" type="text" name="registration_number_expedition_date" value="{{$customer->registration_number_expedition_date ? defaultDateFormat($customer->registration_number_expedition_date) : null}}">
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>CPF</label>
                                    <input class="form-control" type="text" name="cpf_cnpj" value="{{$customer->cpf_cnpj}}">
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>Título de Eleitor</label>
                                    <input class="form-control" type="text" name="voter_id" value="{{$customer->voter_id}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade {{request()->get('tab') == 'residential-address' ? 'show active' : null}}" id="nav-residential-address" role="tabpanel">
                        <div class="address">
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>CEP</label>
                                        <input class="form-control cep" type="text" name="residential_address_zip" value="{{$customer->residential_address->zip ?? null}}" id="residential_cep">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Logradouro</label>
                                        <input class="form-control address_1" type="text" name="residential_address_1" value="{{$customer->residential_address->address_1 ?? null}}" id="residential_address_1">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Complemento</label>
                                        <input class="form-control address_2" type="text" name="residential_address_2" value="{{$customer->residential_address->address_2 ?? null}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>Número</label>
                                        <input class="form-control number" type="text" name="residential_address_number" value="{{$customer->residential_address->number ?? null}}">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Bairro</label>
                                        <input class="form-control neighborhood" type="text" name="residential_address_neighborhood" value="{{$customer->residential_address->neighborhood ?? null}}" id="residential_address_neighborhood">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Cidade</label>
                                        <input class="form-control city" type="text" name="residential_address_city" value="{{$customer->residential_address->city ?? null}}" id="residential_address_city">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>UF</label>
                                        <input class="form-control state" type="text" name="residential_address_state" value="{{$customer->residential_address->state ?? null}}" id="residential_address_state">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Telefone Residencial</label>
                                        <span class="input-group">
                                            <div class="input-group-addon">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-phone"></i>
                                                    </span>
                                            </div>
                                            <input class="form-control phone" type="text" name="residential_phone" value="{{$customer->residential_phone}}">
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade {{request()->get('tab') == 'commercial-address' ? 'show active' : null}}" id="nav-commercial-address" role="tabpanel">
                        <div class="address">
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>CEP</label>
                                        <input class="form-control cep" type="text" name="comercial_address_zip" value="{{$customer->comercial_address->zip ?? null}}" id="comercial_cep">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Endereço</label>
                                        <input class="form-control address_1" type="text" name="comercial_address_1" value="{{$customer->comercial_address->address_1 ?? null}}" id="comercial_address_1">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Complemento</label>
                                        <input class="form-control address_2" type="text" name="comercial_address_2" value="{{$customer->comercial_address->address_2 ?? null}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>Número</label>
                                        <input class="form-control number" type="text" name="comercial_address_number" value="{{$customer->comercial_address->number ?? null}}">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Bairro</label>
                                        <input class="form-control neighborhood" type="text" name="comercial_address_neighborhood" value="{{$customer->comercial_address->neighborhood ?? null}}" id="comercial_address_neighborhood">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Cidade</label>
                                        <input class="form-control city" type="text" name="comercial_address_city" value="{{$customer->comercial_address->city ?? null}}" id="comercial_address_city">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>UF</label>
                                        <input class="form-control state" type="text" name="comercial_address_state" value="{{$customer->comercial_address->state ?? null}}" id="comercial_address_state">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Telefone Comercial</label>
                                        <span class="input-group">
                                            <div class="input-group-addon">
                                                <span class="input-group-text">
                                                    <i class="fa fa-phone"></i>
                                                </span>
                                            </div>
                                            <input class="form-control phone" type="text" name="comercial_phone" value="{{$customer->comercial_phone}}">
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade {{request()->get('tab') == 'weapon-documents' ? 'show active' : null}}" id="nav-weapon-documents" role="tabpanel">
                        <div class="row">
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>Clube de tiro afiliado</label>
                                    <select class="form-control" type="select" name="club_id" value="{{$customer->club_id}}">
                                        <option value="" selected>Nenhum</option>
                                        @foreach(\App\Models\Club::all() as $club)
                                            <option value="{{$club->id}}"
                                                    {{$club->id == $customer->club_id ? 'selected' : null}}
                                            >{{$club->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>Categoria CR</label>
                                    <select name="cr_category" id="cr_category" class="form-control">
                                        <option value="">Não possui</option>
                                        @foreach(\App\Models\Customer::CR_CATEGORIES as $category_value => $category_name)
                                            <option value="{{$category_value}}"
                                                    {{$category_value == $customer->cr_category ? 'selected' : null}}
                                            >{{$category_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>Número CR</label>
                                    <input class="form-control" type="text" name="cr_number" value="{{$customer->cr_number}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>Data de expedição CR</label>
                                    <span class="input-group">
                                        <div class="input-group-addon">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                        <input class="form-control" type="text" name="cr_expedition_date" value="{{$customer->cr_expedition_date ? defaultDateFormat($customer->cr_expedition_date) : null}}">
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>Data de expiração CR</label>
                                    <span class="input-group">
                                        <div class="input-group-addon">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                        <input class="form-control" type="text" name="cr_expiration_date" value="{{$customer->cr_expiration_date ? defaultDateFormat($customer->cr_expiration_date) : null}}">
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="form-group">
                                    <label>Número RM</label>
                                    <input class="form-control" type="text" name="rm_number" value="{{$customer->rm_number}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade {{request()->get('tab') == 'other-data' ? 'show active' : null}}" id="nav-other-data" role="tabpanel">
                        <div class="address">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Email Adicional</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-at"></i>
                                            </div>
                                            <input class="form-control" type="email" name="email2" value="{{$customer->email2}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Parceiro Comercial</label>
                                        <input class="form-control" type="text" name="commercial_partner" value="{{$customer->commercial_partner}}" id="commercial_partner">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Observações</label>
                                        <input class="form-control number" type="text" name="obs" value="{{$customer->obs}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="crud-button">
                    <a href="{{route('get_customers')}}" class="btn btn-outline-danger"><i class="fa fa-reply"></i> Voltar</a>
                    <button type="submit" class="btn btn_default_color pull-right">
                        <i class="fa fa-check"></i>
                        Salvar
                    </button>
                </div>
            </div>
        </div>
    </form>
@stop

@section('extras_js')
    <script src="{{asset('assets/plugins/jquery-mask/jquery.mask.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/cep/cep.js') }}"></script>
    <script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js')}}"></script>
    <script src="{{ asset('assets/js/datepickers.js') }}"></script>
@stop