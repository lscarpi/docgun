@extends('layout', [
    'page_title' => 'Novo documento de '. $customer->name,
])

@section('extras_css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@stop

@section('content')
    <div class="">
        <div class="card">
            <div class="card-header">
                Gerar documento para cliente
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <label class="label-sub-header" for="select_document_layout">Selecione um modelo para gerar</label>
                    <select name="select_document_layout" id="select_document_layout" class="form-control">
                        <option value=""></option>
                        @foreach($documents as $document)
                            <option value="{{$document->id}}">
                                {{$document->title}}
                                @if($document->isShared())
                                     - (compartilhado)
                                @endif
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <form method="POST" id="form_issue_new_document" style="width: 100%;">
            {!! csrf_field() !!}
            <input type="hidden" name="document_id">

            <div class="col-lg-12" style="display: none;" id="form_issue_new_document_content">

                <hr />

                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="label-sub-header" for="">Título do Documento</label>
                                    <input type="text" name="title" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="label-sub-header" for="">Conteúdo do Documento</label>
                                    <textarea id="document_content" name="content" cols="30" rows="20"></textarea>
                                </div>
                            </div>
                        </div>
                        @include('partials._masks')
                    </div>
                    <div class="card-footer">
                        <div class="crud-button">
                            <a href="{{route('get_customers_view', ['customer' => $customer->id, 'tab' => 'weapons'])}}" class="btn btn-outline-danger"><i class="fa fa-reply"></i> Voltar</a>
                            <button type="submit" class="btn btn_default_color pull-right">
                                <i class="fa fa-check"></i>
                                Salvar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop

@section('extras_js')

    @include('partials._tinymce_script')


    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script type="text/javascript">

        jQuery(document).ready(function($){
            let
                select_document_layout = $('#select_document_layout'),
                route_get_layout_details = '{{route('get_documents_view', '__LAYOUT_ID__')}}';

            select_document_layout.select2({
                placeholder: 'Escolha',
            }).on('select2:select', function(e){
                loadLayoutIntoForm(e.params.data.id);
            });



            let loadLayoutIntoForm = function(layout){
                $.get(
                    route_get_layout_details.replace('__LAYOUT_ID__', layout),
                    function(response){
                        // Se achou a response corretamente
                        if(response && response.content && response.title){
                            // Fill no hidden para enviar para o backend
                            $('[name=document_id]').val(layout);
                            // Rehabilitar disabled do submit
                            $('[type=submit]').removeAttr('disabled');
                            // Colocar o title no campo
                            $('[name=title]').val(response.title);
                            // Inserir conteúdo no TinyMCE
                            tinyMCE.get('document_content').setContent(response.content);
                            // Mostrar o espaço oculto
                            $('#form_issue_new_document_content').slideDown();
                        // Se não deu certo
                        }else{
                            // Alertar show
                            alert('Erro interno, por favor, tente novamente.');
                        }
                    }
                );
            }

        });
    </script>
@stop