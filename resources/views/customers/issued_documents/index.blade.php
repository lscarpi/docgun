<div class="row">
    <div class="col-12 mb-4">
        <a href="{{route('get_customers_issued_documents_create', ['customer' => $customer->id])}}" id="btn_issue_new_document" class="btn btn-success">
            <i class="fa fa-file"></i>
            Gerar novo documento
        </a>
    </div>
    <div class="col-12">
        @if($customer->issuedDocuments->count())
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <tr>
                        <th style="width: 35%">Título</th>
                        <th style="width: 15%">Data</th>
                        <th style="width: 35%">Original</th>
                        <th style="width: 15%">Ações</th>
                    </tr>
                    @foreach($customer->issuedDocuments as $issued_document)
                        <tr>
                            <td>{{$issued_document->title}}</td>
                            <td>{{ defaultDateTimeFormat($issued_document->created_at)}}</td>
                            <td>
                                <a href="{{route('get_documents_edit', $issued_document->document_id)}}">
                                    {{$issued_document->originalDocument->title}}
                                </a>
                            </td>
                            <td>
                                <a href="" data-issued_document_id="{{$issued_document->id}}" class="btn btn-light btn_view_issued_document_details" data-toggle="tooltip" title="Ver detalhes">
                                    <i class="fa fa-search"></i>
                                </a>

                                <a href="{{route('get_customers_issued_documents_delete', ['customer' => $customer->id, 'issued_document' => $issued_document->id])}}" data-delete-confirm="true" class="btn btn-light" data-toggle="tooltip" title="Excluir">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        @else
            <div class="alert alert-warning">
                Nenhum documento gerado para este cliente.
            </div>
        @endif
    </div>
</div>