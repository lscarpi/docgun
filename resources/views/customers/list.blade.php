@extends('layout')

@section('content')
    <div class="row mt-3">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Clientes
                    <a href="{{route('get_customers_create')}}" class="btn btn-outline-light btn-add pull-right">
                        <i class="fa fa-plus"></i> Cliente
                    </a>
                </div>
                <div class="card-body">
                    @if($customers->count())
                        <form method="GET">
                            <div class="form-group input-group">
                                <div class="input-group-prepend">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Buscar</button>
                                </div>
                                <input type="text" class="form-control search-list" placeholder="nome ou email" name="busca_texto" value="{{request()->get('busca_texto')}}" aria-label="Username" aria-describedby="basic-addon1">
                                <a href="{{route('get_customers')}}" class="btn btn-secondary"><i class="fa fa-times"></i></a>
                            </div>
                        </form>
                        <hr />
                        <div class="table table-responsive">
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <th>Nome</th>
                                    <th>Email</th>
                                    <th>Criado em</th>
                                    <th>Ações</th>
                                </tr>
                                @foreach($customers as $customer)
                                    <tr>
                                        <td>
                                            <a href="{{route('get_customers_view', $customer->id)}}">{{$customer->name}}</a>
                                        </td>
                                        <td>
                                            <a href="mailto:{{$customer->email}}">{{$customer->email}}</a>
                                        </td>
                                        <td>
                                            {{$customer->created_at->formatLocalized('%d/%m/%Y')}}
                                        </td>
                                        <td>
                                            <a href="{{route('get_customers_view', $customer->id)}}" class="btn btn-light" data-toggle="tooltip" title="Visualizar">
                                                <i class="fa fa-search"></i>
                                            </a>
                                            <a href="{{route('get_customers_edit', $customer->id)}}" class="btn btn-light" data-toggle="tooltip" title="Editar">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            <a href="{{route('get_customers_delete', $customer->id)}}" data-delete-confirm="true" class="btn btn-light" data-toggle="tooltip" title="Excluir">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        {{$customers->appends(request()->except('page'))->links()}}
                    @else
                        @include('partials._no_resources_found', [
                            'not_found_message' => 'Nenhum cliente encontrado',
                        ])

                        {{--Checa se houve busca ou não para determinar se o botão voltar aparece--}}
                        @if(isSearchBeingMade())
                            <a href="{{route('get_customers')}}" class="btn btn-outline-danger pull-right btn-no-mr"><i class="fa fa-reply"></i> Voltar</a>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop

