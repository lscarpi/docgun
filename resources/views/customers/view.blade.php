@extends('layout', [
    'page_title' => 'Dados do cliente: ' . $customer->name,
])

@section('content')
    <ul class="nav nav-tabs" id="customer-nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link {{request()->get('tab', 'main') == 'main' ? 'active' : null }}" id="nav-main-tab" data-toggle="tab" role="tab" href="#nav-main">Principais Dados</a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{request()->get('tab') == 'weapons' ? 'active' : null}}" id="nav-weapons-tab" data-toggle="tab" role="tab" href="#nav-weapons">Armas</a>
        </li>
        <li class="nav-item">
            <a class="nav-link  {{request()->get('tab') == 'issued-documents' ? 'active' : null}}" id="nav-issued-documents-tab" data-toggle="tab" role="tab" href="#nav-issued-documents">Documentos</a>
        </li>
    </ul>

    <div class="tab-content" id="customer-nav-content">
        <div class="tab-pane fade {{request()->get('tab', 'main') == 'main' ? 'show active' : null }}" id="nav-main" role="tabpanel">
            @include('customers._profile', ['customer' => $customer])
        </div>
        <div class="tab-pane fade {{request()->get('tab') == 'weapons' ? 'show active' : null}}" id="nav-weapons" role="tabpanel">
            @include('customers.weapons.index', ['customer' => $customer])
        </div>
        <div class="tab-pane fade {{request()->get('tab') == 'issued-documents' ? 'show active' : null}}" id="nav-issued-documents" role="tabpanel">
            @include('customers.issued_documents.index', ['customer' => $customer])
        </div>
    </div>
@stop

@section('modals')

    {{--ISSUED DOCUMENTS--}}
    <div class="modal fade fadeIn" tabindex="-1" role="dialog" id="show_issued_document_details_modal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <iframe id="iframe_issued_document_details" style="border: none; width: 100%; min-height: 400px;" frameborder="0"></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" id="print_issued_document" class="btn btn-outline-primary" >Imprimir</button>
                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>

    {{--CUSTOMER DATA--}}
    <div class="modal fade fadeIn" tabindex="-1" role="dialog" id="show_customer_data_modal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <iframe id="iframe_customer_data_details" style="border: none; width: 100%; min-height: 400px;" frameborder="0" src="{{route('get_customers_customer_data_details', $customer->id)}}"></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" id="print_customer_data" class="btn btn-outline-primary" >Imprimir</button>
                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('extras_js')
    <script>
        jQuery(document).ready(function($){
            $(document).on('click', '.btn_view_weapon_details', function(e){
                e.preventDefault();
                e.stopImmediatePropagation();

                let
                    weapon_details_route = "{{route('get_customers_weapons_details', ['customer' => $customer->id, 'weapon' => '__WEAPON_ID__'])}}",
                    weapon_id = $(this).data('weapon_id');

                $('#iframe_weapon_details').attr('src', weapon_details_route.replace('__WEAPON_ID__', weapon_id));

                $('#show_weapon_details_modal').modal({
                    backdrop: 'static',
                    keyboard: false,
                })

            });

            $(document).on('hidden.bs.modal', '#show_weapon_details_modal', function(){
                $('#iframe_weapon_details').attr('src', '');
            });

            $(document).on('click', '.btn_view_issued_document_details', function(e){
                e.preventDefault();
                e.stopImmediatePropagation();

                let
                    issued_document_details_route = "{{route('get_customers_issued_documents_details', ['customer' => $customer->id, 'issued_document' => '__ISSUED_DOCUMENT_ID__'])}}",
                    issued_document_id = $(this).data('issued_document_id');

                $('#iframe_issued_document_details').attr('src', issued_document_details_route.replace('__ISSUED_DOCUMENT_ID__', issued_document_id));

                $('#show_issued_document_details_modal').modal({
                    backdrop: 'static',
                    keyboard: false,
                })

            });

            $(document).on('hidden.bs.modal', '#show_issued_document_details_modal', function(){
                $('#iframe_issued_document_details').attr('src', '');
            });

            $(document).on('click', '#print_issued_document', function(){
                $('#iframe_issued_document_details')
                    .get(0)
                    .contentWindow
                    .postMessage({method: 'print'}, '{{asset('')}}');
            });


            //MOSTRAR DADOS DO CLIENTE
            $(document).on('click', '.btn_view_customer_data_details', function(e){
                e.preventDefault();
                e.stopImmediatePropagation();

                $('#show_customer_data_modal').modal({
                    backdrop: 'static',
                    keyboard: false,
                })

            });

            $(document).on('click', '#print_customer_data', function(){
                $('#iframe_customer_data_details')
                    .get(0)
                    .contentWindow
                    .postMessage({method: 'print'}, '{{asset('')}}');
            });
        });
    </script>
@stop