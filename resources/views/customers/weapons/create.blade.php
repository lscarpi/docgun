@extends('layout',[
    'page_title' => 'Nova arma de ' . $customer->name,
])


@section('extras_css')
    <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}">
@stop

@section('content')
    <form method="POST">
        {!! csrf_field() !!}
        <div class="card">
            <div class="card-header">
                Cadastrar Arma
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nota fiscal</label>
                            <input class="form-control" type="text" name="receipt_number" placeholder="" value="{{request()->old('receipt_number')}}" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Tipo</label>
                            <input class="form-control" type="text" name="type" placeholder="" value="{{request()->old('type')}}" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Marca</label>
                            <input class="form-control" type="text" name="brand" placeholder="" value="{{request()->old('brand')}}" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="form-group"><label>Modelo</label>
                                <input class="form-control" type="text" name="model" placeholder="" value="{{request()->old('model')}}" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Calibre</label>
                            <input class="form-control" type="text" name="caliber" placeholder="" value="{{request()->old('caliber')}}" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label>Número de série</label>
                        <input class="form-control" type="text" name="serial_number" placeholder="" value="{{request()->old('serial_number')}}" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Acervo</label>
                            <select name="stash" class="form-control" required>
                                @foreach(\App\Models\Weapon::WEAPON_STASHES as $option_value => $option_name)
                                    <option value="{{$option_value}}" {{request()->old('stash') == $option_value ? 'selected' : null}}>{{$option_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Data de emissão dos documentos</label>
                            <input class="form-control init-datepicker" type="text" name="docs_issued_at init-datepicker" placeholder="" value="{{request()->old('docs_issued_at')}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Número SIGMA</label>
                            <input class="form-control" type="text" name="sigma_number" placeholder="" value="{{request()->old('sigma_number')}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Status do apostilamento</label>
                            <select class="form-control" name="status" id="">
                                <option value="">Nenhum</option>
                                @foreach(\App\Models\Weapon::WEAPON_STATUSES as $option_value => $option_name)
                                    <option value="{{$option_value}}"  {{request()->old('status') == $option_value ? 'selected' : null}}>{{$option_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Número da guia de trânsito</label>
                            <input class="form-control" type="text" name="transit_permission_number" placeholder="" value="{{request()->old('transit_permission_number')}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Data de vencimento da guia de trânsito</label>
                            <input class="form-control init-datepicker" type="text" name="transit_permission_expiry_date" placeholder="" value="{{request()->old('transit_permission_expiry_date')}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="crud-button">
                    <button type="submit" class="btn btn_default_color pull-right">
                        <i class="fa fa-check"></i>
                        Salvar
                    </button>
                    <a href="{{route('get_customers_view', ['customer' => $customer->id, 'tab' => 'weapons'])}}" class="btn btn-outline-danger"><i class="fa fa-reply"></i> Voltar</a>
                </div>
            </div>
        </div>
    </form>
@stop

@section('extras_js')
    <script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js')}}"></script>
    <script src="{{ asset('assets/js/datepickers.js') }}"></script>
@stop