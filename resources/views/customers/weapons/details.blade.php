
@extends('base')

{{--TODO: Montar a view da arma show--}}

<div class="row" style="margin-top: -50px;">
    <div class="col-12 text-center">
        <h1>{{$weapon->getName()}}</h1>
        <hr>
    </div>
</div>

<div class="row">
    <div class="col-4">
        <div class="form-group">
            <p><strong>NF.: </strong>{{$weapon->receipt_number}}</p>
        </div>
    </div>
    <div class="col-4">
        <div class="form-group">
            <p><strong>Tipo: </strong>{{$weapon->type}}</p>
        </div>
    </div>
    <div class="col-4">
        <div class="form-group">
            <p><strong>Marca: </strong>{{$weapon->brand}}</p>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-4">
        <div class="form-group">
            <p><strong>Calibre: </strong>{{$weapon->caliber}}</p>
        </div>
    </div>
    <div class="col-4">
        <div class="form-group">
            <p><strong>N. Série: </strong>{{$weapon->serial_number}}</p>
        </div>
    </div>
    <div class="col-4">
        <div class="form-group">
            <p><strong>Modelo: </strong>{{$weapon->model}}</p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-4">
        <div class="form-group">
            <p><strong>Acervo: </strong>{{$weapon->getStash()}}</p>
        </div>
    </div>
    <div class="col-4">
        <div class="form-group">
            <p><strong>N. SIGMA: </strong>{{$weapon->sigma_number}}</p>
        </div>
    </div>
    <div class="col-4">
        <div class="form-group">
            <p><strong>Status: </strong> <span class="{{$weapon->getStatusClass()}}">{{$weapon->getStatus()}}</span></p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="form-group">
            <p><strong>Dt. Emissão documentos: </strong> {{ $weapon->docs_issued_at ? defaultDateFormat($weapon->docs_issued_at) : '-'}}</p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <p><strong>N. Guia de Trânsito:</strong> {{$weapon->transit_permission_number ?? '-'}}</p>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <p><strong>Dt. Exp. Guia de Trânsito:</strong> {{ $weapon->transit_permissiion_expiry_date ? defaultDateFormat($weapon->transit_permissiion_expiry_date) : '-'}}</p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="form-group">
            <p><strong>Cadastro realizado em: </strong> {{defaultDateTimeFormat($weapon->created_at)}}</p>
        </div>
    </div>
</div>
