@extends('layout', [
    'page_title' => 'Editar ' . $weapon->getName() . ' de ' . $customer->name,
])

@section('extras_css')
    <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}">
@stop


@section('content')
    <form method="POST">
        {!! csrf_field() !!}
        <div class="row">
            <div class="col-lg">
                <div class="form-group">
                    <label>Nota fiscal</label>
                    <input class="form-control" type="text" name="receipt_number" placeholder="Número da nota fiscal da Arma" value="{{$weapon->receipt_number}}" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg">
                <div class="form-group">
                    <label>Tipo</label>
                    <input class="form-control" type="text" name="type" placeholder="Tipo da Arma" value="{{$weapon->type}}" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg">
                <div class="form-group">
                    <label>Marca</label>
                    <input class="form-control" type="text" name="brand" placeholder="Marca da Arma" value="{{$weapon->brand}}" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg">
                <div class="form-group">
                    <label>Calibre</label>
                    <input class="form-control" type="text" name="caliber" placeholder="Calibre da Arma" value="{{$weapon->caliber}}" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg">
                <div class="form-group">
                    <label>Número de série</label>
                    <input class="form-control" type="text" name="serial_number" placeholder="Número de série da Arma" value="{{$weapon->serial_number}}" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg">
                <div class="form-group">
                    <label>Modelo</label>
                    <input class="form-control" type="text" name="model" placeholder="Modelo da Arma" value="{{$weapon->model}}" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg">
                <div class="form-group">
                    <label>Acervo</label>
                    <select name="stash" class="form-control" required>
                        @foreach(\App\Models\Weapon::WEAPON_STASHES as $option_value => $option_name)
                            <option value="{{$option_value}}" {{$weapon->stash == $option_value ? 'selected' : null}}>{{$option_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg">
                <div class="form-group">
                    <label>Data de emissão dos documentos</label>
                    <input class="form-control" type="text" name="docs_issued_at init-datepicker" placeholder="Data de emissão dos documentos da Arma" value="{{$weapon->docs_issued_at}}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg">
                <div class="form-group">
                    <label>Número SIGMA</label>
                    <input class="form-control" type="text" name="sigma_number" placeholder="Número SIGMA da Arma" value="{{$weapon->sigma_number}}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg">
                <div class="form-group">
                    <label>Status do apostilamento</label>
                    <select class="form-control" name="status" id="">
                        <option value="">Nenhum</option>
                        @foreach(\App\Models\Weapon::WEAPON_STATUSES as $option_value => $option_name)
                            <option value="{{$option_value}}"  {{$weapon->status == $option_value ? 'selected' : null}}>{{$option_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg">
                <div class="form-group">
                    <label>Número da guia de trânsito</label>
                    <input class="form-control" type="text" name="transit_permission_number" placeholder="Número da guia de trânsito da Arma" value="{{$weapon->transit_permission_number}}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg">
                <div class="form-group">
                    <label>Data de vencimento da guia de trânsito</label>
                    <input class="form-control" type="text" name="transit_permission_expiry_date init-datepicker" placeholder="Data de vencimento da guia de trânsito da Arma" value="{{$weapon->transit_permission_expiry_date}}">
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <button type="submit" class="btn btn-success">
                <i class="fa fa-check"></i>
                Alterar
            </button>
            <a href="{{route('get_customers_view', ['customer' => $customer->id, 'tab' => 'weapons'])}}" class="btn btn-outline-danger"><i class="fa fa-reply"></i> Cancelar</a>
        </div>
    </form>
@stop

@section('extras_js')
    <script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js')}}"></script>
    <script src="{{ asset('assets/js/datepickers.js') }}"></script>
@stop