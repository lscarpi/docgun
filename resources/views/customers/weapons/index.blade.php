
<div class="row">
    <div class="col-12 mb-4">
        <a href="{{route('get_customers_weapons_create', ['customer' => $customer->id])}}" id="btn_add_weapon" class="btn btn-success">
            <i class="fa fa-plus"></i>
            Adicionar Arma
        </a>
    </div>
    <div class="col-12">
        <div class="alert alert-warning">
            As máscaras de dados para armas cadastradas estão em desenvolvimento e serão adicionadas em breve.
        </div>
        @if($customer->weapons->count())
        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <tr>
                    <th>N. Série</th>
                    <th>Tipo</th>
                    <th>Arma</th>
                    <th>Acervo</th>
                    <th>Status</th>
                    <th>Ações</th>
                </tr>
                @foreach($customer->weapons as $weapon)
                    <tr>
                        <td>{{$weapon->serial_number}}</td>
                        <td>{{$weapon->type}}</td>
                        <td>{{$weapon->getName()}}</td>
                        <td>{{$weapon->getStash()}}</td>
                        <td class="{{$weapon->getStatusClass()}}">{{$weapon->getStatus()}}</td>
                        <td>
                            <a href="" data-weapon_id="{{$weapon->id}}" class="btn btn-light btn_view_weapon_details" data-toggle="tooltip" title="Ver detalhes">
                                <i class="fa fa-search"></i>
                            </a>
                            <a href="{{route('get_customers_weapons_edit', ['customer' => $customer->id, 'weapon' => $weapon->id])}}" class="btn btn-light" data-toggle="tooltip" title="Editar">
                                <i class="fa fa-pencil"></i>
                            </a>

                            <a href="{{route('get_customers_weapons_delete', ['customer' => $customer->id, 'weapon' => $weapon->id])}}" data-delete-confirm="true" class="btn btn-light" data-toggle="tooltip" title="Excluir">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        @else
            <div class="alert alert-warning">
                Nenhuma arma cadastrada para este cliente.
            </div>
        @endif
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="show_weapon_details_modal">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <iframe id="iframe_weapon_details" style="border: none; width: 100%; min-height: 400px;" frameborder="0"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

