@extends('layout', [
    'titulo_pagina' => 'Dashboard'
])

@section('content')

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <img class="img-responsive float-left" style="max-width: 90px" src="{{asset('assets/img/shooting-in-the-air004.png')}}" alt="">
                    <h3>Olá, {{auth()->user()->name}}!</h3>
                    <p class="mb-0">Bem vindo a página inicial da sua conta</p>
                </div>
                <div class="col-lg-6 d-md-down-none">
                    <ul class="list-inline quick-actions">
                        <li class="list-inline-item quick-action">
                            <a href="{{route('get_customers_create')}}">
                                <i class="fa fa-user-plus"></i> <br />
                                Novo Cliente
                            </a>
                        </li>
                        <li class="list-inline-item quick-action">
                            <a href="{{route('get_clubs_create')}}">
                                <i class="fa fa-flag"></i> <br />
                                Novo Clube
                            </a>
                        </li>
                        <li class="list-inline-item quick-action">
                            <a href="{{route('get_documents_create')}}">
                                <i class="fa fa-newspaper-o"></i> <br />
                                Novo Modelo
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <hr />
                </div>
            </div>
            <div class="row" style="margin-top: 50px; min-height: 250px;">
                {{-- Gráfico de clientes nos últimos 06 meses --}}
                <div class="col-md-6">
                    <h5>Total de clientes nos útimos 06 meses</h5>
                    <div id="customers_chart"></div>
                </div>

                {{--  Últimos 05 clientes cadastrados --}}
                <div class="col-md-6">
                    <h5>Últimos clientes cadastrados</h5>
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>Nome</th>
                            <th>Data</th>
                        </tr>
                        @if(count($latest_customers))
                            @foreach($latest_customers as $customer)
                                <tr>
                                    <td>
                                        <a href="{{route('get_customers_edit', $customer->id)}}">
                                            {{$customer->name}}
                                        </a>
                                    </td>
                                    <td>
                                        {{$customer->created_at->formatLocalized('%d/%m/%Y')}}
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="2">Nenhum cliente encontrado.</td>
                            </tr>
                        @endif
                    </table>
                </div>

            </div>

            <div class="row" style="margin-top: 50px; min-height: 250px;">
                {{-- Gráfico do total de documentos emitidos nos últimos 06 meses --}}
                <div class="col-md-6">
                    <h5>Total de documentos gerados nos útimos 06 meses</h5>
                    <div id="issued_documents_chart"></div>
                </div>

                {{-- Últimos 05 documentos emitidos--}}
                <div class="col-md-6">

                    <h5>Últimos documentos emitidos</h5>
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>Título</th>
                            <th>Cliente</th>
                            <th>Data</th>
                        </tr>
                        @if(count($latest_issued_documents))
                            @foreach($latest_issued_documents as $issued_document)
                                <tr>
                                    <td>
                                        {{$issued_document->originalDocument->title}}
                                    </td>
                                    <td>
                                        <a href="{{route('get_customers_edit', $customer->id)}}">
                                            {{$customer->name}}
                                        </a>
                                    </td>
                                    <td>
                                        {{$customer->created_at->formatLocalized('%d/%m/%Y')}}
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="3">Nenhum documento encontrado.</td>
                            </tr>
                        @endif
                    </table>

                </div>
            </div>
        </div>
    </div>
@stop


@section('extras_js')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

        google.charts.load('current', {'packages':['bar']});
        google.charts.setOnLoadCallback(drawChart);


        function drawChart() {

            // Set chart options
            let options = {
                legend: {
                    position: 'none',
                },
                hAxis: {
                    title: '',
                },
                series: {
                    0: {color: '#2F5B38'},
                }
            };

            // ===================== Customers chart
            let array_data = JSON.parse('{!! json_encode($customers_datatable) !!}');
            array_data.unshift(['Mês', 'Total']);
            let data = new google.visualization.arrayToDataTable(array_data, false);

            // Instantiate and draw our chart, passing in some options.
            let chart = new google.charts.Bar(document.getElementById('customers_chart'));
            chart.draw(data, google.charts.Bar.convertOptions(options));


            // ===================== Issued Documents chart
            array_data = JSON.parse('{!! json_encode($issued_documents_datatable) !!}');
            array_data.unshift(['Mês', 'Total']);
            data = new google.visualization.arrayToDataTable(array_data, false);

            // Instantiate and draw our chart, passing in some options.
            chart = new google.charts.Bar(document.getElementById('issued_documents_chart'));
            chart.draw(data, google.charts.Bar.convertOptions(options));
        }
    </script>
@stop