@extends('layout', [
    'page_title' => 'Cadastrar Documento',
])

@section('content')
    <form method="POST">
        {!! csrf_field() !!}
        <div class="card">
            <div class="card-header">
                Criar Modelo de Documento
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="label-sub-header" for="">Título do Documento</label>
                            <input type="text" name="title" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="label-sub-header" for="">Conteúdo do Documento</label>
                            <textarea name="content" id="document_content" cols="30" rows="20"></textarea>
                        </div>
                    </div>
                    @include('partials._masks')
                </div>
            </div>
            <div class="card-footer">
                <div class="crud-button">
                    <a href="{{route('get_documents')}}" class="btn btn-outline-danger"><i class="fa fa-reply"></i> Voltar</a>
                    <button type="submit" class="btn btn_default_color pull-right">
                        <i class="fa fa-check"></i>
                        Salvar
                    </button>
                </div>
            </div>
        </div>
    </form>
@stop

@section('extras_js')
    @include('partials._tinymce_script')
@stop