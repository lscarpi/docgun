{!! $document->content !!}

<style>
    @media print{
        @page {
            margin: 0;
            /*width: 21cm !important;*/
        }

        body{
            margin: 1.6cm;
            /*width: 21cm !important;*/
        }
    }
</style>

<script>
    window.addEventListener('message', receiveMessage, false);

    function receiveMessage(event){
        if(event.data.method === 'print'){
            window.print();
        }
    }
</script>