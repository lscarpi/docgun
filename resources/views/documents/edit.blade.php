@extends('layout', [
    'page_title' => 'Editar Documento: ' . $document->title,
])

@section('content')
    <form method="POST">
        {!! csrf_field() !!}
        <div class="card">
            <div class="card-header">
                Editar Modelo de Documento
            </div>

            <div class="card-body">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Título do Documento</label>
                        <input {{$document->canUpdate(auth()->user()) ? null : 'disabled'}} type="text" name="title" class="form-control" required value="{{$document->title}}">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Conteúdo do Documento</label>
                        <textarea {{$document->canUpdate(auth()->user()) ? null : 'disabled'}} name="content" id="document_content" cols="30" rows="20">{{$document->content}}</textarea>
                    </div>
                </div>
                <div class="col-md-12">
                    @if($document->canUpdate(auth()->user()))
                        @include('partials._masks')
                    @endif
                </div>
            </div>
            <div class="card-footer">
                @if($document->canUpdate(auth()->user()))
                    <div class="crud-button">
                        <button type="submit" class="btn btn_default_color pull-right">
                            <i class="fa fa-check"></i>
                            Salvar
                        </button>
                    </div>
                @endif
                <div class="crud-button">
                    <a href="{{route('get_documents')}}" class="btn btn-outline-danger"><i class="fa fa-reply"></i> Voltar</a>
                </div>
            </div>
        </div>
    </form>
@stop

@section('extras_js')
    @include('partials._tinymce_script', [
        'disabled' => ! $document->canUpdate(auth()->user())
    ])
@stop