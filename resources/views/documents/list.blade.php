@extends('layout', [
    'page_title' => 'Modelos de Documentos',
])

@section('content')
    <div class="row mt-3">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Modelos
                    <a href="{{route('get_documents_create')}}" class="btn btn-outline-light btn-add pull-right">
                        <i class="fa fa-plus"></i> Documento
                    </a>
                </div>
                <div class="card-body">
                    <form method="GET">
                        <div class="form-group input-group">
                            <div class="input-group-prepend">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Buscar</button>
                            </div>
                            <input type="text" class="form-control search-list" placeholder="título do documento" name="busca_texto" value="{{request()->get('busca_texto')}}" aria-label="Username" aria-describedby="basic-addon1">
                            <a href="{{route('get_documents')}}" class="btn btn-secondary"><i class="fa fa-times"></i></a>
                        </div>
                    </form>
                    <hr />
                    <ul class="nav nav-tabs" id="docs-nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link {{request()->get('tab', 'owned-docs') == 'owned-docs' ? 'active' : null }}" id="nav-owned-docs-tab" data-toggle="tab" role="tab" href="#nav-owned-docs">Meus Documentos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{request()->get('tab') == 'shared-docs' ? 'active' : null}}" id="nav-shared-docs-tab" data-toggle="tab" role="tab" href="#nav-shared-docs">Compartilhados docGun</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="docs-nav-content">
                        <div class="tab-pane fade {{request()->get('tab', 'owned-docs') == 'owned-docs' ? 'show active' : null }}" id="nav-owned-docs" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    @if($documents->count())
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover">
                                                <tr>
                                                    <th>Título</th>
                                                    <th>Criado em</th>
                                                    <th>Ações</th>
                                                </tr>
                                                @foreach($documents as $document)
                                                    <tr>
                                                        <td>
                                                            @if($document->isOwnedByCurrentUser())
                                                                <a href="{{route('get_documents_edit', $document->id)}}">{{$document->title}}</a>
                                                            @else
                                                                {{$document->title}}
                                                            @endif

                                                            @if($document->isShared())
                                                                (compartilhado)
                                                            @endif
                                                        </td>
                                                        <td>
                                                            {{$document->created_at->formatLocalized('%d/%m/%Y')}}
                                                        </td>
                                                        <td>
                                                            @if($document->isOwnedByCurrentUser())
                                                                <a href="{{route('get_documents_edit', $document->id)}}" class="btn btn-light" data-toggle="tooltip" title="Editar">
                                                                    <i class="fa fa-pencil"></i>
                                                                </a>
                                                                <a href="{{route('get_documents_delete', $document->id)}}" data-delete-confirm="true" class="btn btn-light" data-toggle="tooltip" title="Excluir">
                                                                    <i class="fa fa-trash"></i>
                                                                </a>
                                                            @endif
                                                            @if($document->currentUserCanShare())
                                                                @if($document->isShared())
                                                                    <a href="{{route('get_documents_unshare', $document->id)}}" class="btn btn-light" data-toggle="tooltip" title="Remover Compartilhamento">
                                                                        <i class="fa fa-stop-circle"></i>
                                                                    </a>
                                                                @else
                                                                    <a href="{{route('get_documents_share', $document->id)}}" class="btn btn-light" data-toggle="tooltip" title="Compartilhar">
                                                                        <i class="fa fa-share"></i>
                                                                    </a>
                                                                @endif
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                        <br />
                                        <br />
                                        {{$documents->appends(array_merge(request()->except('page'), ['tab' => 'owned-docs']))->links()}}
                                    @else

                                        @include('partials._no_resources_found', [
                                            'not_found_message' => 'Nenhum documento encontrado',
                                        ])

                                        {{--Checa se houve busca ou não para determinar se o botão voltar aparece--}}
                                        @if(isSearchBeingMade())
                                            <a href="{{route('get_documents')}}" class="btn btn-outline-danger btn-no-mr pull-right"><i class="fa fa-reply"></i> Voltar</a>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade {{request()->get('tab') == 'shared-docs' ? 'show active' : null }}" id="nav-shared-docs" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    @if($shared_documents->count())
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover">
                                                <tr>
                                                    <th>Título</th>
                                                    <th>Criado em</th>
                                                    <th>Ações</th>
                                                </tr>
                                                @foreach($shared_documents as $shared_document)
                                                    @if($shared_document->isShared())
                                                        <tr>
                                                            <td>
                                                                {{$shared_document->title}}
                                                            </td>
                                                            <td>
                                                                {{$shared_document->created_at->formatLocalized('%d/%m/%Y')}}
                                                            </td>
                                                            <td>
                                                                <a href class="btn btn-light btn_view_document_details" data-document_id="{{$shared_document->id}}" title="Ver detalhes">
                                                                    <i class="fa fa-search"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            </table>
                                        </div>
                                        <br />
                                        <br />
                                        {{$shared_documents->appends(array_merge(request()->except('page'), ['tab' => 'shared-docs']))->links()}}
                                    @else
                                        @include('partials._no_resources_found', [
                                            'not_found_message' => 'Nenhum documento compartilhado encontrado',
                                        ])

                                        {{--Checa se houve busca ou não para determinar se o botão voltar aparece--}}
                                        @if(isSearchBeingMade())
                                            <a href="{{route('get_documents')}}" class="btn btn-outline-danger btn-no-mr pull-right"><i class="fa fa-reply"></i> Voltar</a>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('modals')
    {{--Shared document view detail--}}
    <div class="modal fade fadeIn" tabindex="-1" role="dialog" id="shared_document_view">
        <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal"><i class="fa fa-check"></i> Voltar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

@section('extras_js')
    <script>
        $(document).ready( function () {

            let document_preview_url = "{{route('get_documents_view', '__DOCUMENT_ID__')}}",
                shared_view_modal = $('#shared_document_view');

            $(document).on('click', '.btn_view_document_details', function (e) {
                e.preventDefault();

                let document_id = $(this).data('document_id'),
                    final_preview_url = document_preview_url.replace('__DOCUMENT_ID__', document_id);

                $.get(final_preview_url, function(response){
                   if(response.title && response.content){
                       shared_view_modal.find('.modal-title').text(response.title);
                       shared_view_modal.find('.modal-body').html(response.content);

                       shared_view_modal.modal({
                           backdrop: 'static',
                           keyboard: false,
                       })
                   }else{
                       swal('', 'Erro ao abrir visualização do documento', 'error');
                   }
                });

                return false;
            });

            $(document).on('hidden.bs.modal', '#shared_document_view', function(){
                shared_view_modal.find('.modal-title').text('');
                shared_view_modal.find('.modal-body').html('');
            });
        })

        function ShowSharedDocument() {

        }

    </script>
@stop