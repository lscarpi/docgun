
@extends('emails.admin_notifications.layout', [
    'title' => 'Novo feedback recebido.',
])

@section('content')
    <p>
        Um novo feedback de {{$user->name}} foi recebido. <br />
        <br />
        <strong>Mensagem:</strong> <br />
        <br />
        {{$feedback_message}}
    </p>
@stop