
<table style="width: 1024px; height: 100px; margin: 0 auto">
    <tbody>
    <tr>
        <td style="padding-top: 10px">
            <a href="{{route('get_site_homepage')}}" style="color: #535353" target="_blank" rel="noreferrer">
                <img alt="docGun" src="{{asset('assets/img/docgun-logo-small.png')}}" style="width: 200px;">
            </a>
        </td>
    </tr>
    </tbody>
</table>
<table style="min-width: 1024px; font-family: Arial,Geneva,sans-serif; background-color: #2f5b38; border-radius: 5px 5px 0 0; margin: 0 auto">
    <tbody>
    <tr>
        <td style="height: 5px"><br></td>
    </tr>
    </tbody>
</table>
<table style="width: 1024px; border: 1px solid #e5e5e5; font-family: Arial,Geneva,sans-serif; border-bottom: 0; border-top: 0; margin: 0 auto; background-color: #ffffff">
    <tbody>
    <tr>
        <td style="padding: 15px 30px 0 30px">
            <span style="color: #2f5b38; font-weight: bold; font-size: 24px">{{$title ?? ''}}</span>
        </td>
    </tr>
    </tbody>
</table>
<table style="width: 1024px; border: 1px solid #e5e5e5; font-family: Arial,Geneva,sans-serif; border-bottom: 0; border-top: 0; margin: 0 auto; background-color: #ffffff">
    <tbody>
    <tr>
        <td style="padding: 15px 30px 15px 30px">
                    <span style="font-size: 14px; color: #535353">
                        @yield('content')
                    </span>
        </td>
    </tr>
    </tbody>
</table>

<table style="width: 1024px; border: 1px solid #e5e5e5; font-family: Arial,Geneva,sans-serif; border-bottom: 0; border-top: 0; margin: 0 auto; background-color: #ffffff">
    <tbody>
    <tr>
        <td style="padding: 15px 30px 15px 30px">
                <span style="font-size: 14px; color: #535353">
                    Atenciosamente,<br>
                    Equipe docGun
                </span>
        </td>
    </tr>
    </tbody>
</table>
<table style="width: 1024px; font-family: Arial,Geneva,sans-serif; padding: 0 0 25px 0; font-size: 10px; margin: 0 auto; border-top: 1px solid #e5e5e5">
    <tbody>
    <tr>
        <td style="width: 130px">
            <span style="color: #999999">©{{\Carbon\Carbon::now()->year}} docGun</span>
            <span style="color: #d4d4d6; margin: 0 10px">|</span>
            <span style="color: #bdbdc1">Todos os direitos reservados.</span>
        </td>
    </tr>
    </tbody>
</table>