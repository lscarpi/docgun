@extends('emails.admin_notifications.layout', [
    'title' => 'Novo usuário registrado',
])

@section('content')
    <p>
        O usuário {{$user->name}} acabou de se registrar<br />
        <br />
        <strong>Email: </strong> {{$user->email}}<br />
    </p>
@stop