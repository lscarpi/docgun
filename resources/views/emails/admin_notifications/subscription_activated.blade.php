@extends('emails.admin_notifications.layout', [
    'title' => 'Assinatura ativada',
])

@section('content')
    <p>
        Uma assinatura acabou de ser ativada<br />
        <br />
        <strong>Usuário: </strong> {{$user->name}} <br />
        <strong>Plano: </strong> {{$subscription->plan->name}}<br />
    </p>
@stop