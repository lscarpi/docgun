@extends('emails.user_notifications.layout')

@section('content')
    <div style="background-image:url('{{asset('assets/img/bg.png')}}');background-position:top left;background-repeat:repeat;;background-color:#E9E9E9;">
        <div style="Margin: 0 auto;min-width: 320px;max-width: 585px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #e9e9e9;" class="block-grid ">
            <div style="border-collapse: collapse;display: table;width: 100%;background-color:#e9e9e9;">
                <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="background-image:url('{{asset('assets/img/bg.png')}}');background-position:top left;background-repeat:repeat;;background-color:#E9E9E9" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 585px;"><tr class="layout-full-width" style="background-color:#e9e9e9;"><![endif]-->

                <!--[if (mso)|(IE)]><td align="center" width="585" style=" width:585px; padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><![endif]-->
                <div class="col num12" style="min-width: 320px;max-width: 585px;display: table-cell;vertical-align: top;">
                    <div style="background-color: transparent; width: 100% !important;">
                        <!--[if (!mso)&(!IE)]><!--><div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;"><!--<![endif]-->


                            <div class="">
                                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 20px; padding-left: 20px; padding-top: 10px; padding-bottom: 10px;"><![endif]-->
                                <div style="color:#2f5b38;font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;line-height:150%; padding-right: 20px; padding-left: 20px; padding-top: 10px; padding-bottom: 10px;">
                                    <div style="line-height:18px;font-size:12px;font-family:Montserrat, 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;color:#2f5b38;text-align:left;"><p style="margin: 0;line-height: 18px;text-align: center;font-size: 12px"><span style="font-size: 26px; line-height: 39px;"><strong><span style="line-height: 39px; font-size: 26px;">Obrigado por se juntar ao time docGun</span></strong></span></p></div>
                                </div>
                                <!--[if mso]></td></tr></table><![endif]-->
                            </div>

                            <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                    </div>
                </div>
                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
            </div>
        </div>
    </div>
    <div style="background-image:url('{{asset('assets/img/bg.png')}}');background-position:top left;background-repeat:repeat;;background-color:#E9E9E9;">
        <div style="Margin: 0 auto;min-width: 320px;max-width: 585px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #e9e9e9;" class="block-grid ">
            <div style="border-collapse: collapse;display: table;width: 100%;background-color:#e9e9e9;">
                <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="background-image:url('{{asset('assets/img/bg.png')}}');background-position:top left;background-repeat:repeat;;background-color:#E9E9E9" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 585px;"><tr class="layout-full-width" style="background-color:#e9e9e9;"><![endif]-->

                <!--[if (mso)|(IE)]><td align="center" width="585" style=" width:585px; padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><![endif]-->
                <div class="col num12" style="min-width: 320px;max-width: 585px;display: table-cell;vertical-align: top;">
                    <div style="background-color: transparent; width: 100% !important;">
                        <!--[if (!mso)&(!IE)]><!--><div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;"><!--<![endif]-->


                            <div class="">
                                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 20px; padding-left: 20px; padding-top: 20px; padding-bottom: 10px;"><![endif]-->
                                <div style="color:#2f5b38;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:150%; padding-right: 20px; padding-left: 20px; padding-top: 20px; padding-bottom: 10px;">
                                    <div style="font-size:12px;line-height:18px;color:#2f5b38;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">
                                        <p style="margin: 0;font-size: 14px;line-height: 21px;text-align: center">
                                                    <span style="font-size: 20px; line-height: 30px;">
                                                        Olá {{$user->firstName()}}, recebemos seu pagamento com sucesso. <br />
                                                        Seu plano <strong>{{$subscription->plan->name}}</strong> foi ativado. Aproveite todas as funcionalidades!
                                                    </span>
                                        </p>
                                    </div>
                                </div>
                                <!--[if mso]></td></tr></table><![endif]-->
                            </div>

                            <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                    </div>
                </div>
                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
            </div>
        </div>
    </div>
    <div style="background-image:url('{{asset('assets/img/bg.png')}}');background-position:top left;background-repeat:repeat;;background-color:#E9E9E9;">
        <div style="Margin: 0 auto;min-width: 320px;max-width: 585px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #E9E9E9;" class="block-grid ">
            <div style="border-collapse: collapse;display: table;width: 100%;background-color:#E9E9E9;">
                <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="background-image:url('{{asset('assets/img/bg.png')}}');background-position:top left;background-repeat:repeat;;background-color:#E9E9E9" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 585px;"><tr class="layout-full-width" style="background-color:#E9E9E9;"><![endif]-->

                <!--[if (mso)|(IE)]><td align="center" width="585" style=" width:585px; padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><![endif]-->
                <div class="col num12" style="min-width: 320px;max-width: 585px;display: table-cell;vertical-align: top;">
                    <div style="background-color: transparent; width: 100% !important;">
                        <!--[if (!mso)&(!IE)]><!--><div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;"><!--<![endif]-->


                            <div class="mobile_hide">
                                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;"><![endif]-->
                                <div style="color:#555555;line-height:200%;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                    <div style="font-size:12px;line-height:24px;color:#555555;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;"><p style="margin: 0;font-size: 12px;line-height: 24px"><br data-mce-bogus="1"></p></div>
                                </div>
                                <!--[if mso]></td></tr></table><![endif]-->
                            </div>

                            <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                    </div>
                </div>
                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
            </div>
        </div>
    </div>
    <div style="background-image:url('{{asset('assets/img/bg.png')}}');background-position:top left;background-repeat:repeat;;background-color:#E9E9E9;">
        <div style="Margin: 0 auto;min-width: 320px;max-width: 585px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #78866b;" class="block-grid ">
            <div style="border-collapse: collapse;display: table;width: 100%;background-color:#78866b;">
                <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="background-image:url('{{asset('assets/img/bg.png')}}');background-position:top left;background-repeat:repeat;;background-color:#E9E9E9" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 585px;"><tr class="layout-full-width" style="background-color:#78866b;"><![endif]-->

                <!--[if (mso)|(IE)]><td align="center" width="585" style=" width:585px; padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><![endif]-->
                <div class="col num12" style="min-width: 320px;max-width: 585px;display: table-cell;vertical-align: top;">
                    <div style="background-color: transparent; width: 100% !important;">
                        <!--[if (!mso)&(!IE)]><!--><div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;"><!--<![endif]-->


                            <div class="">
                                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 20px; padding-left: 20px; padding-top: 20px; padding-bottom: 10px;"><![endif]-->
                                <div style="color:#FFFFFF;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:150%; padding-right: 20px; padding-left: 20px; padding-top: 20px; padding-bottom: 10px;">
                                    <div style="line-height:18px;font-size:12px;color:#FFFFFF;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;"><p style="margin: 0;line-height: 18px;text-align: center;font-size: 12px"><span style="font-size: 20px; line-height: 30px;">Continue seus trabalhos:</span></p></div>
                                </div>
                                <!--[if mso]></td></tr></table><![endif]-->
                            </div>



                            <div align="center" class="button-container center " style="padding-right: 20px; padding-left: 20px; padding-top:15px; padding-bottom:25px;">
                                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"><tr><td style="padding-right: 20px; padding-left: 20px; padding-top:15px; padding-bottom:25px;" align="center"><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="#" style="height:40pt; v-text-anchor:middle; width:138pt;" arcsize="19%" strokecolor="#555555" fillcolor="#2F5B38"><w:anchorlock/><v:textbox inset="0,0,0,0"><center style="color:#FFFFFF; font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size:16px;"><![endif]-->
                                <a href="{{route('get_login')}}" target="_blank" style="display: block;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #FFFFFF; background-color: #2F5B38; border-radius: 10px; -webkit-border-radius: 10px; -moz-border-radius: 10px; max-width: 185px; width: 135px;width: auto; border-top: 1px solid #555555; border-right: 1px solid #555555; border-bottom: 1px solid #555555; border-left: 1px solid #555555; padding-top: 10px; padding-right: 25px; padding-bottom: 10px; padding-left: 25px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;mso-border-alt: none">
                                    <span style="font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;font-size:16px;line-height:32px;"><span style="font-size: 14px;"><span style="font-family: arial, helvetica, sans-serif;"><strong>ACESSAR O SISTEMA</strong></span></span></span>
                                </a>
                                <!--[if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif]-->
                            </div>


                            <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                    </div>
                </div>
                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
            </div>
        </div>
    </div>
@endsection