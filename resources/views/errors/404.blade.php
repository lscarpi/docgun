@extends('base')

@section('extras_css')
    @include('partials._page_full_height')
@stop

@section('page')
    <div style="display: flex; height: 100%; justify-content: center">
        <div class="flex-row align-self-center text-center" style="width: 100%;">
            <img width="200px;" src="{{asset('assets/img/errors/404.png')}}" alt="" style="margin-bottom: 30px;">
            <h3 class="">Página não encontrada, táoquei?</h3> <br />
            <a href="{{route('get_dashboard')}}" class="btn btn_default_color">
                <i class="fa fa-dashboard"></i>
                Voltar para Dashboard
            </a>
        </div>
    </div>
@stop