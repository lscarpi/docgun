@extends('base')

@section('extras_css')
    @include('partials._page_full_height')
@stop

@section('page')
    <div style="display: flex; height: 100%; justify-content: center">
        <div class="flex-row align-self-center text-center" style="width: 100%;">
            <img width="400px;" src="{{asset('assets/img/errors/500.png')}}" alt="">
            <h3 class="">Ooops! Erro encontrado no sistema.</h3>
            <p>Fique tranquilo, nossa equipe já foi avisada.</p>
            <a href="{{route('get_dashboard')}}" class="btn btn_default_color">
                <i class="fa fa-dashboard"></i>
                Voltar para Dashboard
            </a>
        </div>
    </div>
@stop