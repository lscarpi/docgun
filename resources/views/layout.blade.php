@extends('base', [
    'page_title' => isset($page_title) ? $page_title : null,
    'body_class' => 'dotted app header-fixed sidebar-fixed sidebar-compact'
])
@section('page')
    <header class="app-header navbar">
        <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button">☰</button>
        <a class="navbar-brand text-center" href="{{route('get_dashboard')}}">
            <img class="logomarca" src="{{asset('assets/img/docgun-logo.png')}}" alt="">
        </a>

        <ul class="nav navbar-nav ml-auto" style="margin-right: 50px;">
            @if(auth()->user()->isCompanyOwner() && auth()->user()->company->isOnTrial())
                <li class="nav-item" style="margin-right: 25px !important;">
                    <a href="{{route('get_account_subscription')}}" class="nav nav-link btn p-0 d-sm-down-none" id="planUpgrade">
                        <img src="{{asset('assets/img/docgun-upgrade.png')}}" alt="" style="height: 40px;">
                        <span style="width: 40px; padding: 0 10px; margin-left: -10px;">
                            Upgrade de Plano
                        </span>
                    </a>
                </li>
            @endif

            <li class="nav-item dropdown" >
                <a class="nav-link nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    {{--<img class="img-avatar" src="img/avatars/6.jpg">--}}
                    Olá, {{auth()->user()->firstName()}}
                    <i class="fa fa-caret-down"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    {{--<div class="dropdown-header text-center">--}}
                        {{--<strong>Settings</strong>--}}
                    {{--</div>--}}
                    @if(auth()->user()->isAdmin())
                        <a href="{{route('admin.getDashboard')}}" class="dropdown-item">
                            <i class="fa fa-wrench"></i> Admin Dashboard
                        </a>
                    @endif
                    <a class="dropdown-item" href="{{route('get_account_profile')}}">
                        <i class="fa fa-user"></i> Meu Perfil
                    </a>
                    <a class="dropdown-item" href="{{route('get_account_company')}}">
                        <i class="fa fa-institution"></i> Dados da Empresa
                    </a>
                    <a class="dropdown-item" href="">
                        <i class="fa fa-key"></i> Reiniciar Senha
                    </a>
                    @if(auth()->user()->isCompanyOwner() && ! auth()->user()->company->getCurrentSubscription()->isRightfulHeirToTheThrone())
                        <a class="dropdown-item" href="{{route('get_account_subscription')}}">
                            <i class="fa fa-angle-double-up"></i> Upgrade de Plano
                        </a>
                    @endif
                    <a class="dropdown-item" href="{{route('get_logout')}}">
                        <i class="fa fa-sign-out"></i> Sair
                    </a>
                </div>
            </li>
        </ul>

    </header>
    <div class="app-body">
        @include('partials._sidebar')

        <!-- Main content -->
        <main class="main">

            <div class="container-fluid">
                <div class="animated fadeIn">

                    {{-- Alert para trial do cliente--}}
                    @if(auth()->user()->isOnTrialSubscription())
                        <div class="alert alert-warning">
                            Você está em período de testes da plataforma. Seu teste se encerra em {{pluralize(auth()->user()->getCompanySubscription()->trialDaysRemaining(), 'dia', 'dias')}}
                        </div>
                    @endif

                    {{--Mensagens em flash--}}
                    @include('partials._flash')

                    {{--Mensagens em erro de forms--}}
                    @include('partials._errors')

                    {{--Conteúdo da página--}}
                    @yield('content')
                </div>

            </div>
            <!-- /.conainer-fluid -->
        </main>
    </div>

    {{--Feedback modal--}}
    <div class="modal fade fadeIn" tabindex="-1" role="dialog" id="sendFeedbackModal">
        <div class="modal-dialog" role="document">
            <form method="POST" action="{{route('post_send_feedback')}}">
            {{csrf_field()}}
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Enviar feedback</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Por favor, conte-nos o que está achando do docGun:</label>
                        <textarea name="feedback_message" class="form-control" style="resize: none;" rows="10"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Enviar Feedback</button>
                </div>
            </div>
            </form>
        </div>
    </div>

    <footer class="app-footer">
        <a href="https://monazite.com.br" target="_blank"><span style="color: #d2a147">mona</span><span style="color: #0a0a0a">zite</span></a> © {{\Carbon\Carbon::now()->year}} docGun -
        <a href="" id="btnSendFeedback" class="btn btn-sm btn-outline-default">
            Enviar feedback
        </a>
    </footer>
@stop

@section('layout_js')
    <script>
        jQuery(document).ready(function($){
            $(document).on('click', '#btnSendFeedback', function(e){
                e.preventDefault();
                e.stopImmediatePropagation();

                $('#sendFeedbackModal').modal({
                    'keyboard': false,
                    'backdrop': 'static',
                })

            });
        });
    </script>
@stop