<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <a class="navband" href="{{route('get_dashboard')}}">
        {{--<img src="https://github.com/lscarpi/bonigun/blob/master/public/assets/images/bonigun-logo.png?raw=true" alt="bonigun-logo">--}}
        <img class="doctudo-logo" src="{{asset('img/doc-tudo.png')}}" alt="doctudo-logo">
    </a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse"  id="navbarResponsive">
        <ul class="navbar-nav navbar-sidenav">
            {{--<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">--}}
                {{--<a class="nav-link" href="{{route('get_dashboard')}}">--}}
                    {{--<i class="fa fa-fw fa-dashboard"></i>--}}
                    {{--<span class="nav-link-text">Dashboard</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Clientes">
                <a class="nav-link default_color_dark" href="{{route('get_customers')}}">
                    <i class="fa fa-fw fa-users"></i>
                    <span class="nav-link-text">Clientes</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Clubes">
                <a class="nav-link" href="{{route('get_clubs')}}">
                    <i class="fa fa-fw fa-bullseye"></i>
                    <span class="nav-link-text">Clubes</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Documentos">
                <a class="nav-link" href="{{route('get_documents')}}">
                    <i class="fa fa-fw fa-file"></i>
                    <span class="nav-link-text">Modelos</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Usuários">
                <a class="nav-link" href="{{route('get_users')}}">
                    <i class="fa fa-fw fa-user"></i>
                    <span class="nav-link-text">Usuários</span>
                </a>
            </li>
            {{--// Mostra Companies View para usuário admin--}}
            @if(auth()->user()->isAdmin())
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Usuários">
                    <a class="nav-link" href="{{route('get_companies')}}">
                        <i class="fa fa-fw fa-user"></i>
                        <span class="nav-link-text">Empresas</span>
                    </a>
                </li>
            @endif
        </ul>
        <ul class="navbar-nav sidenav-toggler">
            <li class="nav-item">
                <a class="nav-link text-center" id="sidenavToggler">
                    <i class="fa fa-fw fa-angle-left"></i>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{route('get_logout')}}" id="nav-top-right">
                    {{--Olá, {{auth()->user()->firstName()}}.--}}
                    <i class="fa fa-fw fa-sign-out"></i> Logout
                </a>
            </li>
        </ul>
    </div>
</nav>