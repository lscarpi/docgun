
@if(session()->has('flash-message'))
    <div class="alert alert-{{session('flash-message')->type}}">
        {!! session('flash-message')->message !!}
    </div>
@endif
