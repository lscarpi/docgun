<div class="row">
    @foreach($masks as $mask => $unused)
            <div class="col-md-3 mask-btn">
                <div class="mask" data-mask="{{ '{' . $mask . '}' }}" data-toggle="tooltip" title="Clique para adicionar">
                    {{ '{' . $mask . '}' }}
                </div>
            </div>
    @endforeach
</div>