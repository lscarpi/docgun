<div class="col-md-12">
    <div class="">
        <label class="label-sub-header" for="">Máscaras de Dados</label>
        <ul class="nav nav-tabs" id="mask-nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link {{request()->get('tab', 'customer-data') == 'customer-data' ? 'active' : null }}" id="nav-customer-data-tab" data-toggle="tab" role="tab" href="#nav-customer-data">Pessoal</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{request()->get('tab') == 'company-data' ? 'active' : null }}" id="nav-company-data-tab" data-toggle="tab" role="tab" href="#nav-company-data">Empresarial</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{request()->get('tab') == 'residential-address' ? 'active' : null}}" id="nav-residential-address-tab" data-toggle="tab" role="tab" href="#nav-residential-address">Residencial</a>
            </li>
            <li class="nav-item">
                <a class="nav-link  {{request()->get('tab') == 'commercial-address' ? 'active' : null}}" id="nav-commercial-address-tab" data-toggle="tab" role="tab" href="#nav-commercial-address">Comercial</a>
            </li>
            <li class="nav-item">
                <a class="nav-link  {{request()->get('tab') == 'club-data' ? 'active' : null}}" id="nav-club-tab" data-toggle="tab" role="tab" href="#nav-clubs">Clubes</a>
            </li>
            <li class="nav-item">
                <a class="nav-link  {{request()->get('tab') == 'documents' ? 'active' : null}}" id="nav-documents-tab" data-toggle="tab" role="tab" href="#nav-documents">Documentos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link  {{request()->get('tab') == 'helpers' ? 'active' : null}}" id="nav-helpers-tab" data-toggle="tab" role="tab" href="#nav-helpers">Datas</a>
            </li>
        </ul>
        <div class="tab-content" id="documents-nav-content">
            <div class="tab-pane fade {{request()->get('tab', 'customer-data') == 'customer-data' ? 'show active' : null }}" id="nav-customer-data" role="tabpanel">
                @include('partials._mask_tab', ['masks' => \App\Models\Mask::MASK_MAPPINGS_CUSTOMER])
            </div>
            <div class="tab-pane fade {{request()->get('tab') == 'residential-address' ? 'show active' : null}}" id="nav-residential-address" role="tabpanel">
                @include('partials._mask_tab', ['masks' => \App\Models\Mask::MASK_MAPPINGS_RES_ADDRESS])
            </div>
            <div class="tab-pane fade {{request()->get('tab') == 'commercial-address' ? 'show active' : null}}" id="nav-commercial-address" role="tabpanel">
                @include('partials._mask_tab', ['masks' => \App\Models\Mask::MASK_MAPPINGS_COM_ADDRESS])
            </div>
            <div class="tab-pane fade {{request()->get('tab') == 'club-data' ? 'show active' : null}}" id="nav-clubs" role="tabpanel">
                @include('partials._mask_tab', ['masks' => \App\Models\Mask::MASK_MAPPINGS_CLUBS])
            </div>
            <div class="tab-pane fade {{request()->get('tab') == 'documents' ? 'show active' : null}}" id="nav-documents" role="tabpanel">
                @include('partials._mask_tab', ['masks' => \App\Models\Mask::MASK_MAPPINGS_DOCUMENTS])
            </div>
            <div class="tab-pane fade {{request()->get('tab') == 'helpers' ? 'show active' : null}}" id="nav-helpers" role="tabpanel">
                @include('partials._mask_tab', ['masks' => \App\Models\Mask::MASK_MAPPINGS_HELPERS])
            </div>
            <div class="tab-pane fade {{request()->get('tab') == 'company_data' ? 'show active' : null}}" id="nav-company-data" role="tabpanel">
                @include('partials._mask_tab', ['masks' => \App\Models\Mask::MASK_MAPPINGS_COMPANIES])
            </div>
        </div>
    </div>
</div>