<div class="text-center">
    <img style="width: 100px;" src="{{asset('assets/img/errors/404.png')}}" alt="">
    <br />
    <br />
    <h5>{{$not_found_message}}</h5>
</div>