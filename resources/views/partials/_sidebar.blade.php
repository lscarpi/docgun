<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">

            <li class="nav-item">
                <a class="nav-link" href="{{route('get_dashboard')}}" ><i class="fa fa-home"></i> Início</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('get_customers')}}" ><i class="fa fa-users"></i> Clientes</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('get_clubs')}}" ><i class="fa fa-flag-o"></i> Clubes</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('get_documents')}}"><i class="fa fa-newspaper-o"></i> Modelos</a>
            </li>

            @if(auth()->user()->isCompanyOwner())
                <li class="nav-item">
                    <a class="nav-link" href="{{route('get_users')}}"><i class="fa fa-user-o"></i> Usuários</a>
                </li>
            @endif

            <button class="navbar-toggler sidebar-minimizer text-center d-md-down-none" id="sidebarToggler" type="button">☰</button>



            {{--<button class="navbar-toggler sidebar-minimizer d-md-down-none" type="button"><i class="fa fa-fw fa-angle-left"></i></button>--}}

            {{--<li class="nav-item nav-dropdown">--}}
                {{--<a class="nav-link nav-dropdown-toggle" href="#"><i class="icon-star"></i> Cotações</a>--}}
                {{--<ul class="nav-dropdown-items">--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="index.html"><i class="icon-speedometer"></i> Em aberto</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="index.html"><i class="icon-speedometer"></i> Listar Todas</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
        </ul>
    </nav>
</div>