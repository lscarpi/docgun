<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=6qro22doo21hdqen8mk6p6x9agrs1kgo1alxmongrzyzivfy"></script>
<script>
    tinymce.init({
        height: '400px',
        selector:'#document_content',
        language_url: "{{asset('assets/plugins/tinymce/langs/pt_BR.js')}}",
        toolbar1: 'fontselect formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | table | code',
        branding: false,
        elementpath: false,
        plugins: 'table code',
    });

    jQuery(document).ready(function($){

        @if(isset($disabled) && $disabled)
            tinymce.get('document_content').setMode('readonly');
        @endif

        $(document).on('click', 'div.mask', function(){
            tinymce.get('document_content').execCommand('mceInsertContent', false, $(this).data('mask'));
        });
    });

</script>