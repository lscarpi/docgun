@extends('layout', [
    'page_title' => 'Cadastrar Usuário'
])

@section('content')
    <form method="POST">
        {!! csrf_field() !!}
        <div class="card">
            <div class="card-header">
                Cadastrar Usuário
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Nome</label>
                            <input class="form-control" type="text" name="name" required value="{{request()->old('name')}}">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Email</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-at"></i>
                                </div>
                                <input class="form-control" type="email" name="email" required value="{{request()->old('email')}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Senha</label>
                            <input class="form-control" type="password" name="password" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Confirme a Senha</label>
                            <input class="form-control" type="password" name="password_confirmation" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="crud-button">
                    <a href="{{route('get_users')}}" class="btn btn-outline-danger"><i class="fa fa-reply"></i> Voltar</a>
                    <button type="submit" class="btn btn_default_color pull-right">
                        <i class="fa fa-check"></i>
                        Salvar
                    </button>
                </div>
            </div>
        </div>
    </form>
@stop