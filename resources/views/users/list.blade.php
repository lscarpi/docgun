@extends('layout', [
    'page_title' => 'Usuários'
])

@section('content')
    <div class="row mt-3">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Usuários
                    <a href="{{route('get_users_create')}}" class="btn btn-outline-light btn-add pull-right">
                        <i class="fa fa-plus"></i> Usuário
                    </a>
                </div>
                <div class="card-body">
                    <div class="alert alert-info">
                        Seu plano atual lhe permite o cadastro de mais {{pluralize(auth()->user()->company->usersRemaining(), 'usuário', 'usuários')}}.
                    </div>
                    @if($users->count())
                        <form method="GET">
                            <div class="form-group input-group">
                                <div class="input-group-prepend">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Buscar</button>
                                </div>
                                <input type="text" class="form-control search-list" placeholder="nome do usuário" name="busca_texto" value="{{request()->get('busca_texto')}}" aria-label="Username" aria-describedby="basic-addon1">
                                <a href="{{route('get_users')}}" class="btn btn-secondary"><i class="fa fa-times"></i></a>

                            </div>


                            {{--<div class="form-group">--}}
                            {{--<label for="">Busca</label>--}}
                            {{--<input type="text" placeholder="Pesquise pelo nome ou email" name="busca_texto" value="{{request()->get('busca_texto')}}" class="form-control">--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                            {{--<button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Buscar</button>--}}
                            {{--<a href="{{route('get_customers')}}" class="btn btn-secondary"><i class="fa fa-times"></i> Limpar Busca</a>--}}
                            {{--</div>--}}
                        </form>
                        <hr />
                        <div class="table table-responsive">
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <th>Nome</th>
                                    <th>Email</th>
                                    <th>Criado em</th>
                                    <th>Ações</th>
                                </tr>
                                @foreach($users as $user)
                                    <tr>
                                        <td>
                                            <a href="{{route('get_users_edit', $user->id)}}">{{$user->name}}</a>
                                        </td>
                                        <td>
                                            <a href="mailto:{{$user->email}}">{{$user->email}}</a>
                                        </td>
                                        <td>
                                            {{$user->created_at->formatLocalized('%d/%m/%Y')}}
                                        </td>
                                        <td>
                                            <a href="{{route('get_users_edit', $user->id)}}" class="btn btn-light" data-toggle="tooltip" title="Editar">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            @if($user->id != auth()->user()->id)
                                                <a href="{{route('get_users_delete', $user->id)}}" data-delete-confirm="true" class="btn btn-light" data-toggle="tooltip" title="Excluir">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        {{$users->appends(request()->except('page'))->links()}}
                    @else
                        <div class="alert alert-warning">
                            Nenhum usuário encontrado.
                        </div>
                        @if(isSearchBeingMade())
                            <a href="{{route('get_users')}}" class="btn btn-outline-danger btn-no-mr pull-right"><i class="fa fa-reply"></i> Voltar</a>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop