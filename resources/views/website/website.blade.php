<!DOCTYPE html>
<html lang="en">
<head>
   <!-- Global site tag (gtag.js) - Google Analytics -->
   <script async src="https://www.googletagmanager.com/gtag/js?id=UA-135041517-1"></script>
   <script>
       window.dataLayer = window.dataLayer || [];
       function gtag(){dataLayer.push(arguments);}
       gtag('js', new Date());

       gtag('config', 'UA-135041517-1');
   </script>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>docGun | Sistema </title>
<link href="{{asset('favicon.ico')}}" rel="icon">

<!--Plugins css-->
<link rel="stylesheet" href="{{asset('css/plugins.css')}}">

<!--Custom Styles-->
<link rel="stylesheet" href="{{asset('css/style.css')}}">
<link rel="stylesheet" href="{{asset('css/yellow.css')}}">
<link rel="stylesheet" href="{{asset('css/custom.css')}}">

</head>
<body data-spy="scroll" data-target=".navbar" data-offset="90">

<!--PreLoader-->
<div class="loader">
   <div class="loader-inner">
      <div class="loader-blocks">
         <span class="block-1"></span>
         <span class="block-2"></span>
         <span class="block-3"></span>
         <span class="block-4"></span>
         <span class="block-5"></span>
         <span class="block-6"></span>
         <span class="block-7"></span>
         <span class="block-8"></span>
         <span class="block-9"></span>
         <span class="block-10"></span>
         <span class="block-11"></span>
         <span class="block-12"></span>
         <span class="block-13"></span>
         <span class="block-14"></span>
         <span class="block-15"></span>
         <span class="block-16"></span>
      </div>
   </div>
</div>
<!--PreLoader Ends-->

<!-- header -->
<header class="site-header">
   <nav class="navbar navbar-expand-lg transparent-bg static-nav">
      <div class="container">
         <a class="navbar-brand" href="#">
            <img src="{{asset('assets/img/docgun-200px.png')}}" alt="logo">
         </a>
         <button class="navbar-toggler navbar-toggler-right collapsed" type="button" data-toggle="collapse" data-target="#xenav">
            <span> </span>
            <span> </span>
            <span> </span>
         </button>
         <div class="collapse navbar-collapse" id="xenav">
            <ul class="navbar-nav ml-auto">
               {{--<li class="nav-item active">--}}
                  {{--<a class="nav-link" href="#home">Home</a>--}}
               {{--</li>--}}
               <li class="nav-item ">
                  <a class="nav-link nav-custom" href="#our-feature">Teste grátis</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link nav-custom" href="#our-apps">Recursos</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link nav-custom" href="#our-pricings">Planos</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link nav-custom" href="#our-testimonial">Clientes</a>
               </li>
               <li class="nav-item">
                  <a class="btn btn-login" href="{{route('get_login')}}">Entrar / Cadastrar</a>
               </li>
            </ul>
         </div>
      </div>

      <!--side menu open button-->
      {{--<a href="javascript:void(0)" class="d-none d-lg-inline-block sidemenu_btn" id="sidemenu_toggle">--}}
          {{--<span></span> <span></span> <span></span>--}}
       {{--</a>--}}
   </nav>

   <!-- side menu -->
   {{--<div class="side-menu">--}}
      {{--<div class="inner-wrapper">--}}
         {{--<span class="btn-close" id="btn_sideNavClose"><i></i><i></i></span>--}}
         {{--<nav class="side-nav">--}}
            {{--<ul class="navbar-nav w-100">--}}
               {{--<li class="nav-item active">--}}
                  {{--<a class="nav-link" href="#home">Home</a>--}}
               {{--</li>--}}
               {{--<li class="nav-item">--}}
                  {{--<a class="nav-link" href="#our-feature">Features</a>--}}
               {{--</li>--}}
               {{--<li class="nav-item">--}}
                  {{--<a class="nav-link" href="#our-team">Team</a>--}}
               {{--</li>--}}
               {{--<li class="nav-item">--}}
                  {{--<a class="nav-link" href="#our-portfolio">Portfolio</a>--}}
               {{--</li>--}}
               {{--<li class="nav-item">--}}
                  {{--<a class="nav-link" href="#our-pricings">Packages</a>--}}
               {{--</li>--}}
               {{--<li class="nav-item">--}}
                  {{--<a class="nav-link" href="#our-testimonial">Clients</a>--}}
               {{--</li>--}}
               {{--<li class="nav-item">--}}
                  {{--<a class="nav-link" href="#our-blog">Blog</a>--}}
               {{--</li>--}}
               {{--<li class="nav-item">--}}
                  {{--<a class="nav-link" href="#contactus">contact</a>--}}
               {{--</li>--}}
            {{--</ul>--}}
         {{--</nav>--}}

         {{--<div class="side-footer w-100">--}}
            {{--<ul class="social-icons-simple white top40">--}}
            {{--<li><a href="javascript:void(0)"><i class="fa fa-facebook"></i> </a> </li>--}}
            {{--<li><a href="javascript:void(0)"><i class="fa fa-instagram"></i> </a> </li>--}}
            {{--<li><a href="javascript:void(0)"><i class="fa fa-twitter"></i> </a> </li>--}}
         {{--</ul>--}}
         {{--<p class="whitecolor">monazite | 2018</p>--}}
         {{--</div>--}}
      {{--</div>--}}
   {{--</div>--}}
   {{--<a id="close_side_menu" href="javascript:void(0);"></a>--}}
   <!-- End side menu -->
</header>
<!-- header -->


<!--Main Slider-->
<div class="rotating-slider padding_top full-screen center-block parallaxie" id="home">
   <div class="container">
      <div class="row padding_half">
         <div class="col-md-12">
            <div id="text-fading" class="owl-carousel text-center">
               {{--<div class="item bottom30">--}}
                  {{--<h2 class="text-capitalize font-xlight whitecolor">--}}
                     {{--<span class="d-block">The Ultimate</span>--}}
                     {{--<span class="d-block fontbold">Next Big Thing </span>--}}
                     {{--<span class="d-block font-xlight"> In One Page</span>--}}
                  {{--</h2>--}}
                  {{--<a class="button btnprimary hvrwhite top40 pagescroll" href="#our-process">Learn More</a>--}}
               {{--</div>--}}
               {{--<div class="item bottom30">--}}
                  {{--<h2 class="text-capitalize font-xlight whitecolor">--}}
                     {{--<span class="d-block">Professional code</span>--}}
                     {{--<span class="d-block fontbold">core power </span>--}}
                     {{--<span class="d-block font-xlight"> In One Page</span>--}}
                  {{--</h2>--}}
                  {{--<a class="button btnprimary hvrwhite top40 pagescroll" href="#our-process">Learn More</a>--}}
               {{--</div>--}}
               {{--<div class="item bottom30">--}}
                  {{--<h2 class="text-capitalize font-xlight whitecolor">--}}
                     {{--<span class="d-block">font-xlightThe Ultimate</span>--}}
                     {{--<span class="d-block fontbold">Next Big Thing </span>--}}
                     {{--<span class="d-block font-xlight"> In One Page</span>--}}
                  {{--</h2>--}}
                  {{--<a class="button btnprimary hvrwhite top40 pagescroll" href="#our-process">Learn More</a>--}}
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!--Main Slider ends -->
  
<!--Some Feature -->  
<section id="our-feature" class="padding single-feature">
   <div class="container">
      <div class="row">
         <div class="col-md-6 col-sm-7 text-md-left text-center">
            <div class="heading-title heading_space">
               <span>A Pasta Inteligente do Despachante de Armas</span>
               <h2 class="darkcolor bottom30">docGun</h2>
            </div>
            <p class="bottom35">O docGun é um sistema 100% web, que você pode acessar e utilizar em qualquer aparelho. É simples de usar, mas qualquer dúvida é só falar direto com os desenvolvedores. Crie sua conta gratuita, facilite seu trabalho, e não olhe para trás.</p>
            <a class="button btnsecondary" href="{{route('get_register')}}" target="_blank">criar conta grátis</a>
         </div>
         <div class="col-md-6 col-sm-5">
            <div class="image top50"><img alt="SEO" src="{{asset('assets/img/docgun-responsivo-2.png')}}"></div>
         </div>
      </div>
   </div>
</section>
<!--Some Feature ends-->          

<!--half img section-->  
<section class="half-section">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-6 nopadding">
            <div class="image hover-effect img-container">
               <img alt="" src="{{asset('assets/img/suporte-sem-frescura-2.jpg')}}" class="equalheight">
            </div>
         </div>
         <div class="col-md-6 nopadding">
            <div class="split-box text-center center-block container-padding equalheight">
               <div class="heading-title padding">
               <span>Chama no chat | manda um zap</span>
               <h2 class="darkcolor bottom20">suporte sem frescura</h2>
               <p class="heading_space"> Sistema sem suporte é arma sem gatilho. Quando você mais precisar, quem irá te ajudar? Nós mesmos. E sem essa de criar ticket, enviar email e "esperar sentado". Chama a gente no chat ou manda um zap. Pronto.</p>
            </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="half-section">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-6 nopadding">
            <div class="split-box text-center center-block container-padding equalheight">
               <div class="heading-title padding">
               <span>novas funcionalidades sem custo extra</span>
               <h2 class="darkcolor bottom20">desenvolvimento contínuo</h2>
               <p class="heading_space"> A ideia é a seguinte: você não tem custo de instalação, não tem taxa de cancelamento. Queremos que você continue conosco porque nosso sistema facilita sua vida. E, para isso, não paramos de desenvolver novos recursos. E sem custo adicional.</p>
            </div>
            </div>
         </div>
         <div class="col-md-6 nopadding">
            <div class="image hover-effect img-container">
               <img alt="" src="{{asset('assets/img/desenvolvimento.jpg')}}" class="equalheight">
            </div>
         </div>
      </div>
   </div>
</section>  
<!--half img section ends-->   

<!-- Mobile Apps -->  
<section id="our-apps" class="padding">
   <div class="container">
      <div class="row">
         <div class="col-md-12 col-sm-12 text-center">
            <div class="heading-title">
               {{--<span>Como </span>--}}
               <h2 class="darkcolor heading_space">oferecemos</h2>
            </div>
         </div>
      </div>
      <div class="row" id="app-feature">
         <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="content-left clearfix">
               <div class="feature-item left top30 bottom30" data-target="1">
                  <span class="icon"><i class="fa fa-desktop"></i></span>
                  <div class="text">
                     <h4>Design Responsivo</h4>
                     <p>O mesmo sistema em qualquer dispositivo. Acesse em seu desktop, notebook, tablet ou celular.</p>
                  </div>
               </div>
               <div class="feature-item left top30 bottom30" data-target="2">
                  <span class="icon"><i class="fa fa-cog"></i></span>
                  <div class="text">
                     <h4>Soluções reais</h4>
                     <p>O docGun foi e continuará sendo desenvolvido em conjunto com profissionais da área.</p>
                  </div>
               </div>
               <div class="feature-item left top30 bottom30" data-target="3">
                  <span class="icon"><i class="fa fa-edit"></i></span>
                  <div class="text">
                     <h4>Sistema fácil de usar</h4>
                     <p>Materializamos as ideias pensando na melhor experiência do usuário.</p>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="image feature-item text-center">
               <img src="{{asset('assets/img/print-celular.png')}}" alt="">
            </div>
         </div>
         <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="content-right clearfix">
               <div class="feature-item right top30 bottom30" data-target="4">
                  <span class="icon"><i class="fa fa-handshake-o"></i></span>
                  <div class="text">
                     <h4>Liberdade</h4>
                     <p>17 dias para testar tudo. Sem amarras, sem multas. Aqui é parceria.</p>
                  </div>
               </div>
               <div class="feature-item right top30 bottom30" data-target="5">
                  <span class="icon"><i class="fa fa-folder-o"></i></span>
                  <div class="text">
                     <h4>Compartilhamento</h4>
                     <p>Compartilhe modelos de documentos com despachantes de todo o Brasil.</p>
                  </div>
               </div>
               <div class="feature-item right top30 bottom30" data-target="6">
                  <span class="icon"><i class="fa fa-support"></i></span>
                  <div class="text">
                     <h4>Suporte 24h</h4>
                     <p>Através de chat no facebook e whatsapp.</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>                                                                                                                             <!--Mobile Apps ends-->  
        
<!-- Counters -->  
<section id="funfacts" class="padding_top fact-iconic gradient_bg">
   <div class="container">
      <div class="row">
         <div class="col-md-5 col-sm-12 margin_bottom whitecolor text-md-left text-center">
            <h3 class="bottom25">docGun em números</h3>
            <p class="title">Faça parte do time de assessores em armas de todo o Brasil que fazem parte dessa conquista.</p>
         </div>
         <div class="col-md-7 col-sm-12 text-center">
            <div class="row">
               <div class="col-md-4 col-sm-4 icon-counters whitecolor margin_bottom">
                  <div class="img-icon bottom15">
                     <i class="fa fa-smile-o"></i>
                  </div>
                  <div class="counters">
                     <span class="count_nums" data-to="93" data-speed="2500"> </span> <i class="fa fa-plus"></i>
                  </div>
                  <p class="title">Clientes cadastrados</p>
               </div>
               <div class="col-md-4 col-sm-4 icon-counters whitecolor margin_bottom">
                  <div class="img-icon bottom15">
                     <i class="fa fa-fire"> </i>
                  </div>
                  <div class="counters">
                     <span class="count_nums" data-to="112" data-speed="2500"> </span> <i class="fa fa-plus"></i>
                  </div>
                  <p class="title">Armas registradas</p>
               </div>
               <div class="col-md-4 col-sm-4 icon-counters whitecolor margin_bottom">
                  <div class="img-icon bottom15">
                     <i class="fa fa-file-archive-o"></i>
                  </div>
                  <div class="counters">
                     <span class="count_nums" data-to="178" data-speed="2500"> </span> <i class="fa fa-plus"></i>
                  </div>
                  <p class="title">Documentos criados</p>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!--Counters ends-->  
  
<!-- Pricing Tables -->  
<section id="our-pricings" class="padding bglight">
   <div class="container">
      <div class="row">
         <div class="col-md-8 offset-md-2 col-sm-12 text-center">
            <div class="heading-title">
               <span>flexibilidade para acompanhar seu negócio</span>
               <h2 class="darkcolor bottom30">planos e preços</h2>
            </div>
         </div>
      </div>
      <div class="row three-col-pricing">
         <div class="col-lg-4 col-sm-12">
            <div class="price-table top60">
               <h3 class="bottom20 darkcolor">Teste grátis</h3>
               <div class="ammount">
                  <h2 class="defaultcolor">R$ 0,00 <span class="dur">/ 17 dias</span></h2>
               </div>
               <ul class="top20">
                  <li><span>1 usuário</span></li>
                  <li><span>Todas as funcionalidades</span></li>
                  <li><span>Suporte 24h</span></li>
               </ul>
               <div class="clearfix"></div>
               <a href="{{route('get_register')}}" target="_blank" class="button btnprimary top50">Criar conta</a>
            </div>
         </div>
         <div class="col-lg-4 col-sm-12">
            <div class="price-table top60">
               <h3 class="bottom20 darkcolor">plano Soldado</h3>
               <div class="ammount">
                  <h2 class="defaultcolor">R$ 69.90 <span class="dur">/ mês</span></h2>
               </div>
               <ul class="top20">
                  <li><span>1 usuário</span></li>
                  <li><span>Todas as funcionalidades</span></li>
                  <li><span>Suporte 24h</span></li>
               </ul>
               <div class="clearfix"></div>
               <a href="{{route('get_register')}}" class="button btnprimary top50">Testar 17 dias</a>
            </div>
         </div>
         <div class="col-lg-4 col-sm-12">
            <div class="price-table top60">
               <h3 class="bottom20 darkcolor">plano Tropa</h3>
               <div class="ammount">
                  <h2 class="defaultcolor">R$ 119.80 <span class="dur">/ mês</span></h2>
               </div>
               <ul class="top20">
                  <li><span>2 usuários</span></li>
                  <li><span>Todas as funcionalidades</span></li>
                  <li><span>Suporte 24h</span></li>
               </ul>
               <div class="clearfix"></div>
               <a href="{{route('get_register')}}" class="button btnprimary top50">Testar 17 dias</a>
            </div>
         </div>
      </div>
      <div class="row three-col-pricing">
         <div class="col-lg-4 col-sm-12">
            <div class="price-table top60">
               <h3 class="bottom20 darkcolor">plano Esquadrão</h3>
               <div class="ammount">
                  <h2 class="defaultcolor">R$ 169.70 <span class="dur">/ mês</span></h2>
               </div>
               <ul class="top20">
                  <li><span>3 usuários</span></li>
                  <li><span>Todas as funcionalidades</span></li>
                  <li><span>Suporte 24h</span></li>
               </ul>
               <div class="clearfix"></div>
               <a href="{{route('get_register')}}" class="button btnprimary top50">Testar 17 dias</a>
            </div>
         </div>
         <div class="col-lg-4 col-sm-12">
            <div class="price-table top60">
               <h3 class="bottom20 darkcolor">plano Batalhão</h3>
               <div class="ammount">
                  <h2 class="defaultcolor">R$ 269.50 <span class="dur">/ mês</span></h2>
               </div>
               <ul class="top20">
                  <li><span>5 usuários</span></li>
                  <li><span>Todas as funcionalidades</span></li>
                  <li><span>Suporte 24h</span></li>
               </ul>
               <div class="clearfix"></div>
               <a href="{{route('get_register')}}" class="button btnprimary top50">Testar 17 dias</a>
            </div>
         </div>
         <div class="col-lg-4 col-sm-12">
            <div class="price-table top60">
               <h3 class="bottom20 darkcolor">plano Exército</h3>
               <div class="ammount">
                  <h2 class="defaultcolor">R$ 369.30 <span class="dur">/ mês</span></h2>
               </div>
               <ul class="top20">
                  <li><span>7 usuários</span></li>
                  <li><span>Todas as funcionalidades</span></li>
                  <li><span>Suporte 24h</span></li>
               </ul>
               <div class="clearfix"></div>
               <a href="{{route('get_register')}}" class="button btnprimary top50">Testar 17 dias</a>
            </div>
         </div>
      </div>
      </div>
   </div>
</section>
<!--Pricing Tables ends-->  
  
<!-- Testimonials -->  
<section id="our-testimonial" class="padding testimonial-bg parallaxie">
   <div class="container">
      <div class="row">
         <div class="col-md-6 col-sm-8">
            <div id="testimonial-quote" class="owl-carousel">
               <div class="item">
                  <div class="testimonial-quote whitecolor">
                    <h3 class="bottom30">Fico feliz em ter ajudado no desenvolvimento de uma solução pioneira para assessores em armas. Recomendo aos colegas.</h3>
                     <h6>Flavio Boni</h6>
                     <small>Assessor, Boni Assessoria em Armas</small>
                  </div>
               </div>
               <div class="item">
                  <div class="testimonial-quote whitecolor">
                    <h3 class="bottom30">Conta com uma plataforma online disponível no meu notebook ou no celular, que é melhor para minha comodidade, pois estou em
                       constante movimento entre clube de tiro e o escritório.</h3>
                     <h6>Michael da Silva Dias</h6>
                     <small>Assessor, Acervo Armas</small>
                  </div>
               </div>
            </div>
            <div id="owl-thumbs" class="owl-dots">
               <div class="owl-dot active"><img src="images/testimonial-1.jpg" alt=""></div>
               <div class="owl-dot"><img src="images/testimonial-2.jpg" alt=""></div>
            </div>
         </div>
      </div>
   </div>
</section>
<!--Testimonials Ends-->


<!-- Video Click -->  
<section id="video-bg" class="video-parallax padding_top bg-light ">
   <div class="container">
      <div class="row">
         <div class="col-md-5 col-sm-5">
            <div class="heading-title text-md-left text-center padding_bottom">
               <span>conheça o sistema em menos de 1 minuto</span>
               <h2 class="fontregular bottom20 darkcolor">docGun em ação</h2>
               <p></p>
               <a href="{{route('get_register')}}" target="_blank" class="button btnprimary fontmedium top20 pagescroll">Testar por 17 dias</a>
            </div>
         </div>
         <div class="col-md-7 col-sm-7 padding_bottom">
            <div class="image">
               {{--<img alt="video img" src="images/video-click.jpg">--}}
               {{--<a data-fancybox href="https://www.youtube.com/watch?v=GhvD7NtUT-Q&autoplay=1&rel=0&controls=1&showinfo=0" class="button-play fontmedium"><i class="fa fa-play"></i></a>--}}
               <iframe style="height: 315px" width="560" height="315" src="https://www.youtube-nocookie.com/embed/KfdDy_8QDzM?controls=0&rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
         </div>
      </div>
   </div>
</section>
<!--Video Click-->

<!--Site Footer Here-->
<footer id="site-footer" class="padding_half">
   <div class="container">
      <div class="row">
         <div class="col-md-12 col-sm-12 text-center">
            <ul class="social-icons bottom25">
               <li><a href="https://www.linkedin.com/company/docgun"><i class="fa fa-linkedin"></i> </a> </li>
               <li><a href="https://www.facebook.com/docgun.mnzt/"><i class="fa fa-facebook"></i> </a> </li>
               <li><a href="https://www.instagram.com/docgun.mnzt/"><i class="fa fa-instagram"></i> </a> </li>
            </ul>
            <p class="copyrights"><a href="https://monazite.com.br">monazite</a> | 2018 </p>
         </div>
      </div>
   </div>
</footer>
<!--Footer ends-->   



<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="js/jquery-3.1.1.min.js"></script>

<!--Bootstrap Core-->
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<!--to view items on reach-->
<script src="js/jquery.appear.js"></script>

<!--Equal-Heights-->
<script src="js/jquery.matchHeight-min.js"></script>

<!--Owl Slider-->
<script src="js/owl.carousel.min.js"></script>

<!--number counters-->
<script src="js/jquery-countTo.js"></script>
 
<!--Parallax Background-->
<script src="js/parallaxie.js"></script>
  
<!--Cubefolio Gallery-->
<script src="js/jquery.cubeportfolio.min.js"></script>

<!--FancyBox popup-->
<script src="js/jquery.fancybox.min.js"></script>          

<!-- Video Background-->
<script src="js/jquery.background-video.js"></script>

<!--TypeWriter-->
<script src="js/typewriter.js"></script> 
      
<!--Particles-->
<script src="js/particles.min.js"></script>            
  
<!--WOw animations-->
<script src="js/wow.min.js"></script>

<!--Revolution SLider-->
<script src="js/revolution/jquery.themepunch.tools.min.js"></script>
<script src="js/revolution/jquery.themepunch.revolution.min.js"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script src="js/revolution/extensions/revolution.extension.actions.min.js"></script>
<script src="js/revolution/extensions/revolution.extension.carousel.min.js"></script>
<script src="js/revolution/extensions/revolution.extension.kenburn.min.js"></script>
<script src="js/revolution/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="js/revolution/extensions/revolution.extension.migration.min.js"></script>
<script src="js/revolution/extensions/revolution.extension.navigation.min.js"></script>
<script src="js/revolution/extensions/revolution.extension.parallax.min.js"></script>
<script src="js/revolution/extensions/revolution.extension.slideanims.min.js"></script>
<script src="js/revolution/extensions/revolution.extension.video.min.js"></script>

<!--Google Map API-->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyAOBKD6V47-g_3opmidcmFapb3kSNAR70U"></script>
<script src="js/functions.js"></script>
		
</body>
</html>