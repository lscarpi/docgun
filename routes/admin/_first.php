<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'check_if_its_admin'], function () {

    // GET /admin/
    Route::get('/', 'DashboardController@getDashboard')->name('admin.getDashboard');
});
