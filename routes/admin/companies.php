<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'admin/companies', 'middleware' => 'check_if_its_admin'], function () {

    // GET /companies/
    Route::get('/', [
        'as' => 'get_companies',
        'uses' => 'CompaniesController@get_companies'
    ]);

    // GET /companies/{company}/delete
    Route::get('/{company}/delete', [
        'as' => 'get_companies_delete',
        'uses' => 'CompaniesController@get_companies_delete'
    ]);

    // GET /companies/{company}/login-as
    Route::get('/{company}/login-as', [
        'as' => 'get_companies_login_as',
        'uses' => 'CompaniesController@get_companies_login_as'
    ]);

    // GET /companies/login-back
    Route::get('/login-back', [
        'as' => 'get_companies_login_back',
        'uses' => 'CompaniesController@get_companies_login_back'
    ]);
});