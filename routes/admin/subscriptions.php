<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'admin/subscriptions',
    'namespace' => 'Admin',
    'middleware' => 'check_if_its_admin'
], function () {

    // GET /admin/subscriptions
    Route::get('/', 'SubscriptionsController@getSubscriptions')->name('admin.getSubscriptions');

    // GET /admin/subscriptions/{subscription}/edit
    Route::get('/{subscription}/edit', 'SubscriptionsController@getSubscriptionsEdit')
        ->name('admin.getSubscriptionsEdit');

    // PATCH /admin/subscriptions/{subscription}
    Route::patch('/{subscription}', 'SubscriptionsController@patchSubscriptionsUpdate')
        ->name('admin.patchSubscriptionsUpdate');
});
