<?php


use Illuminate\Support\Facades\Route;

// GET /
Route::get('/', [
    'as' => 'get_site_homepage',
    'uses' => 'SiteController@get_site_homepage',
]);

// GET /teste
Route::get('teste', [
    'as' => 'get_teste',
    'uses' => 'SiteController@get_teste',
]);

// Se for development server
if (isDevelopmentServer()) {
    Route::get('/app', [
        'as' => 'get_site_homepage',
        'uses' => 'SiteController@get_site_homepage',
    ]);
}

/*
  * Assinaturas
  */
// GET /subscriptions/{provider}/handle-notifications
Route::post('/subscriptions/{provider}/handle-notifications', [
    'as' => 'post_subscriptions_handle_notifications',
    'uses' => 'SubscriptionsController@post_subscriptions_handle_notifications'
]);


/*
 * Route para auto pull do server para o gitlab
 */
Route::post('gitlab/update', [
    'as' => 'post_gitlab_update',
    'uses' => 'SiteController@post_gitlab_update'
]);


// Todas as rotas dentro dessa closure terão que estar autenticadas e a subscription ser valid
Route::group(['middleware' => ['auth', 'verify_company_subscription', 'verify_current_logged_in_user']], function () {

    // GET /
    Route::get('/dashboard', [
        'as' => 'get_dashboard',
        'middleware' => 'auth',
        'uses' => 'SiteController@get_dashboard'
    ]);

    // POST /send-feedback
    Route::post('/send-feedback', [
        'as' => 'post_send_feedback',
        'uses' => 'SiteController@post_send_feedback'
    ]);
});
