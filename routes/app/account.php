<?php

use Illuminate\Support\Facades\Route;

Route::group([
        'prefix' => 'account',
        'middleware' => ['auth', 'verify_company_subscription', 'verify_current_logged_in_user']
], function () {
    Route::get('/profile', [
        'as' => 'get_account_profile',
        'uses' => 'AccountController@get_account_profile'
    ]);

    Route::post('/profile', [
        'as' => 'post_account_profile',
        'uses' => 'AccountController@post_account_profile'
    ]);

    Route::get('/company', [
        'as' => 'get_account_company',
        'uses' => 'AccountController@get_account_company'
    ]);

    Route::post('/company', [
        'as' => 'post_account_company',
        'uses' => 'AccountController@post_account_company'
    ]);

    Route::get('/subscription', [
        'as' => 'get_account_subscription',
        'uses' => 'AccountController@get_account_subscription',
    ]);

    Route::post('/subscription', [
        'as' => 'post_account_subscription',
        'uses' => 'AccountController@post_account_subscription',
    ]);

    Route::get('/subscription/{subscription}/pay', [
        'as' => 'get_account_subscription_pay',
        'uses' => 'AccountController@get_account_subscription_pay',
    ]);

    Route::get('/subscription/{subscription}/cancel', [
        'as' => 'get_account_subscription_cancel',
        'uses' => 'AccountController@get_account_subscription_cancel',
    ]);

    Route::get('/subscription/{subscription}/payment-thankyou', [
        'as' => 'get_account_subscription_thankyou',
        'uses' => 'AccountController@get_account_subscription_thankyou',
    ]);
});
