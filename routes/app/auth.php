<?php


// GET /login
use Illuminate\Support\Facades\Route;

Route::get('/login', [
    'as' => 'get_login',
    'uses' => 'AuthController@get_login'
]);

// POST /login
Route::post('/login', [
    'as' => 'post_login',
    'uses' => 'AuthController@post_login'
]);

// GET /register
Route::get('/register', [
    'as' => 'get_register',
    'uses' => 'AuthController@get_register'
]);

// POST /register
Route::post('/register', [
    'as' => 'post_register',
    'uses' => 'AuthController@post_register'
]);

// GET /register/thank-you
Route::get('/register/thank-you', [
    'as' => 'get_register_thank_you',
    'uses' => 'AuthController@get_register_thank_you'
]);

// GET /register/{token}/confirm-registration
Route::get('/register/{token}/confirm-registration', [
    'as' => 'get_register_confirm_registration',
    'uses' => 'AuthController@get_register_confirm_registration'
]);

// GET /logout
Route::get('/logout', [
    'as' => 'get_logout',
    'uses' => 'AuthController@get_logout'
]);

// GET /recover-password
Route::get('/recover-password', [
    'as' => 'get_recover_password',
    'uses' => 'AuthController@get_recover_password'
]);

// POST /recover-password
Route::post('/recover-password', [
    'as' => 'post_recover_password',
    'uses' => 'AuthController@post_recover_password'
]);