<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'clubs',
    'middleware' => ['auth', 'verify_company_subscription', 'verify_current_logged_in_user']
], function () {

    // GET /clubs/
    Route::get('/', [
        'as' => 'get_clubs',
        'uses' => 'ClubsController@get_clubs'
    ]);

    // GET /clubs/create
    Route::get('/create', [
        'as' => 'get_clubs_create',
        'uses' => 'ClubsController@get_clubs_create'
    ]);

    // POST /clubs/create
    Route::post('/create', [
        'as' => 'post_clubs_create',
        'uses' => 'ClubsController@post_clubs_create'
    ]);

    // GET /clubs/{club}/edit
    Route::get('/{club}/edit', [
        'as' => 'get_clubs_edit',
        'uses' => 'ClubsController@get_clubs_edit'
    ]);

    // POST /clubs/{club}/edit
    Route::post('/{club}/edit', [
        'as' => 'post_clubs_edit',
        'uses' => 'ClubsController@post_clubs_edit'
    ]);

    // GET /clubs/{club}/delete
    Route::get('/{club}/delete', [
        'as' => 'get_clubs_delete',
        'uses' => 'ClubsController@get_clubs_delete'
    ]);
});