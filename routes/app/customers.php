<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'customers',
    'middleware' => ['auth', 'verify_company_subscription', 'verify_current_logged_in_user']
], function () {

    // GET /customers/
    Route::get('/', [
        'as' => 'get_customers',
        'uses' => 'CustomersController@get_customers'
    ]);

    // GET /customers/create
    Route::get('/create', [
        'as' => 'get_customers_create',
        'uses' => 'CustomersController@get_customers_create'
    ]);

    // POST /customers/create
    Route::post('/create', [
        'as' => 'post_customers_create',
        'uses' => 'CustomersController@post_customers_create'
    ]);

    // GET /customers/{customer}/view
    Route::get('/{customer}', [
        'as' => 'get_customers_view',
        'uses' => 'CustomersController@get_customers_view'
    ]);

    // GET /customers/{customer}/edit
    Route::get('/{customer}/edit', [
        'as' => 'get_customers_edit',
        'uses' => 'CustomersController@get_customers_edit'
    ]);


    // POST /customers/{customer}/edit
    Route::post('/{customer}/edit', [
        'as' => 'post_customers_edit',
        'uses' => 'CustomersController@post_customers_edit'
    ]);

    // GET /customers/{customer}/delete
    Route::get('/{customer}/delete', [
        'as' => 'get_customers_delete',
        'uses' => 'CustomersController@get_customers_delete'
    ]);

    // GET /customers/{customer}/details
    Route::get('/{customer}/details', [
        'as' => 'get_customers_customer_data_details',
        'uses' => 'CustomersController@get_customers_customer_data_details'
    ]);

    // GET /customers/{customer}/details-pdf
    Route::get('/{customer}/details-pdf', [
        'as' => 'get_customers_customer_data_details_pdf',
        'uses' => 'CustomersController@get_customers_customer_data_details_pdf'
    ]);

    /*
    * Armas
     */
    Route::group(['prefix' => '/{customer}/weapons'], function () {


        // GET /customers/{customer}/weapons/create
        Route::get('/create', [
            'as' => 'get_customers_weapons_create',
            'uses' => 'WeaponsController@get_customers_weapons_create'
        ]);

        // POST /customers/{customer}/weapons/create
        Route::post('/create', [
            'as' => 'post_customers_weapons_create',
            'uses' => 'WeaponsController@post_customers_weapons_create'
        ]);

        // GET /customers/{customer}/weapons/{weapon}/details
        Route::get('/{weapon}/details', [
            'as' => 'get_customers_weapons_details',
            'uses' => 'WeaponsController@get_customers_weapons_details'
        ]);

        // GET /customers/{customer}/weapons/{weapon}/edit
        Route::get('/{weapon}/edit', [
            'as' => 'get_customers_weapons_edit',
            'uses' => 'WeaponsController@get_customers_weapons_edit'
        ]);

        // POST /customers/{customer}/weapons/{weapon}/edit
        Route::post('/{weapon}/edit', [
            'as' => 'post_customers_weapons_edit',
            'uses' => 'WeaponsController@post_customers_weapons_edit'
        ]);

        // GET /customers/{customer}/weapons/{weapon}/delete
        Route::get('/{weapon}/delete', [
            'as' => 'get_customers_weapons_delete',
            'uses' => 'WeaponsController@get_customers_weapons_delete'
        ]);
    });

    /*
    * Documentos
     */
    Route::group(['prefix' => '/{customer}/issued-documents'], function () {
        // GET /customers/{customer}/issued-documents/create
        Route::get('/create', [
            'as' => 'get_customers_issued_documents_create',
            'uses' => 'IssuedDocumentsController@get_customers_issued_documents_create'
        ]);

        // POST /customers/{customer}/issued-documents/create
        Route::post('/create', [
            'as' => 'post_customers_issued_documents_create',
            'uses' => 'IssuedDocumentsController@post_customers_issued_documents_create'
        ]);

        // GET /customers/{customer}/issued-documents/details
        Route::get('/{issued_document}/details', [
            'as' => 'get_customers_issued_documents_details',
            'uses' => 'IssuedDocumentsController@get_customers_issued_documents_details'
        ]);

        // POST /customers/{customer}/issued-documents/edit
        Route::post('/{issued_document}/edit', [
            'as' => 'post_customers_issued_documents_edit',
            'uses' => 'IssuedDocumentsController@post_customers_issued_documents_edit'
        ]);

        // GET /customers/{customer}/issued-documents/delete
        Route::get('/{issued_document}/delete', [
            'as' => 'get_customers_issued_documents_delete',
            'uses' => 'IssuedDocumentsController@get_customers_issued_documents_delete'
        ]);
    });
});