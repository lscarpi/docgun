<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'documents',
    'middleware' => ['auth', 'verify_company_subscription', 'verify_current_logged_in_user']
], function () {

    // GET /documents/
    Route::get('/', [
        'as' => 'get_documents',
        'uses' => 'DocumentsController@get_documents'
    ]);

    // GET /documents/create
    Route::get('/create', [
        'as' => 'get_documents_create',
        'uses' => 'DocumentsController@get_documents_create'
    ]);

    // GET /documents/share
    Route::get('/documents/{document}/share', [
        'as' => 'get_documents_share',
        'uses' => 'DocumentsController@get_documents_share'
    ]);

    // GET /documents/unshare
    Route::get('/documents/{document}/unshare', [
        'as' => 'get_documents_unshare',
        'uses' => 'DocumentsController@get_documents_unshare'
    ]);

    // POST /documents/create
    Route::post('/create', [
        'as' => 'post_documents_create',
        'uses' => 'DocumentsController@post_documents_create'
    ]);

    // GET /documents/{document}/
    Route::get('/{document}/', [
        'as' => 'get_documents_view',
        'uses' => 'DocumentsController@get_documents_view'
    ]);

    // GET /documents/{document}/edit
    Route::get('/{document}/edit', [
        'as' => 'get_documents_edit',
        'uses' => 'DocumentsController@get_documents_edit'
    ]);

    // POST /documents/{document}/edit
    Route::post('/{document}/edit', [
        'as' => 'post_documents_edit',
        'uses' => 'DocumentsController@post_documents_edit'
    ]);

    // GET /documents/{document}/delete
    Route::get('/{document}/delete', [
        'as' => 'get_documents_delete',
        'uses' => 'DocumentsController@get_documents_delete'
    ]);
});