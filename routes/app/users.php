<?php


use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'users',
    'middleware' => ['auth', 'verify_company_subscription', 'verify_current_logged_in_user']
], function () {

    // GET /users/
    Route::get('/', [
        'as' => 'get_users',
        'uses' => 'UsersController@get_users'
    ]);

    // GET /users/create
    Route::get('/create', [
        'as' => 'get_users_create',
        'uses' => 'UsersController@get_users_create'
    ]);

    // POST /users/create
    Route::post('/create', [
        'as' => 'post_users_create',
        'uses' => 'UsersController@post_users_create'
    ]);

    // GET /users/{user}/edit
    Route::get('/{user}/edit', [
        'as' => 'get_users_edit',
        'uses' => 'UsersController@get_users_edit'
    ]);

    // POST /users/{user}/edit
    Route::post('/{user}/edit', [
        'as' => 'post_users_edit',
        'uses' => 'UsersController@post_users_edit'
    ]);

    // GET /users/{user}/delete
    Route::get('/{user}/delete', [
        'as' => 'get_users_delete',
        'uses' => 'UsersController@get_users_delete'
    ]);
});