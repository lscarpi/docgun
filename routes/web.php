<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Library\RouteLoader;

// Load the main app routes
RouteLoader::loadRoutesFromDirectory(base_path('routes/app'));
// Load the admin routes
RouteLoader::loadRoutesFromDirectory(base_path('routes/admin'));

// Route::get('/debug-sentry', function () {
//     throw new Exception('My first Sentry error!');
// });